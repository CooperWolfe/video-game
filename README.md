# Apocolypse of the Damned (yes really)
I am making this game just to try developing games in C++. I am using OpenGL for
the graphics, but the game engine is all me. I made all of the sprites but two 
of the paralax backgrounds are stolen.

## Gameplay
Zombies have taken over. The school cafetorium is in shambles and Jeremy and
Michael are the only survivors. Their death is inevitable but they hope to
survive as long as possible. 

In this two-player game, Jeremy and Michael work together, shooting guns that
keep zombies at bay and dropping portals to Hell for them to fall into. The goal
is to kill as many zombies as possible before either player is touched three
times. You have 5 seconds of pacifist invincibility in the beginning.

## Sprites
I made all my sprites but two of the backgrounds are stolen (city and sunset).

## Music
You can find the music I used at
[https://www.youtube.com/watch?v=igB5IoVolwA][0].

## Conclusion
If either player is touched by zombies three times (with 100 frames of recovery
time in between), the game is over.

[0]: https://www.youtube.com/watch?v=igB5IoVolwA