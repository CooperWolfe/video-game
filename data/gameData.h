#ifndef GAMEDATA__H
#define GAMEDATA__H
#include <string>
#include <map>
#include "../math/vector2f.h"
#include "parseXML.h"

class Gamedata {
public:
  static Gamedata& getInstance();
  ~Gamedata();
  void displayData() const;

  bool getBool(const std::string&) const;
  bool tryGetBool(const std::string&, const bool) const;
  unsigned char getChar(const std::string&) const;
  unsigned char tryGetChar(const std::string&, const unsigned char) const;
  const std::string& getStr(const std::string&) const;
  const std::string& tryGetStr(const std::string&, const std::string&) const;
  float getFloat(const std::string&) const;
  float tryGetFloat(const std::string&, const float) const;
  short getShort(const std::string&) const;
  short tryGetShort(const std::string&, const short) const;
  int getInt(const std::string&) const;
  int tryGetInt(const std::string&, const int) const;
  unsigned getUnsigned(const std::string&) const;
  unsigned tryGetUnsigned(const std::string&, const unsigned) const;
  float getRandInRange(int min, int max) const;
  float getRandFloat(float min, float max) const;
  bool checkTag(const std::string&) const;

private:
  ParseXML parser;
  const map<std::string, std::string> gameData;

  Gamedata(const std::string& fn = "data/game.xml");
  Gamedata(const Gamedata&);
  Gamedata& operator=(const Gamedata&);
};
#endif
