#include <sstream>
#include <iostream>
#include <limits>
#include <cmath>
#include <string>
#include "gameData.h"

Gamedata& Gamedata::getInstance() {
  static Gamedata gamedata;
  return gamedata;
}

Gamedata::~Gamedata( ) { }

Gamedata::Gamedata(const string& fn) :
  parser(fn),
  gameData(parser.getXmlData())
{ }

float Gamedata::getRandInRange(int min, int max) const {
  return min + (rand()/(std::numeric_limits<int>::max()+1.0f))*(max-min);
}

float Gamedata::getRandFloat(float min, float max) const {
  return min + (rand() / (RAND_MAX + 1.0f)) * (max - min);
}

bool Gamedata::checkTag(const std::string& tag) const {
  return gameData.count(tag)!=0;
}

bool Gamedata::getBool(const std::string& tag) const {
  auto ptr = gameData.find(tag);
  if (ptr == gameData.end())
    throw std::string("Game: Didn't find boolean tag ") + tag + std::string(" in xml");
  return ptr->second == "true";
}
bool Gamedata::tryGetBool(const std::string& tag, const bool _default) const {
  return checkTag(tag) ? getBool(tag) : _default;
}

unsigned char Gamedata::getChar(const std::string& tag) const {
  auto ptr = gameData.find(tag);
  if (ptr == gameData.end())
    throw string("Game: Didn't find char tag ") + tag + string(" in xml");
  std::stringstream ss;
  ss << ptr->second;
  unsigned char data;
  ss >> data;
  return data;
}
unsigned char Gamedata::tryGetChar(const std::string& tag, const unsigned char _default) const {
  return checkTag(tag) ? getChar(tag) : _default;
}

short Gamedata::getShort(const std::string& tag) const {
  auto ptr = gameData.find(tag);
  if (ptr == gameData.end())
    throw std::string("Game: Didn't find short tag ") + tag + std::string(" in xml");
  std::stringstream strm;
  strm << ptr->second;
  short data;
  strm >> data;
  return data;
}
short Gamedata::tryGetShort(const std::string& tag, const short _default) const {
  return checkTag(tag) ? getShort(tag) : _default;
}

int Gamedata::getInt(const std::string& tag) const {
  auto ptr = gameData.find(tag);
  if (ptr == gameData.end())
    throw std::string("Game: Didn't find integer tag ") + tag + std::string(" in xml");
  std::stringstream strm;
  strm << ptr->second;
  int data;
  strm >> data;
  return data;
}
int Gamedata::tryGetInt(const std::string& tag, const int _default) const {
  return checkTag(tag) ? getInt(tag) : _default;
}

unsigned Gamedata::getUnsigned(const std::string& tag) const {
  auto ptr = gameData.find(tag);
  if (ptr == gameData.end())
    throw std::string("Game: Didn't find unsigned tag ") + tag + std::string(" in xml");
  std::stringstream ss;
  ss << ptr->second;
  unsigned data;
  ss >> data;
  return data;
}
unsigned Gamedata::tryGetUnsigned(const std::string& tag, const unsigned _default) const {
  return checkTag(tag) ? getUnsigned(tag) : _default;
}

float Gamedata::getFloat(const std::string& tag) const {
  auto ptr = gameData.find(tag);
  if (ptr == gameData.end())
    throw string("Game: Didn't find float tag ")+tag+string(" in xml");
  else {
    std::stringstream strm;
    strm << ptr->second;
    float data;
    strm >> data;
    return data;
  }
}
float Gamedata::tryGetFloat(const std::string& tag, const float _default) const {
  return checkTag(tag) ? getFloat(tag) : _default;
}

const std::string& Gamedata::getStr(const std::string& tag) const {
  auto ptr = gameData.find(tag);
  if (ptr == gameData.end())
    throw string("Game: Didn't find string tag ")+tag+string(" in xml");
  return ptr->second;
}
const std::string& Gamedata::tryGetStr(const std::string& tag, const std::string& _default) const {
  return checkTag(tag) ? getStr(tag) : _default;
}

void Gamedata::displayData() const {
  for (auto& pair : gameData)
    std::cout << pair.first << ", " << pair.second << std::endl;
}
