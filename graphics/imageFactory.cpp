#include "ioMod.h"
#include "../math/vector2f.h"
#include "renderContext.h"

ImageFactory& ImageFactory::getInstance() {
  static ImageFactory instance;
  return instance;
}

ImageFactory::~ImageFactory() {
  // Free single image containers
  for (auto& surface : surfaces) SDL_FreeSurface(surface.second);
  for (auto& texture : textures) SDL_DestroyTexture(texture.second);
  for (auto& image : images) {
    std::cout << image.first << ", ";
    delete image.second;
  }

  // Free multi-image containers
  for (auto& surfaces : multiSurfaces)
    for (unsigned int i = 0; i < surfaces.second.size(); ++i)
      SDL_FreeSurface(surfaces.second[i]);
  for (auto& textures : multiTextures)
    for (unsigned int i = 0; i < textures.second.size(); ++i)
      SDL_DestroyTexture( textures.second[i] );

  for ( auto& images : multiImages ) {
    std::cout << images.first << ", ";
    for (unsigned int i = 0; i < images.second.size(); ++i)
      delete images.second[i];
  }
  std::cout << "Done.\n\n";
}

Image* ImageFactory::getImage(const std::string& name) {
  auto it = images.find(name);
  if ( it == images.end() ) {
    SDL_Surface * const surface =
      IoMod::getInstance().readSurface( gdata.getStr(name+"/file"));
    surfaces[name] = surface;
    Image * const image =new Image(surface);
    images[name] = image;
    return image;
  }
  else {
    return it->second;
  }
}

std::vector<Image*> ImageFactory::getImages(const std::string& name, unsigned rows) {
  // First search map to see if we've already made it:
  auto pos = multiImages.find(name);
  if (pos != multiImages.end())
    return pos->second;

  IoMod& iomod = IoMod::getInstance();
  RenderContext* renderContext = &RenderContext::getInstance();
  std::string sheetPath = gdata.getStr(name+"/file");
  SDL_Surface* spriteSurface = iomod.readSurface(sheetPath);

  // It wasn't in the map, so we have to make the vector of Images:
  unsigned numberOfFrames = gdata.getInt(name+"/frames");
  std::vector<Image*> images;
  std::vector<SDL_Surface*> surfaces;
  std::vector<SDL_Texture*> textures;
  images.reserve(numberOfFrames);
  surfaces.reserve(numberOfFrames);
  textures.reserve(numberOfFrames);

  int width = spriteSurface->w/numberOfFrames;
  int height = spriteSurface->h/rows;

  if(  gdata.checkTag(name+"/imageWidth")
    && gdata.checkTag(name+"/imageHeight") ){
    width  = gdata.getInt(name+"/imageWidth");
    height = gdata.getInt(name+"/imageHeight");
  }

  SpriteSheet sheet(spriteSurface,width,height);

  for (unsigned j = 0; j < rows; ++j) {
    for (unsigned i = 0; i < numberOfFrames; ++i) {
      SDL_Surface* surface = sheet(i, j);
      SDL_Texture* texture =
        SDL_CreateTextureFromSurface(renderContext->getRenderer(),surface);
      surfaces.push_back(surface);
      textures.push_back(texture);
      images.push_back(new Image(surface));
    }
  }
  multiSurfaces[name] = surfaces;
  multiTextures[name] = textures;
  multiImages[name] = images;
  return images;
}
