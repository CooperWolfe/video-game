#ifndef VIEWPORT__H
#define VIEWPORT__H
#include "../drawable.h"
#include "../data/gameData.h"
#include "components/hud.h"
#include "components/menu.h"
#include "../enums/state.h"
#include "components/image.h"

class Viewport {
public:
  static Viewport& getInstance();
  void draw(bool, short, short) const;
  void draw(state) const;
  void update();

  Vector2f getPosition() const { return viewPos; }
  int getWidth() const { return viewWidth; }
  int getHeight() const { return viewHeight; }
  float getX() const  { return viewPos[0]; }
  void  setX(float x) { viewPos[0] = x; }
  float getY() const  { return viewPos[1]; }
  void  setY(float y) { viewPos[1] = y; }
  void toggleHud() { displayHud = !displayHud; }

  void setObjectToTrack(int index, const Drawable * obj);
  void setObjectsToTrack(const Drawable * obj1, const Drawable * obj2);
  const Drawable * const * getObjectsToTrack() const { return objectsToTrack; }

private:
  const Gamedata& gdata;
  const Hud hud;
  const Menu menu;
  Vector2f viewPos;
  Vector2f msgPos;
  int worldWidth, worldHeight;
  int viewWidth, viewHeight;
  bool displayHud;

  const Drawable * objectsToTrack[2];
  const Image * heart;

  Viewport();
  Viewport(const Viewport&);
  Viewport& operator=(const Viewport&);

  void updateViewPos();
  void updateViewSize();
};
#endif
