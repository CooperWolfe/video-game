#include <sstream>
#include <cmath>
#include <string>
#include "viewport.h"
#include "ioMod.h"
#include "../engine/clock.h"
#include "../enums/state.h"
#include "imageFactory.h"

Viewport& Viewport::getInstance() {
  static Viewport viewport;
  return viewport;
}

Viewport::Viewport() :
  gdata(Gamedata::getInstance()),
  hud(), menu(),
  viewPos(0, 0),
  msgPos(Vector2f(gdata.getInt("view/loc/x"), gdata.getInt("view/loc/y"))),
  worldWidth(gdata.getInt("world/width")),
  worldHeight(gdata.getInt("world/height")),
  viewWidth(gdata.getInt("view/width")),
  viewHeight(gdata.getInt("view/height")),
  displayHud(false),
  objectsToTrack{ nullptr, nullptr },
  heart(ImageFactory::getInstance().getImage("sprites/heart")) {}

void Viewport::setObjectToTrack(int index, const Drawable * obj) {
  objectsToTrack[index] = obj;
}
void Viewport::setObjectsToTrack(const Drawable * obj1, const Drawable * obj2) {
  objectsToTrack[0] = obj1;
  objectsToTrack[1] = obj2;
}

void Viewport::draw(bool godMode, short jLives, short mLives) const {
  const IoMod& io = IoMod::getInstance();

  if (displayHud) hud.draw();

  io.writeText("Cooper Wolfe", 30, viewHeight - 48);
  io.writeText("Jeremy:", 30, 20);
  io.writeText("Michael:", viewWidth - 160, 20);
  if (godMode) io.writeText("God Mode", 375, 20);

  for (short i = 0; i < jLives; ++i)
    heart->draw(viewPos[0] + 30 + 32*i, viewPos[1] + 45);
  for (short i = 0; i < mLives; ++i)
    heart->draw(viewPos[0] + viewWidth-160 + 32*i, viewPos[1] + 45);
}

void Viewport::draw(state gameState) const {
  std::string message;
  switch (gameState) {
    case start: message = "APOCOLYPSE OF THE DAMNED!!!!"; break;
    case lose: message = "GAME OVER"; break;
    case win: message = "YOU WIN!!!!"; break;
    default: throw std::string("Invalid state for a menu.");
  }
  menu.draw(message);
}

void Viewport::update() {
  updateViewPos();
  if (viewPos[0] < 0) viewPos[0] = 0;
  if (viewPos[1] < 0) viewPos[1] = 0;
  if (viewPos[0] > (worldWidth - viewWidth)) viewPos[0] = worldWidth-viewWidth;
  if (viewPos[1] > (worldHeight - viewHeight)) viewPos[1] = worldHeight-viewHeight;
}
void Viewport::updateViewPos() {
  const float x0 = objectsToTrack[0]->getX() + objectsToTrack[0]->getScaledWidth() / 2;
  const float y0 = objectsToTrack[0]->getY() + objectsToTrack[0]->getScaledHeight() / 2;
  if (!objectsToTrack[1]) {
    viewPos[0] = x0 - viewWidth / 2;
    viewPos[1] = y0 - viewHeight / 2;
    return;
  }

  const float x1 = objectsToTrack[1]->getX() + objectsToTrack[1]->getScaledWidth() / 2;
  const float y1 = objectsToTrack[1]->getY() + objectsToTrack[1]->getScaledHeight() / 2;
  const float x = (x0 + x1) / 2.0;
  const float y = (y0 + y1) / 2.0;
  viewPos[0] = x - viewWidth / 2;
  viewPos[1] = y - viewHeight / 2;
}
