#ifndef MENU_H
#define MENU_H
#include <SDL.h>
#include <string>
#include "../ioMod.h"
#include "../../data/gameData.h"

class Menu {
public:
  Menu();

  void draw(const std::string&) const;

private:
  IoMod& io;
  Gamedata& data;
  SDL_Rect screen;
  SDL_Color boxColor, textColor;
};

#endif // MENU_H
