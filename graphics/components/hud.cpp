#include <SDL.h>
#include <sstream>
#include "hud.h"
#include "../ioMod.h"
#include "../../engine/clock.h"
#include "../../data/gameData.h"

Hud::Hud() :
  data(Gamedata::getInstance()),
  clock(Clock::getInstance()),
  io(IoMod::getInstance()),
  box{
    data.tryGetInt("hud/position/x", 30),
    data.tryGetInt("hud/position/y", 90),
    400,
    210
  },
  boxColor{
    data.tryGetChar("hud/boxColor/r", 0xff),
    data.tryGetChar("hud/boxColor/g", 0xff),
    data.tryGetChar("hud/boxColor/b", 0xff),
    data.tryGetChar("hud/boxColor/a", 0x7f)
  },
  textColor{
    data.tryGetChar("hud/textColor/r", 0),
    data.tryGetChar("hud/textColor/g", 0),
    data.tryGetChar("hud/textColor/b", 0),
    data.tryGetChar("hud/textColor/a", 0xff)
  } {}

void Hud::draw() const {
  SDL_Renderer * renderer = io.getRenderer();
  SDL_SetRenderDrawColor(renderer, boxColor.r, boxColor.g, boxColor.b, boxColor.a);
  SDL_RenderFillRect(renderer, &box);

  std::stringstream ss;
  ss << "FPS: " << clock.getFps();
  io.writeText(ss.str(), box.x + 20, box.y + 20, textColor);

  io.writeText("Controls: Left | Right | Jump | Portal | Shoot", box.x + 20, box.y + 50, textColor);
  io.writeText("Jeremy: A | D | W | S | E", box.x + 20, box.y + 70, textColor);
  io.writeText("Michael: Left | Right | Up | Down | /", box.x + 20, box.y + 90, textColor);

  io.writeText("Toggle God Mode: G", box.x + 20, box.y + 120, textColor);

  io.writeText("Kill all zombies to win", box.x + 20, box.y + 150, textColor);
  io.writeText("Either player dies to lose", box.x + 20, box.y + 170, textColor);
}
