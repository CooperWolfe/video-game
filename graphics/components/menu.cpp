#include <string>
#include "menu.h"
#include "../ioMod.h"
#include "../../data/gameData.h"

Menu::Menu() :
  io(IoMod::getInstance()),
  data(Gamedata::getInstance()),
  screen{ 0, 0, data.getInt("view/width"), data.getInt("view/height") },
  boxColor{
    data.tryGetChar("menu/boxColor/r", 0x34),
    data.tryGetChar("menu/boxColor/g", 0x34),
    data.tryGetChar("menu/boxColor/b", 0x34),
    data.tryGetChar("menu/boxColor/a", 0xff)
  },
  textColor{
    data.tryGetChar("menu/textColor/r", 0xff),
    data.tryGetChar("menu/textColor/g", 0xff),
    data.tryGetChar("menu/textColor/b", 0xff),
    data.tryGetChar("menu/textColor/a", 0xff)
  } {}

void Menu::draw(const std::string& message) const {
  SDL_Renderer * renderer = io.getRenderer();
  SDL_SetRenderDrawColor(renderer, boxColor.r, boxColor.g, boxColor.b, boxColor.a);
  SDL_RenderFillRect(renderer, &screen);

  io.writeText(message, 20, 20, textColor);
  io.writeText("Press enter to continue", 20, 50, textColor);
}
