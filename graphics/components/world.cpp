#include <iostream>
#include "world.h"
#include "../imageFactory.h"
#include "../../data/gameData.h"

World::World(const std::string& name) :
  image(ImageFactory::getInstance().getImage("background/" + name)),
  factor(Gamedata::getInstance().getFloat("background/" + name + "/factor")),
  dy(Gamedata::getInstance().getInt("world/height") - image->getHeight()),
  worldWidth(Gamedata::getInstance().getInt("world/width")),
  imageWidth(image->getWidth()),
  view(Viewport::getInstance()),
  viewX(0.0), viewY(0.0)
{ }

void World::update() {
  viewX = static_cast<int>(view.getX() / factor) % imageWidth;
  viewY = view.getY() - dy;
}

void World::draw() const {
  image->draw(0,0,-viewX,-viewY);
  image->draw(0,0,imageWidth-viewX,-viewY);
}
