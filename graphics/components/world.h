#include <string>
#include "image.h"
#include "../viewport.h"

class World {
public:
  World(const std::string& name);
  World(const World&) = delete;
  World& operator=(const World&) = delete;
  // ~World() { } // The image will be deleted by the FrameFactory
  void update();
  void draw() const;
private:
  const Image* const image;
  const float factor;
  const int dy;
  const unsigned worldWidth, imageWidth;
  const Viewport& view;
  float viewX, viewY;
};
