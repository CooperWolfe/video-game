#ifndef HUD_H
#define HUD_H
#include <SDL.h>
#include <sstream>
#include "../ioMod.h"
#include "../../engine/clock.h"
#include "../../data/gameData.h"

class Hud {
public:
  Hud();

  void draw() const;

private:
  const Gamedata& data;
  const Clock& clock;
  IoMod& io;
  SDL_Rect box;
  SDL_Color boxColor, textColor;
};

#endif // HUD_H
