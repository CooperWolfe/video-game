#include <cmath>
#include "projectilePool.h"
#include "../sprites/projectileSprite.h"

ProjectilePool::~ProjectilePool() {
  for (auto projectile : freeList) delete projectile;
  for (auto projectile : activeList) delete projectile;
}
ProjectilePool& ProjectilePool::operator=(const ProjectilePool& pool) {
  freeList = std::list<ProjectileSprite *>();
  activeList = std::list<ProjectileSprite *>();
  for (auto projectile : pool.freeList)
    freeList.push_back(new ProjectileSprite(*projectile));
  for (auto projectile : pool.activeList)
    activeList.push_back(new ProjectileSprite(*projectile));
  name = pool.name;
  max = pool.max;
  return *this;
}

void ProjectilePool::activate(const Vector2f& position, const bool left) {
  if (activeList.size() == max) return;

  ProjectileSprite * projectile;
  if (!freeList.empty()) {
    projectile = freeList.back();
    freeList.pop_back();
    projectile->toggleActive();
  }
  else projectile = new ProjectileSprite(name);
  projectile->setPosition(position);
  if (left) projectile->setX(projectile->getX() - projectile->getScaledWidth());
  projectile->setVelocityX(std::abs(projectile->getVelocityX()) * (left ? -1 : 1));
  activeList.push_back(projectile);
}
void ProjectilePool::draw() {
  for (auto projectile : activeList) projectile->draw();
}
void ProjectilePool::update(Uint32 ticks) {
  auto it = activeList.begin();
  while (it != activeList.end()) {
    (*it)->update(ticks);
    if ((*it)->isActive())
      ++it;
    else {
      freeList.push_back(*it);
      it = activeList.erase(it);
    }
  }
}
