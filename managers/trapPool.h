#ifndef TRAP_POOL_H
#define TRAP_POOL_H
#include <string>
#include <list>
#include "../sprites/multisprite.h"
#include "../math/vector2f.h"

class TrapPool {
public:
  TrapPool() = delete;
  TrapPool(const std::string& n, const unsigned m) : max(m), name(n), freeList(), activeList() {}
  ~TrapPool();
  TrapPool& operator=(const TrapPool&);

  void activate(const Vector2f&);
  void free(MultiSprite *);
  void draw();
  void update(Uint32);

  std::list<MultiSprite *> getFree() const {
    return freeList;
  }
  std::list<MultiSprite *> getActive() const {
    return activeList;
  }

private:
  unsigned max;
  std::string name;
  std::list<MultiSprite *> freeList;
  std::list<MultiSprite *> activeList;
};

#endif // TRAP_POOL_H
