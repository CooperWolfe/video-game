#ifndef PROJECTILE_POOL_H
#define PROJECTILE_POOL_H
#include <string>
#include <list>
#include "../sprites/projectileSprite.h"
#include "../math/vector2f.h"

class ProjectilePool {
public:
  ProjectilePool() = delete;
  ProjectilePool(const std::string& n, const unsigned m) : max(m), name(n), freeList(), activeList() {}
  ~ProjectilePool();
  ProjectilePool& operator=(const ProjectilePool&);

  void activate(const Vector2f&, const bool);
  void draw();
  void update(Uint32);

  std::list<ProjectileSprite *> getFree() const {
    return freeList;
  }
  std::list<ProjectileSprite *> getActive() const {
    return activeList;
  }

private:
  unsigned max;
  std::string name;
  std::list<ProjectileSprite *> freeList;
  std::list<ProjectileSprite *> activeList;
};

#endif // PROJECTILE_POOL_H
