#include <cmath>
#include "trapPool.h"
#include "../sprites/multisprite.h"
#include "../math/vector2f.h"

TrapPool::~TrapPool() {
  for (auto trap : freeList) delete trap;
  for (auto trap : activeList) delete trap;
}
TrapPool& TrapPool::operator=(const TrapPool& pool) {
  freeList = std::list<MultiSprite *>();
  activeList = std::list<MultiSprite *>();
  for (auto trap : pool.freeList)
    freeList.push_back(new MultiSprite(*trap));
  for (auto trap : pool.activeList)
    activeList.push_back(new MultiSprite(*trap));
  name = pool.name;
  max = pool.max;
  return *this;
}

void TrapPool::activate(const Vector2f& position) {
  if (activeList.size() == max) return;

  MultiSprite * trap;
  if (!freeList.empty()) {
    trap = freeList.back();
    freeList.pop_back();
  }
  else trap = new MultiSprite(name);
  trap->setPosition(position);
  activeList.push_back(trap);
}
void TrapPool::free(MultiSprite * ptr) {
  auto it = activeList.begin();
  while (it != activeList.end())
    if (*it == ptr) it = activeList.erase(it);
    else ++it;
  freeList.push_back(ptr);
}
void TrapPool::draw() {
  for (auto trap : activeList) trap->draw();
}
void TrapPool::update(Uint32 ticks) {
  for (auto trap : activeList) trap->update(ticks);
}
