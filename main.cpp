// Brian Malloy        Data-Driven Object oriented Game Construction
#include "engine/engine.h"

int main(int, char*[]) {
  try {
    bool done = false;
    while (!done) {
      Engine engine;
      done = engine.play();
    }
    std::cout << "Terminating program:\n\n";
  }
  catch (const string& msg) { std::cout << msg << std::endl; }
  catch (...) {
    std::cout << "Oops, someone threw an exception!" << std::endl;
  }
  return 0;
}
