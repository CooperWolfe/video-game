#include <iostream>
#include <algorithm>
#include <sstream>
#include <string>
#include <random>
#include <iomanip>
#include <SDL.h>
#include "../data/gameData.h"
#include "../sprites/sprite.h"
#include "../sprites/multisprite.h"
#include "../sprites/bidirectionalMultisprite.h"
#include "engine.h"
#include "../sprites/player.h"
#include "../sprites/enemy.h"

Engine::~Engine() {
  delete jeremy; delete michael;
  for (auto enemy : enemies) delete enemy;
  for (auto world : background) delete world;
}

Engine::Engine() :
  rc(&RenderContext::getInstance()),
  io(IoMod::getInstance()),
  clock(Clock::getInstance()),
  renderer(rc->getRenderer()),
  background{ new World("back"), new World("city"), new World("ground"), new World("dungeon"), new World("floor") },
  collisionStrategy(),
  viewport(Viewport::getInstance()),
  jeremy(new Player("Jeremy")),
  michael(new Player("Michael")),
  playerLocs(),
  currentSprite(0),
  godMode(false),
  enemies(),
  gameState(state::start)
{
  SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
  viewport.setObjectsToTrack(jeremy, michael);

  const unsigned zQuantity = Gamedata::getInstance().getInt("sprites/zombie/quantity");
  for (unsigned i = 0; i < zQuantity; ++i) enemies.emplace_back(new Enemy("zombie", playerLocs, 2));

  // Start audio
  SDL_Init(SDL_INIT_AUDIO);
  SDL_AudioSpec wavSpec; Uint32 wavLength; Uint8 * wavBuffer;
  SDL_LoadWAV("../assets/background.wav", &wavSpec, &wavBuffer, &wavLength);
  SDL_AudioDeviceID deviceId = SDL_OpenAudioDevice(NULL, 0, &wavSpec, NULL, 0);
  SDL_QueueAudio(deviceId, wavBuffer, wavLength);
  SDL_PauseAudioDevice(deviceId, 0);
}

void Engine::draw() const {
  for (World * layer : background) layer->draw();
  for (Enemy * enemy : enemies) enemy->draw();
  jeremy->draw(); michael->draw();

  viewport.draw(godMode, jeremy->getLives(), michael->getLives());

  SDL_RenderPresent(renderer);
}

void Engine::update(Uint32 ticks) {
  static unsigned iteration = 1;
  if (!(iteration++ % 15)) playerLocs.notify(new Vector2f[2] { jeremy->getPosition(), michael->getPosition() });
  if (iteration > 15) iteration = 1;

  jeremy->update(ticks); michael->update(ticks);
  for (auto enemy : enemies) enemy->update(ticks);
  for (auto layer : background) layer->update();

  // Keep this last
  viewport.update();
}

void Engine::checkForCollisions() {
  for (Enemy * enemy : enemies) {
    if (enemy->isExploding()) continue;
    if (collisionStrategy.execute(*jeremy, *enemy) && !godMode) jeremy->hurt();
    if (collisionStrategy.execute(*michael, *enemy) && !godMode) michael->hurt();
    for (auto bullet : jeremy->getBullets())
      if (bullet->isActive() && collisionStrategy.execute(*bullet, *enemy)) {
        enemy->hurt(bullet->getVelocityX() < 0);
        bullet->toggleActive();
      }
    for (auto bullet : michael->getBullets())
      if (bullet->isActive() && collisionStrategy.execute(*bullet, *enemy)) {
        enemy->hurt(bullet->getVelocityX() < 0);
        bullet->toggleActive();
      }
    for (auto trap : jeremy->getTraps())
      if (enemy->getX() > trap->getX() && (enemy->getX() + enemy->getScaledWidth()) < (trap->getX() + trap->getScaledWidth()) && enemy->getY() == 710) {
        enemy->kill();
        jeremy->removeTrap(trap);
      }
    for (auto trap : michael->getTraps())
      if (enemy->getX() > trap->getX() && (enemy->getX() + enemy->getScaledWidth()) < (trap->getX() + trap->getScaledWidth()) && enemy->getY() == 710) {
        enemy->kill();
        michael->removeTrap(trap);
      }
  }

  // Check for game lose
  if (jeremy->getLives() == 0 || michael->getLives() == 0)
    gameState = state::lose;
}

void Engine::removeDeadEnemies() {
  auto it = enemies.begin();
  while (it != enemies.end()) {
    if ((*it)->isDead()) {
      delete *it;
      it = enemies.erase(it);
    }
    else ++it;
  }

  // Check for game win
  if (enemies.size() == 0)
    gameState = state::win;
}

bool Engine::play() {
  SDL_Event event;
  const Uint8* keystate;
  bool done = false;
  Uint32 ticks = clock.getElapsedTicks();

  while (!done) {
    // The next loop polls for events, guarding against key bounce:
    while (SDL_PollEvent(&event)) {
      keystate = SDL_GetKeyboardState(NULL);
      if (event.type == SDL_QUIT) { done = true; break; }
      if (event.type == SDL_KEYDOWN) {
        if (keystate[SDL_SCANCODE_ESCAPE] || keystate[SDL_SCANCODE_Q])
          return done = true;

        if (gameState != state::play) {
          if (keystate[SDL_SCANCODE_RETURN]) {
            if (gameState != state::start) return false;
            gameState = state::play;
          }
          break;
        }

        if (keystate[SDL_SCANCODE_E]) jeremy->shoot();
        if (keystate[SDL_SCANCODE_S]) jeremy->dropPortal();
        if (keystate[SDL_SCANCODE_SLASH]) michael->shoot();
        if (keystate[SDL_SCANCODE_DOWN]) michael->dropPortal();

        if (keystate[SDL_SCANCODE_G]) godMode = !godMode;
        if (keystate[SDL_SCANCODE_F1]) viewport.toggleHud();
        if (keystate[SDL_SCANCODE_P]) {
          if (clock.isPaused()) clock.unpause();
          else clock.pause();
        }
      }
    }

    // In this section of the event loop we allow key bounce:
    ticks = clock.getElapsedTicks();
    if (ticks > 0) {
      clock.incrFrame();
      if (keystate[SDL_SCANCODE_A]) jeremy->left();
      if (keystate[SDL_SCANCODE_D]) jeremy->right();
      if (keystate[SDL_SCANCODE_W]) jeremy->jump();
      if (keystate[SDL_SCANCODE_LEFT]) michael->left();
      if (keystate[SDL_SCANCODE_RIGHT]) michael->right();
      if (keystate[SDL_SCANCODE_UP]) michael->jump();

      if (gameState != state::play) {
        viewport.draw(gameState);
        SDL_RenderPresent(renderer);
        continue;
      }

      checkForCollisions();
      removeDeadEnemies();
      draw();
      update(ticks);
    }
  }

  return true;
}
