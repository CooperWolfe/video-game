#ifndef COLLISION_STRATEGY_H
#define COLLISION_STRATEGY_H
#include "../drawable.h"

class CollisionStrategy {
public:
  virtual bool execute(const Drawable&, const Drawable&) const = 0;
  virtual void draw() const = 0;
  virtual ~CollisionStrategy() {}
};

#endif // COLLISION_STRATEGY_H
