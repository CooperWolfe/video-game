#ifndef RECTANGULAR_COLLISION_STRATEGY_H
#define RECTANGULAR_COLLISION_STRATEGY_H
#include "collisionStrategy.h"
#include "../drawable.h"

class RectangularCollisionStrategy : public CollisionStrategy {
public:
  RectangularCollisionStrategy() {}
  virtual bool execute(const Drawable&, const Drawable&) const;
  virtual void draw() const;
};

#endif // RECTANGULAR_COLLISION_STRATEGY_H
