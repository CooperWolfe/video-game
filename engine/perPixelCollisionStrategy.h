#ifndef PER_PIXEL_COLLISION_STRATEGY_H
#define PER_PIXEL_COLLISION_STRATEGY_H

#include <SDL.h>
#include "collisionStrategy.h"
#include "../drawable.h"

class PerPixelCollisionStrategy : public CollisionStrategy {
public:
  PerPixelCollisionStrategy() {}
  virtual bool execute(const Drawable&, const Drawable&) const;
  virtual void draw() const;
private:
  bool isVisible(Uint32, SDL_Surface*) const;
};

#endif // PER_PIXEL_COLLISION_STRATEGY_H
