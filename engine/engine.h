#include <vector>
#include <SDL.h>
#include "../graphics/ioMod.h"
#include "../graphics/renderContext.h"
#include "clock.h"
#include "../graphics/components/world.h"
#include "../graphics/viewport.h"
#include "../sprites/enemy.h"
#include "../sprites/player.h"
#include "../observable/observable.h"
#include "../math/vector2f.h"
#include "perPixelCollisionStrategy.h"
#include "../enums/state.h"

class Engine {
public:
  Engine ();
  ~Engine ();
  Engine(const Engine&) = delete;
  Engine& operator=(const Engine&) = delete;
  bool play();

private:
  const RenderContext * rc;
  const IoMod& io;
  Clock& clock;

  SDL_Renderer * const renderer;
  World * background[5];
  const PerPixelCollisionStrategy collisionStrategy;
  Viewport& viewport;

  Player * jeremy;
  Player * michael;
  Observable<Vector2f *> playerLocs;
  int currentSprite;
  bool godMode;
  std::vector<Enemy *> enemies;

  state gameState;

  void draw() const;
  void update(Uint32);

  void printScales() const;
  void checkForCollisions();
  void removeDeadEnemies();
  void begin();
};
