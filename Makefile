CC := g++
CFLAGS := -I/usr/include/SDL2 -D_REENTRANT -g -W -Wall -Werror -std=c++11 -Weffc++ -Wextra -pedantic -O0 -I /usr/include/
LDFLAGS := -L/usr/lib/x86_64-linux-gnu -lSDL2 -lm -lexpat -lSDL2_ttf -lSDL2_image
EXEC := run

# OBJDIR MUST BE SET AND SET NOT TO A DIRECTORY
# CONTAINING IMPORTANT FILES !! SEE TARGET `CLEAN` FOR REASONING
OBJDIR := .os/
OBJS := \
	$(OBJDIR)gameData.o \
	$(OBJDIR)parseXML.o \
	$(OBJDIR)clock.o \
	$(OBJDIR)engine.o \
	$(OBJDIR)perPixelCollisionStrategy.o \
	$(OBJDIR)rectangularCollisionStrategy.o \
	$(OBJDIR)state.o \
	$(OBJDIR)hud.o \
	$(OBJDIR)image.o \
	$(OBJDIR)world.o \
	$(OBJDIR)menu.o \
	$(OBJDIR)imageFactory.o \
	$(OBJDIR)ioMod.o \
	$(OBJDIR)renderContext.o \
	$(OBJDIR)viewport.o \
	$(OBJDIR)main.o \
	$(OBJDIR)projectilePool.o \
	$(OBJDIR)trapPool.o \
	$(OBJDIR)vector2f.o \
	$(OBJDIR)observable.o \
	$(OBJDIR)bidirectionalMultisprite.o \
	$(OBJDIR)chunk.o \
	$(OBJDIR)enemy.o \
	$(OBJDIR)explodingSprite.o \
	$(OBJDIR)multisprite.o \
	$(OBJDIR)player.o \
	$(OBJDIR)projectileSprite.o \
	$(OBJDIR)sprite.o \
	$(OBJDIR)spriteSheet.o \
	$(OBJDIR)walkingSprite.o \

compile: $(OBJS)
	$(CC) $(OBJS) -o $(EXEC) $(LDFLAGS)

$(OBJDIR)gameData.o: data/gameData.cpp /usr/include/stdc-predef.h \
 /usr/include/c++/5/sstream /usr/include/c++/5/istream \
 /usr/include/c++/5/ios /usr/include/c++/5/iosfwd \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/os_defines.h \
 /usr/include/features.h /usr/include/x86_64-linux-gnu/sys/cdefs.h \
 /usr/include/x86_64-linux-gnu/bits/wordsize.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs-64.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/cpu_defines.h \
 /usr/include/c++/5/bits/stringfwd.h /usr/include/c++/5/bits/memoryfwd.h \
 /usr/include/c++/5/bits/postypes.h /usr/include/c++/5/cwchar \
 /usr/include/wchar.h /usr/include/stdio.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdarg.h \
 /usr/include/x86_64-linux-gnu/bits/wchar.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h /usr/include/xlocale.h \
 /usr/include/c++/5/exception \
 /usr/include/c++/5/bits/atomic_lockfree_defines.h \
 /usr/include/c++/5/bits/exception_ptr.h \
 /usr/include/c++/5/bits/exception_defines.h \
 /usr/include/c++/5/bits/nested_exception.h \
 /usr/include/c++/5/bits/char_traits.h \
 /usr/include/c++/5/bits/stl_algobase.h \
 /usr/include/c++/5/bits/functexcept.h \
 /usr/include/c++/5/bits/cpp_type_traits.h \
 /usr/include/c++/5/ext/type_traits.h \
 /usr/include/c++/5/ext/numeric_traits.h \
 /usr/include/c++/5/bits/stl_pair.h /usr/include/c++/5/bits/move.h \
 /usr/include/c++/5/bits/concept_check.h /usr/include/c++/5/type_traits \
 /usr/include/c++/5/bits/stl_iterator_base_types.h \
 /usr/include/c++/5/bits/stl_iterator_base_funcs.h \
 /usr/include/c++/5/debug/debug.h /usr/include/c++/5/bits/stl_iterator.h \
 /usr/include/c++/5/bits/ptr_traits.h \
 /usr/include/c++/5/bits/predefined_ops.h /usr/include/c++/5/cstdint \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdint.h /usr/include/stdint.h \
 /usr/include/c++/5/bits/localefwd.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++locale.h \
 /usr/include/c++/5/clocale /usr/include/locale.h \
 /usr/include/x86_64-linux-gnu/bits/locale.h /usr/include/c++/5/cctype \
 /usr/include/ctype.h /usr/include/x86_64-linux-gnu/bits/types.h \
 /usr/include/x86_64-linux-gnu/bits/typesizes.h /usr/include/endian.h \
 /usr/include/x86_64-linux-gnu/bits/endian.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap-16.h \
 /usr/include/c++/5/bits/ios_base.h /usr/include/c++/5/ext/atomicity.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr-default.h \
 /usr/include/pthread.h /usr/include/sched.h /usr/include/time.h \
 /usr/include/x86_64-linux-gnu/bits/sched.h \
 /usr/include/x86_64-linux-gnu/bits/time.h \
 /usr/include/x86_64-linux-gnu/bits/timex.h \
 /usr/include/x86_64-linux-gnu/bits/pthreadtypes.h \
 /usr/include/x86_64-linux-gnu/bits/setjmp.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h \
 /usr/include/c++/5/bits/locale_classes.h /usr/include/c++/5/string \
 /usr/include/c++/5/bits/allocator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++allocator.h \
 /usr/include/c++/5/ext/new_allocator.h /usr/include/c++/5/new \
 /usr/include/c++/5/bits/ostream_insert.h \
 /usr/include/c++/5/bits/cxxabi_forced.h \
 /usr/include/c++/5/bits/stl_function.h \
 /usr/include/c++/5/backward/binders.h \
 /usr/include/c++/5/bits/range_access.h \
 /usr/include/c++/5/initializer_list \
 /usr/include/c++/5/bits/basic_string.h \
 /usr/include/c++/5/ext/alloc_traits.h \
 /usr/include/c++/5/bits/alloc_traits.h \
 /usr/include/c++/5/ext/string_conversions.h /usr/include/c++/5/cstdlib \
 /usr/include/stdlib.h /usr/include/x86_64-linux-gnu/bits/waitflags.h \
 /usr/include/x86_64-linux-gnu/bits/waitstatus.h \
 /usr/include/x86_64-linux-gnu/sys/types.h \
 /usr/include/x86_64-linux-gnu/sys/select.h \
 /usr/include/x86_64-linux-gnu/bits/select.h \
 /usr/include/x86_64-linux-gnu/bits/sigset.h \
 /usr/include/x86_64-linux-gnu/sys/sysmacros.h /usr/include/alloca.h \
 /usr/include/x86_64-linux-gnu/bits/stdlib-float.h \
 /usr/include/c++/5/cstdio /usr/include/libio.h /usr/include/_G_config.h \
 /usr/include/x86_64-linux-gnu/bits/stdio_lim.h \
 /usr/include/x86_64-linux-gnu/bits/sys_errlist.h \
 /usr/include/c++/5/cerrno /usr/include/errno.h \
 /usr/include/x86_64-linux-gnu/bits/errno.h /usr/include/linux/errno.h \
 /usr/include/x86_64-linux-gnu/asm/errno.h \
 /usr/include/asm-generic/errno.h /usr/include/asm-generic/errno-base.h \
 /usr/include/c++/5/bits/functional_hash.h \
 /usr/include/c++/5/bits/hash_bytes.h \
 /usr/include/c++/5/bits/basic_string.tcc \
 /usr/include/c++/5/bits/locale_classes.tcc \
 /usr/include/c++/5/system_error \
 /usr/include/x86_64-linux-gnu/c++/5/bits/error_constants.h \
 /usr/include/c++/5/stdexcept /usr/include/c++/5/streambuf \
 /usr/include/c++/5/bits/streambuf.tcc \
 /usr/include/c++/5/bits/basic_ios.h \
 /usr/include/c++/5/bits/locale_facets.h /usr/include/c++/5/cwctype \
 /usr/include/wctype.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_base.h \
 /usr/include/c++/5/bits/streambuf_iterator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_inline.h \
 /usr/include/c++/5/bits/locale_facets.tcc \
 /usr/include/c++/5/bits/basic_ios.tcc /usr/include/c++/5/ostream \
 /usr/include/c++/5/bits/ostream.tcc /usr/include/c++/5/bits/istream.tcc \
 /usr/include/c++/5/bits/sstream.tcc /usr/include/c++/5/iostream \
 /usr/include/c++/5/limits /usr/include/c++/5/cmath /usr/include/math.h \
 /usr/include/x86_64-linux-gnu/bits/math-vector.h \
 /usr/include/x86_64-linux-gnu/bits/libm-simd-decl-stubs.h \
 /usr/include/x86_64-linux-gnu/bits/huge_val.h \
 /usr/include/x86_64-linux-gnu/bits/huge_valf.h \
 /usr/include/x86_64-linux-gnu/bits/huge_vall.h \
 /usr/include/x86_64-linux-gnu/bits/inf.h \
 /usr/include/x86_64-linux-gnu/bits/nan.h \
 /usr/include/x86_64-linux-gnu/bits/mathdef.h \
 /usr/include/x86_64-linux-gnu/bits/mathcalls.h data/gameData.h \
 /usr/include/c++/5/map /usr/include/c++/5/bits/stl_tree.h \
 /usr/include/c++/5/ext/aligned_buffer.h \
 /usr/include/c++/5/bits/stl_map.h /usr/include/c++/5/tuple \
 /usr/include/c++/5/utility /usr/include/c++/5/bits/stl_relops.h \
 /usr/include/c++/5/array /usr/include/c++/5/bits/uses_allocator.h \
 /usr/include/c++/5/bits/stl_multimap.h data/../math/vector2f.h \
 data/parseXML.h /usr/include/c++/5/fstream \
 /usr/include/c++/5/bits/codecvt.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/basic_file.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++io.h \
 /usr/include/c++/5/bits/fstream.tcc /usr/include/c++/5/deque \
 /usr/include/c++/5/bits/stl_construct.h \
 /usr/include/c++/5/bits/stl_uninitialized.h \
 /usr/include/c++/5/bits/stl_deque.h /usr/include/c++/5/bits/deque.tcc \
 /usr/include/expat.h /usr/include/expat_external.h
	$(CC) -c $< -o $@ $(CFLAGS)
$(OBJDIR)parseXML.o: data/parseXML.cpp /usr/include/stdc-predef.h \
 /usr/include/c++/5/cstring \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/os_defines.h \
 /usr/include/features.h /usr/include/x86_64-linux-gnu/sys/cdefs.h \
 /usr/include/x86_64-linux-gnu/bits/wordsize.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs-64.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/cpu_defines.h \
 /usr/include/string.h /usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h \
 /usr/include/xlocale.h /usr/include/c++/5/sstream \
 /usr/include/c++/5/istream /usr/include/c++/5/ios \
 /usr/include/c++/5/iosfwd /usr/include/c++/5/bits/stringfwd.h \
 /usr/include/c++/5/bits/memoryfwd.h /usr/include/c++/5/bits/postypes.h \
 /usr/include/c++/5/cwchar /usr/include/wchar.h /usr/include/stdio.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdarg.h \
 /usr/include/x86_64-linux-gnu/bits/wchar.h /usr/include/c++/5/exception \
 /usr/include/c++/5/bits/atomic_lockfree_defines.h \
 /usr/include/c++/5/bits/exception_ptr.h \
 /usr/include/c++/5/bits/exception_defines.h \
 /usr/include/c++/5/bits/nested_exception.h \
 /usr/include/c++/5/bits/char_traits.h \
 /usr/include/c++/5/bits/stl_algobase.h \
 /usr/include/c++/5/bits/functexcept.h \
 /usr/include/c++/5/bits/cpp_type_traits.h \
 /usr/include/c++/5/ext/type_traits.h \
 /usr/include/c++/5/ext/numeric_traits.h \
 /usr/include/c++/5/bits/stl_pair.h /usr/include/c++/5/bits/move.h \
 /usr/include/c++/5/bits/concept_check.h /usr/include/c++/5/type_traits \
 /usr/include/c++/5/bits/stl_iterator_base_types.h \
 /usr/include/c++/5/bits/stl_iterator_base_funcs.h \
 /usr/include/c++/5/debug/debug.h /usr/include/c++/5/bits/stl_iterator.h \
 /usr/include/c++/5/bits/ptr_traits.h \
 /usr/include/c++/5/bits/predefined_ops.h /usr/include/c++/5/cstdint \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdint.h /usr/include/stdint.h \
 /usr/include/c++/5/bits/localefwd.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++locale.h \
 /usr/include/c++/5/clocale /usr/include/locale.h \
 /usr/include/x86_64-linux-gnu/bits/locale.h /usr/include/c++/5/cctype \
 /usr/include/ctype.h /usr/include/x86_64-linux-gnu/bits/types.h \
 /usr/include/x86_64-linux-gnu/bits/typesizes.h /usr/include/endian.h \
 /usr/include/x86_64-linux-gnu/bits/endian.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap-16.h \
 /usr/include/c++/5/bits/ios_base.h /usr/include/c++/5/ext/atomicity.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr-default.h \
 /usr/include/pthread.h /usr/include/sched.h /usr/include/time.h \
 /usr/include/x86_64-linux-gnu/bits/sched.h \
 /usr/include/x86_64-linux-gnu/bits/time.h \
 /usr/include/x86_64-linux-gnu/bits/timex.h \
 /usr/include/x86_64-linux-gnu/bits/pthreadtypes.h \
 /usr/include/x86_64-linux-gnu/bits/setjmp.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h \
 /usr/include/c++/5/bits/locale_classes.h /usr/include/c++/5/string \
 /usr/include/c++/5/bits/allocator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++allocator.h \
 /usr/include/c++/5/ext/new_allocator.h /usr/include/c++/5/new \
 /usr/include/c++/5/bits/ostream_insert.h \
 /usr/include/c++/5/bits/cxxabi_forced.h \
 /usr/include/c++/5/bits/stl_function.h \
 /usr/include/c++/5/backward/binders.h \
 /usr/include/c++/5/bits/range_access.h \
 /usr/include/c++/5/initializer_list \
 /usr/include/c++/5/bits/basic_string.h \
 /usr/include/c++/5/ext/alloc_traits.h \
 /usr/include/c++/5/bits/alloc_traits.h \
 /usr/include/c++/5/ext/string_conversions.h /usr/include/c++/5/cstdlib \
 /usr/include/stdlib.h /usr/include/x86_64-linux-gnu/bits/waitflags.h \
 /usr/include/x86_64-linux-gnu/bits/waitstatus.h \
 /usr/include/x86_64-linux-gnu/sys/types.h \
 /usr/include/x86_64-linux-gnu/sys/select.h \
 /usr/include/x86_64-linux-gnu/bits/select.h \
 /usr/include/x86_64-linux-gnu/bits/sigset.h \
 /usr/include/x86_64-linux-gnu/sys/sysmacros.h /usr/include/alloca.h \
 /usr/include/x86_64-linux-gnu/bits/stdlib-float.h \
 /usr/include/c++/5/cstdio /usr/include/libio.h /usr/include/_G_config.h \
 /usr/include/x86_64-linux-gnu/bits/stdio_lim.h \
 /usr/include/x86_64-linux-gnu/bits/sys_errlist.h \
 /usr/include/c++/5/cerrno /usr/include/errno.h \
 /usr/include/x86_64-linux-gnu/bits/errno.h /usr/include/linux/errno.h \
 /usr/include/x86_64-linux-gnu/asm/errno.h \
 /usr/include/asm-generic/errno.h /usr/include/asm-generic/errno-base.h \
 /usr/include/c++/5/bits/functional_hash.h \
 /usr/include/c++/5/bits/hash_bytes.h \
 /usr/include/c++/5/bits/basic_string.tcc \
 /usr/include/c++/5/bits/locale_classes.tcc \
 /usr/include/c++/5/system_error \
 /usr/include/x86_64-linux-gnu/c++/5/bits/error_constants.h \
 /usr/include/c++/5/stdexcept /usr/include/c++/5/streambuf \
 /usr/include/c++/5/bits/streambuf.tcc \
 /usr/include/c++/5/bits/basic_ios.h \
 /usr/include/c++/5/bits/locale_facets.h /usr/include/c++/5/cwctype \
 /usr/include/wctype.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_base.h \
 /usr/include/c++/5/bits/streambuf_iterator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_inline.h \
 /usr/include/c++/5/bits/locale_facets.tcc \
 /usr/include/c++/5/bits/basic_ios.tcc /usr/include/c++/5/ostream \
 /usr/include/c++/5/bits/ostream.tcc /usr/include/c++/5/bits/istream.tcc \
 /usr/include/c++/5/bits/sstream.tcc data/parseXML.h \
 /usr/include/c++/5/iostream /usr/include/c++/5/fstream \
 /usr/include/c++/5/bits/codecvt.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/basic_file.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++io.h \
 /usr/include/c++/5/bits/fstream.tcc /usr/include/c++/5/map \
 /usr/include/c++/5/bits/stl_tree.h \
 /usr/include/c++/5/ext/aligned_buffer.h \
 /usr/include/c++/5/bits/stl_map.h /usr/include/c++/5/tuple \
 /usr/include/c++/5/utility /usr/include/c++/5/bits/stl_relops.h \
 /usr/include/c++/5/array /usr/include/c++/5/bits/uses_allocator.h \
 /usr/include/c++/5/bits/stl_multimap.h /usr/include/c++/5/deque \
 /usr/include/c++/5/bits/stl_construct.h \
 /usr/include/c++/5/bits/stl_uninitialized.h \
 /usr/include/c++/5/bits/stl_deque.h /usr/include/c++/5/bits/deque.tcc \
 /usr/include/expat.h /usr/include/expat_external.h
	$(CC) -c $< -o $@ $(CFLAGS)
$(OBJDIR)clock.o: engine/clock.cpp /usr/include/stdc-predef.h \
 /usr/include/c++/5/cmath \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/os_defines.h \
 /usr/include/features.h /usr/include/x86_64-linux-gnu/sys/cdefs.h \
 /usr/include/x86_64-linux-gnu/bits/wordsize.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs-64.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/cpu_defines.h \
 /usr/include/c++/5/bits/cpp_type_traits.h \
 /usr/include/c++/5/ext/type_traits.h /usr/include/math.h \
 /usr/include/x86_64-linux-gnu/bits/math-vector.h \
 /usr/include/x86_64-linux-gnu/bits/libm-simd-decl-stubs.h \
 /usr/include/x86_64-linux-gnu/bits/huge_val.h \
 /usr/include/x86_64-linux-gnu/bits/huge_valf.h \
 /usr/include/x86_64-linux-gnu/bits/huge_vall.h \
 /usr/include/x86_64-linux-gnu/bits/inf.h \
 /usr/include/x86_64-linux-gnu/bits/nan.h \
 /usr/include/x86_64-linux-gnu/bits/mathdef.h \
 /usr/include/x86_64-linux-gnu/bits/mathcalls.h \
 /usr/include/c++/5/iostream /usr/include/c++/5/ostream \
 /usr/include/c++/5/ios /usr/include/c++/5/iosfwd \
 /usr/include/c++/5/bits/stringfwd.h /usr/include/c++/5/bits/memoryfwd.h \
 /usr/include/c++/5/bits/postypes.h /usr/include/c++/5/cwchar \
 /usr/include/wchar.h /usr/include/stdio.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdarg.h \
 /usr/include/x86_64-linux-gnu/bits/wchar.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h /usr/include/xlocale.h \
 /usr/include/c++/5/exception \
 /usr/include/c++/5/bits/atomic_lockfree_defines.h \
 /usr/include/c++/5/bits/exception_ptr.h \
 /usr/include/c++/5/bits/exception_defines.h \
 /usr/include/c++/5/bits/nested_exception.h \
 /usr/include/c++/5/bits/char_traits.h \
 /usr/include/c++/5/bits/stl_algobase.h \
 /usr/include/c++/5/bits/functexcept.h \
 /usr/include/c++/5/ext/numeric_traits.h \
 /usr/include/c++/5/bits/stl_pair.h /usr/include/c++/5/bits/move.h \
 /usr/include/c++/5/bits/concept_check.h /usr/include/c++/5/type_traits \
 /usr/include/c++/5/bits/stl_iterator_base_types.h \
 /usr/include/c++/5/bits/stl_iterator_base_funcs.h \
 /usr/include/c++/5/debug/debug.h /usr/include/c++/5/bits/stl_iterator.h \
 /usr/include/c++/5/bits/ptr_traits.h \
 /usr/include/c++/5/bits/predefined_ops.h /usr/include/c++/5/cstdint \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdint.h /usr/include/stdint.h \
 /usr/include/c++/5/bits/localefwd.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++locale.h \
 /usr/include/c++/5/clocale /usr/include/locale.h \
 /usr/include/x86_64-linux-gnu/bits/locale.h /usr/include/c++/5/cctype \
 /usr/include/ctype.h /usr/include/x86_64-linux-gnu/bits/types.h \
 /usr/include/x86_64-linux-gnu/bits/typesizes.h /usr/include/endian.h \
 /usr/include/x86_64-linux-gnu/bits/endian.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap-16.h \
 /usr/include/c++/5/bits/ios_base.h /usr/include/c++/5/ext/atomicity.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr-default.h \
 /usr/include/pthread.h /usr/include/sched.h /usr/include/time.h \
 /usr/include/x86_64-linux-gnu/bits/sched.h \
 /usr/include/x86_64-linux-gnu/bits/time.h \
 /usr/include/x86_64-linux-gnu/bits/timex.h \
 /usr/include/x86_64-linux-gnu/bits/pthreadtypes.h \
 /usr/include/x86_64-linux-gnu/bits/setjmp.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h \
 /usr/include/c++/5/bits/locale_classes.h /usr/include/c++/5/string \
 /usr/include/c++/5/bits/allocator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++allocator.h \
 /usr/include/c++/5/ext/new_allocator.h /usr/include/c++/5/new \
 /usr/include/c++/5/bits/ostream_insert.h \
 /usr/include/c++/5/bits/cxxabi_forced.h \
 /usr/include/c++/5/bits/stl_function.h \
 /usr/include/c++/5/backward/binders.h \
 /usr/include/c++/5/bits/range_access.h \
 /usr/include/c++/5/initializer_list \
 /usr/include/c++/5/bits/basic_string.h \
 /usr/include/c++/5/ext/alloc_traits.h \
 /usr/include/c++/5/bits/alloc_traits.h \
 /usr/include/c++/5/ext/string_conversions.h /usr/include/c++/5/cstdlib \
 /usr/include/stdlib.h /usr/include/x86_64-linux-gnu/bits/waitflags.h \
 /usr/include/x86_64-linux-gnu/bits/waitstatus.h \
 /usr/include/x86_64-linux-gnu/sys/types.h \
 /usr/include/x86_64-linux-gnu/sys/select.h \
 /usr/include/x86_64-linux-gnu/bits/select.h \
 /usr/include/x86_64-linux-gnu/bits/sigset.h \
 /usr/include/x86_64-linux-gnu/sys/sysmacros.h /usr/include/alloca.h \
 /usr/include/x86_64-linux-gnu/bits/stdlib-float.h \
 /usr/include/c++/5/cstdio /usr/include/libio.h /usr/include/_G_config.h \
 /usr/include/x86_64-linux-gnu/bits/stdio_lim.h \
 /usr/include/x86_64-linux-gnu/bits/sys_errlist.h \
 /usr/include/c++/5/cerrno /usr/include/errno.h \
 /usr/include/x86_64-linux-gnu/bits/errno.h /usr/include/linux/errno.h \
 /usr/include/x86_64-linux-gnu/asm/errno.h \
 /usr/include/asm-generic/errno.h /usr/include/asm-generic/errno-base.h \
 /usr/include/c++/5/bits/functional_hash.h \
 /usr/include/c++/5/bits/hash_bytes.h \
 /usr/include/c++/5/bits/basic_string.tcc \
 /usr/include/c++/5/bits/locale_classes.tcc \
 /usr/include/c++/5/system_error \
 /usr/include/x86_64-linux-gnu/c++/5/bits/error_constants.h \
 /usr/include/c++/5/stdexcept /usr/include/c++/5/streambuf \
 /usr/include/c++/5/bits/streambuf.tcc \
 /usr/include/c++/5/bits/basic_ios.h \
 /usr/include/c++/5/bits/locale_facets.h /usr/include/c++/5/cwctype \
 /usr/include/wctype.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_base.h \
 /usr/include/c++/5/bits/streambuf_iterator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_inline.h \
 /usr/include/c++/5/bits/locale_facets.tcc \
 /usr/include/c++/5/bits/basic_ios.tcc \
 /usr/include/c++/5/bits/ostream.tcc /usr/include/c++/5/istream \
 /usr/include/c++/5/bits/istream.tcc /usr/include/c++/5/sstream \
 /usr/include/c++/5/bits/sstream.tcc engine/clock.h  \
 engine/../data/gameData.h /usr/include/c++/5/map \
 /usr/include/c++/5/bits/stl_tree.h \
 /usr/include/c++/5/ext/aligned_buffer.h \
 /usr/include/c++/5/bits/stl_map.h /usr/include/c++/5/tuple \
 /usr/include/c++/5/utility /usr/include/c++/5/bits/stl_relops.h \
 /usr/include/c++/5/array /usr/include/c++/5/bits/uses_allocator.h \
 /usr/include/c++/5/bits/stl_multimap.h engine/../data/../math/vector2f.h \
 engine/../data/parseXML.h /usr/include/c++/5/fstream \
 /usr/include/c++/5/bits/codecvt.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/basic_file.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++io.h \
 /usr/include/c++/5/bits/fstream.tcc /usr/include/c++/5/deque \
 /usr/include/c++/5/bits/stl_construct.h \
 /usr/include/c++/5/bits/stl_uninitialized.h \
 /usr/include/c++/5/bits/stl_deque.h /usr/include/c++/5/bits/deque.tcc \
 /usr/include/expat.h /usr/include/expat_external.h \
 engine/../graphics/ioMod.h 
	$(CC) -c $< -o $@ $(CFLAGS)
$(OBJDIR)engine.o: engine/engine.cpp /usr/include/stdc-predef.h \
 /usr/include/c++/5/iostream \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/os_defines.h \
 /usr/include/features.h /usr/include/x86_64-linux-gnu/sys/cdefs.h \
 /usr/include/x86_64-linux-gnu/bits/wordsize.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs-64.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/cpu_defines.h \
 /usr/include/c++/5/ostream /usr/include/c++/5/ios \
 /usr/include/c++/5/iosfwd /usr/include/c++/5/bits/stringfwd.h \
 /usr/include/c++/5/bits/memoryfwd.h /usr/include/c++/5/bits/postypes.h \
 /usr/include/c++/5/cwchar /usr/include/wchar.h /usr/include/stdio.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdarg.h \
 /usr/include/x86_64-linux-gnu/bits/wchar.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h /usr/include/xlocale.h \
 /usr/include/c++/5/exception \
 /usr/include/c++/5/bits/atomic_lockfree_defines.h \
 /usr/include/c++/5/bits/exception_ptr.h \
 /usr/include/c++/5/bits/exception_defines.h \
 /usr/include/c++/5/bits/nested_exception.h \
 /usr/include/c++/5/bits/char_traits.h \
 /usr/include/c++/5/bits/stl_algobase.h \
 /usr/include/c++/5/bits/functexcept.h \
 /usr/include/c++/5/bits/cpp_type_traits.h \
 /usr/include/c++/5/ext/type_traits.h \
 /usr/include/c++/5/ext/numeric_traits.h \
 /usr/include/c++/5/bits/stl_pair.h /usr/include/c++/5/bits/move.h \
 /usr/include/c++/5/bits/concept_check.h /usr/include/c++/5/type_traits \
 /usr/include/c++/5/bits/stl_iterator_base_types.h \
 /usr/include/c++/5/bits/stl_iterator_base_funcs.h \
 /usr/include/c++/5/debug/debug.h /usr/include/c++/5/bits/stl_iterator.h \
 /usr/include/c++/5/bits/ptr_traits.h \
 /usr/include/c++/5/bits/predefined_ops.h /usr/include/c++/5/cstdint \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdint.h /usr/include/stdint.h \
 /usr/include/c++/5/bits/localefwd.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++locale.h \
 /usr/include/c++/5/clocale /usr/include/locale.h \
 /usr/include/x86_64-linux-gnu/bits/locale.h /usr/include/c++/5/cctype \
 /usr/include/ctype.h /usr/include/x86_64-linux-gnu/bits/types.h \
 /usr/include/x86_64-linux-gnu/bits/typesizes.h /usr/include/endian.h \
 /usr/include/x86_64-linux-gnu/bits/endian.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap-16.h \
 /usr/include/c++/5/bits/ios_base.h /usr/include/c++/5/ext/atomicity.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr-default.h \
 /usr/include/pthread.h /usr/include/sched.h /usr/include/time.h \
 /usr/include/x86_64-linux-gnu/bits/sched.h \
 /usr/include/x86_64-linux-gnu/bits/time.h \
 /usr/include/x86_64-linux-gnu/bits/timex.h \
 /usr/include/x86_64-linux-gnu/bits/pthreadtypes.h \
 /usr/include/x86_64-linux-gnu/bits/setjmp.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h \
 /usr/include/c++/5/bits/locale_classes.h /usr/include/c++/5/string \
 /usr/include/c++/5/bits/allocator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++allocator.h \
 /usr/include/c++/5/ext/new_allocator.h /usr/include/c++/5/new \
 /usr/include/c++/5/bits/ostream_insert.h \
 /usr/include/c++/5/bits/cxxabi_forced.h \
 /usr/include/c++/5/bits/stl_function.h \
 /usr/include/c++/5/backward/binders.h \
 /usr/include/c++/5/bits/range_access.h \
 /usr/include/c++/5/initializer_list \
 /usr/include/c++/5/bits/basic_string.h \
 /usr/include/c++/5/ext/alloc_traits.h \
 /usr/include/c++/5/bits/alloc_traits.h \
 /usr/include/c++/5/ext/string_conversions.h /usr/include/c++/5/cstdlib \
 /usr/include/stdlib.h /usr/include/x86_64-linux-gnu/bits/waitflags.h \
 /usr/include/x86_64-linux-gnu/bits/waitstatus.h \
 /usr/include/x86_64-linux-gnu/sys/types.h \
 /usr/include/x86_64-linux-gnu/sys/select.h \
 /usr/include/x86_64-linux-gnu/bits/select.h \
 /usr/include/x86_64-linux-gnu/bits/sigset.h \
 /usr/include/x86_64-linux-gnu/sys/sysmacros.h /usr/include/alloca.h \
 /usr/include/x86_64-linux-gnu/bits/stdlib-float.h \
 /usr/include/c++/5/cstdio /usr/include/libio.h /usr/include/_G_config.h \
 /usr/include/x86_64-linux-gnu/bits/stdio_lim.h \
 /usr/include/x86_64-linux-gnu/bits/sys_errlist.h \
 /usr/include/c++/5/cerrno /usr/include/errno.h \
 /usr/include/x86_64-linux-gnu/bits/errno.h /usr/include/linux/errno.h \
 /usr/include/x86_64-linux-gnu/asm/errno.h \
 /usr/include/asm-generic/errno.h /usr/include/asm-generic/errno-base.h \
 /usr/include/c++/5/bits/functional_hash.h \
 /usr/include/c++/5/bits/hash_bytes.h \
 /usr/include/c++/5/bits/basic_string.tcc \
 /usr/include/c++/5/bits/locale_classes.tcc \
 /usr/include/c++/5/system_error \
 /usr/include/x86_64-linux-gnu/c++/5/bits/error_constants.h \
 /usr/include/c++/5/stdexcept /usr/include/c++/5/streambuf \
 /usr/include/c++/5/bits/streambuf.tcc \
 /usr/include/c++/5/bits/basic_ios.h \
 /usr/include/c++/5/bits/locale_facets.h /usr/include/c++/5/cwctype \
 /usr/include/wctype.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_base.h \
 /usr/include/c++/5/bits/streambuf_iterator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_inline.h \
 /usr/include/c++/5/bits/locale_facets.tcc \
 /usr/include/c++/5/bits/basic_ios.tcc \
 /usr/include/c++/5/bits/ostream.tcc /usr/include/c++/5/istream \
 /usr/include/c++/5/bits/istream.tcc /usr/include/c++/5/algorithm \
 /usr/include/c++/5/utility /usr/include/c++/5/bits/stl_relops.h \
 /usr/include/c++/5/bits/stl_algo.h \
 /usr/include/c++/5/bits/algorithmfwd.h \
 /usr/include/c++/5/bits/stl_heap.h /usr/include/c++/5/bits/stl_tempbuf.h \
 /usr/include/c++/5/bits/stl_construct.h /usr/include/c++/5/random \
 /usr/include/c++/5/cmath /usr/include/math.h \
 /usr/include/x86_64-linux-gnu/bits/math-vector.h \
 /usr/include/x86_64-linux-gnu/bits/libm-simd-decl-stubs.h \
 /usr/include/x86_64-linux-gnu/bits/huge_val.h \
 /usr/include/x86_64-linux-gnu/bits/huge_valf.h \
 /usr/include/x86_64-linux-gnu/bits/huge_vall.h \
 /usr/include/x86_64-linux-gnu/bits/inf.h \
 /usr/include/x86_64-linux-gnu/bits/nan.h \
 /usr/include/x86_64-linux-gnu/bits/mathdef.h \
 /usr/include/x86_64-linux-gnu/bits/mathcalls.h /usr/include/c++/5/limits \
 /usr/include/c++/5/bits/random.h /usr/include/c++/5/vector \
 /usr/include/c++/5/bits/stl_uninitialized.h \
 /usr/include/c++/5/bits/stl_vector.h \
 /usr/include/c++/5/bits/stl_bvector.h /usr/include/c++/5/bits/vector.tcc \
 /usr/include/c++/5/bits/uniform_int_dist.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/opt_random.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/x86intrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/ia32intrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/mmintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/xmmintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/mm_malloc.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/emmintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/pmmintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/tmmintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/ammintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/smmintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/popcntintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/wmmintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/immintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avxintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx2intrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512fintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512erintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512pfintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512cdintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512vlintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512bwintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512dqintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512vlbwintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512vldqintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512ifmaintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512ifmavlintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512vbmiintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512vbmivlintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/shaintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/lzcntintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/bmiintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/bmi2intrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/fmaintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/f16cintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/rtmintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/xtestintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/mm3dnow.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/prfchwintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/fma4intrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/xopintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/lwpintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/tbmintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/rdseedintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/fxsrintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/xsaveintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/xsaveoptintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/adxintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/clwbintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/clflushoptintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/xsavesintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/xsavecintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/mwaitxintrin.h \
 /usr/include/c++/5/bits/random.tcc /usr/include/c++/5/numeric \
 /usr/include/c++/5/bits/stl_numeric.h /usr/include/c++/5/sstream \
 /usr/include/c++/5/bits/sstream.tcc /usr/include/c++/5/iomanip \
 /usr/include/c++/5/locale /usr/include/c++/5/bits/locale_facets_nonio.h \
 /usr/include/c++/5/ctime \
 /usr/include/x86_64-linux-gnu/c++/5/bits/time_members.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/messages_members.h \
 /usr/include/libintl.h /usr/include/c++/5/bits/codecvt.h \
 /usr/include/c++/5/bits/locale_facets_nonio.tcc \
 /usr/include/c++/5/bits/locale_conv.h \
 /usr/include/c++/5/bits/stringfwd.h /usr/include/c++/5/bits/allocator.h \
 /usr/include/c++/5/bits/codecvt.h /usr/include/c++/5/bits/unique_ptr.h \
 /usr/include/c++/5/tuple /usr/include/c++/5/array \
 /usr/include/c++/5/bits/uses_allocator.h  engine/../data/gameData.h \
 /usr/include/c++/5/map /usr/include/c++/5/bits/stl_tree.h \
 /usr/include/c++/5/ext/aligned_buffer.h \
 /usr/include/c++/5/bits/stl_map.h /usr/include/c++/5/bits/stl_multimap.h \
 engine/../data/../math/vector2f.h engine/../data/parseXML.h \
 /usr/include/c++/5/fstream \
 /usr/include/x86_64-linux-gnu/c++/5/bits/basic_file.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++io.h \
 /usr/include/c++/5/bits/fstream.tcc /usr/include/c++/5/deque \
 /usr/include/c++/5/bits/stl_deque.h /usr/include/c++/5/bits/deque.tcc \
 /usr/include/expat.h /usr/include/expat_external.h \
 engine/../sprites/sprite.h engine/../sprites/../drawable.h \
 engine/../sprites/../math/vector2f.h \
 engine/../sprites/../graphics/components/image.h \
 engine/../sprites/../data/gameData.h \
 engine/../sprites/../data/gameData.h \
 engine/../sprites/../math/vector2f.h engine/../sprites/multisprite.h \
 engine/../sprites/sprite.h engine/../sprites/bidirectionalMultisprite.h \
 engine/../sprites/multisprite.h engine/engine.h \
 engine/../graphics/ioMod.h  engine/../graphics/renderContext.h \
 engine/../graphics/imageFactory.h engine/../graphics/components/image.h \
 engine/../graphics/../data/gameData.h \
 engine/../graphics/../sprites/spriteSheet.h   \
 engine/clock.h engine/../graphics/components/world.h \
 engine/../graphics/components/image.h \
 engine/../graphics/components/../viewport.h \
 engine/../graphics/components/../../drawable.h \
 engine/../graphics/components/../../data/gameData.h \
 engine/../graphics/components/../components/hud.h \
 engine/../graphics/components/../components/../ioMod.h \
 engine/../graphics/components/../components/../../engine/clock.h \
 engine/../graphics/components/../components/../../data/gameData.h \
 engine/../graphics/components/../components/menu.h \
 engine/../graphics/components/../../enums/state.h \
 engine/../graphics/components/../components/image.h \
 engine/../graphics/viewport.h engine/../sprites/enemy.h \
 /usr/include/c++/5/functional /usr/include/c++/5/typeinfo \
 engine/../sprites/bidirectionalMultisprite.h \
 engine/../sprites/../observable/observable.h \
 engine/../sprites/walkingSprite.h engine/../sprites/explodingSprite.h \
 /usr/include/c++/5/list /usr/include/c++/5/bits/stl_list.h \
 /usr/include/c++/5/bits/list.tcc engine/../sprites/chunk.h \
 engine/../sprites/../graphics/components/image.h \
 engine/../sprites/player.h \
 engine/../sprites/../managers/projectilePool.h \
 engine/../sprites/../managers/../sprites/projectileSprite.h \
 engine/../sprites/../managers/../sprites/sprite.h \
 engine/../sprites/../managers/../math/vector2f.h \
 engine/../sprites/../managers/trapPool.h \
 engine/../sprites/../managers/../sprites/multisprite.h \
 engine/../observable/observable.h engine/../math/vector2f.h \
 engine/perPixelCollisionStrategy.h engine/collisionStrategy.h \
 engine/../drawable.h engine/../enums/state.h
	$(CC) -c $< -o $@ $(CFLAGS)
$(OBJDIR)perPixelCollisionStrategy.o: engine/perPixelCollisionStrategy.cpp \
 /usr/include/stdc-predef.h /usr/include/c++/5/cmath \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/os_defines.h \
 /usr/include/features.h /usr/include/x86_64-linux-gnu/sys/cdefs.h \
 /usr/include/x86_64-linux-gnu/bits/wordsize.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs-64.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/cpu_defines.h \
 /usr/include/c++/5/bits/cpp_type_traits.h \
 /usr/include/c++/5/ext/type_traits.h /usr/include/math.h \
 /usr/include/x86_64-linux-gnu/bits/math-vector.h \
 /usr/include/x86_64-linux-gnu/bits/libm-simd-decl-stubs.h \
 /usr/include/x86_64-linux-gnu/bits/huge_val.h \
 /usr/include/x86_64-linux-gnu/bits/huge_valf.h \
 /usr/include/x86_64-linux-gnu/bits/huge_vall.h \
 /usr/include/x86_64-linux-gnu/bits/inf.h \
 /usr/include/x86_64-linux-gnu/bits/nan.h \
 /usr/include/x86_64-linux-gnu/bits/mathdef.h \
 /usr/include/x86_64-linux-gnu/bits/mathcalls.h  \
 /usr/include/c++/5/vector /usr/include/c++/5/bits/stl_algobase.h \
 /usr/include/c++/5/bits/functexcept.h \
 /usr/include/c++/5/bits/exception_defines.h \
 /usr/include/c++/5/ext/numeric_traits.h \
 /usr/include/c++/5/bits/stl_pair.h /usr/include/c++/5/bits/move.h \
 /usr/include/c++/5/bits/concept_check.h /usr/include/c++/5/type_traits \
 /usr/include/c++/5/bits/stl_iterator_base_types.h \
 /usr/include/c++/5/bits/stl_iterator_base_funcs.h \
 /usr/include/c++/5/debug/debug.h /usr/include/c++/5/bits/stl_iterator.h \
 /usr/include/c++/5/bits/ptr_traits.h \
 /usr/include/c++/5/bits/predefined_ops.h \
 /usr/include/c++/5/bits/allocator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++allocator.h \
 /usr/include/c++/5/ext/new_allocator.h /usr/include/c++/5/new \
 /usr/include/c++/5/exception \
 /usr/include/c++/5/bits/atomic_lockfree_defines.h \
 /usr/include/c++/5/bits/exception_ptr.h \
 /usr/include/c++/5/bits/nested_exception.h \
 /usr/include/c++/5/bits/memoryfwd.h \
 /usr/include/c++/5/bits/stl_construct.h \
 /usr/include/c++/5/ext/alloc_traits.h \
 /usr/include/c++/5/bits/alloc_traits.h \
 /usr/include/c++/5/bits/stl_uninitialized.h \
 /usr/include/c++/5/bits/stl_vector.h /usr/include/c++/5/initializer_list \
 /usr/include/c++/5/bits/stl_bvector.h \
 /usr/include/c++/5/bits/functional_hash.h \
 /usr/include/c++/5/bits/hash_bytes.h \
 /usr/include/c++/5/bits/range_access.h \
 /usr/include/c++/5/bits/vector.tcc /usr/include/c++/5/algorithm \
 /usr/include/c++/5/utility /usr/include/c++/5/bits/stl_relops.h \
 /usr/include/c++/5/bits/stl_algo.h /usr/include/c++/5/cstdlib \
 /usr/include/stdlib.h /usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h \
 /usr/include/x86_64-linux-gnu/bits/waitflags.h \
 /usr/include/x86_64-linux-gnu/bits/waitstatus.h /usr/include/endian.h \
 /usr/include/x86_64-linux-gnu/bits/endian.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap.h \
 /usr/include/x86_64-linux-gnu/bits/types.h \
 /usr/include/x86_64-linux-gnu/bits/typesizes.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap-16.h /usr/include/xlocale.h \
 /usr/include/x86_64-linux-gnu/sys/types.h /usr/include/time.h \
 /usr/include/x86_64-linux-gnu/sys/select.h \
 /usr/include/x86_64-linux-gnu/bits/select.h \
 /usr/include/x86_64-linux-gnu/bits/sigset.h \
 /usr/include/x86_64-linux-gnu/bits/time.h \
 /usr/include/x86_64-linux-gnu/sys/sysmacros.h \
 /usr/include/x86_64-linux-gnu/bits/pthreadtypes.h /usr/include/alloca.h \
 /usr/include/x86_64-linux-gnu/bits/stdlib-float.h \
 /usr/include/c++/5/bits/algorithmfwd.h \
 /usr/include/c++/5/bits/stl_heap.h /usr/include/c++/5/bits/stl_tempbuf.h \
 /usr/include/c++/5/random /usr/include/c++/5/string \
 /usr/include/c++/5/bits/stringfwd.h \
 /usr/include/c++/5/bits/char_traits.h /usr/include/c++/5/bits/postypes.h \
 /usr/include/c++/5/cwchar /usr/include/wchar.h /usr/include/stdio.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdarg.h \
 /usr/include/x86_64-linux-gnu/bits/wchar.h /usr/include/c++/5/cstdint \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdint.h /usr/include/stdint.h \
 /usr/include/c++/5/bits/localefwd.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++locale.h \
 /usr/include/c++/5/clocale /usr/include/locale.h \
 /usr/include/x86_64-linux-gnu/bits/locale.h /usr/include/c++/5/iosfwd \
 /usr/include/c++/5/cctype /usr/include/ctype.h \
 /usr/include/c++/5/bits/ostream_insert.h \
 /usr/include/c++/5/bits/cxxabi_forced.h \
 /usr/include/c++/5/bits/stl_function.h \
 /usr/include/c++/5/backward/binders.h \
 /usr/include/c++/5/bits/basic_string.h \
 /usr/include/c++/5/ext/atomicity.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr-default.h \
 /usr/include/pthread.h /usr/include/sched.h \
 /usr/include/x86_64-linux-gnu/bits/sched.h \
 /usr/include/x86_64-linux-gnu/bits/timex.h \
 /usr/include/x86_64-linux-gnu/bits/setjmp.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h \
 /usr/include/c++/5/ext/string_conversions.h /usr/include/c++/5/cstdio \
 /usr/include/libio.h /usr/include/_G_config.h \
 /usr/include/x86_64-linux-gnu/bits/stdio_lim.h \
 /usr/include/x86_64-linux-gnu/bits/sys_errlist.h \
 /usr/include/c++/5/cerrno /usr/include/errno.h \
 /usr/include/x86_64-linux-gnu/bits/errno.h /usr/include/linux/errno.h \
 /usr/include/x86_64-linux-gnu/asm/errno.h \
 /usr/include/asm-generic/errno.h /usr/include/asm-generic/errno-base.h \
 /usr/include/c++/5/bits/basic_string.tcc /usr/include/c++/5/limits \
 /usr/include/c++/5/bits/random.h \
 /usr/include/c++/5/bits/uniform_int_dist.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/opt_random.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/x86intrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/ia32intrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/mmintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/xmmintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/mm_malloc.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/emmintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/pmmintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/tmmintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/ammintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/smmintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/popcntintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/wmmintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/immintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avxintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx2intrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512fintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512erintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512pfintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512cdintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512vlintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512bwintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512dqintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512vlbwintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512vldqintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512ifmaintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512ifmavlintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512vbmiintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512vbmivlintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/shaintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/lzcntintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/bmiintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/bmi2intrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/fmaintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/f16cintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/rtmintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/xtestintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/mm3dnow.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/prfchwintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/fma4intrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/xopintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/lwpintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/tbmintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/rdseedintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/fxsrintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/xsaveintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/xsaveoptintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/adxintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/clwbintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/clflushoptintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/xsavesintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/xsavecintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/mwaitxintrin.h \
 /usr/include/c++/5/bits/random.tcc /usr/include/c++/5/numeric \
 /usr/include/c++/5/bits/stl_numeric.h engine/perPixelCollisionStrategy.h \
 engine/collisionStrategy.h engine/../drawable.h \
 /usr/include/c++/5/iostream /usr/include/c++/5/ostream \
 /usr/include/c++/5/ios /usr/include/c++/5/bits/ios_base.h \
 /usr/include/c++/5/bits/locale_classes.h \
 /usr/include/c++/5/bits/locale_classes.tcc \
 /usr/include/c++/5/system_error \
 /usr/include/x86_64-linux-gnu/c++/5/bits/error_constants.h \
 /usr/include/c++/5/stdexcept /usr/include/c++/5/streambuf \
 /usr/include/c++/5/bits/streambuf.tcc \
 /usr/include/c++/5/bits/basic_ios.h \
 /usr/include/c++/5/bits/locale_facets.h /usr/include/c++/5/cwctype \
 /usr/include/wctype.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_base.h \
 /usr/include/c++/5/bits/streambuf_iterator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_inline.h \
 /usr/include/c++/5/bits/locale_facets.tcc \
 /usr/include/c++/5/bits/basic_ios.tcc \
 /usr/include/c++/5/bits/ostream.tcc /usr/include/c++/5/istream \
 /usr/include/c++/5/bits/istream.tcc engine/../math/vector2f.h \
 engine/../graphics/components/image.h engine/../data/gameData.h \
 /usr/include/c++/5/map /usr/include/c++/5/bits/stl_tree.h \
 /usr/include/c++/5/ext/aligned_buffer.h \
 /usr/include/c++/5/bits/stl_map.h /usr/include/c++/5/tuple \
 /usr/include/c++/5/array /usr/include/c++/5/bits/uses_allocator.h \
 /usr/include/c++/5/bits/stl_multimap.h engine/../data/../math/vector2f.h \
 engine/../data/parseXML.h /usr/include/c++/5/fstream \
 /usr/include/c++/5/bits/codecvt.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/basic_file.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++io.h \
 /usr/include/c++/5/bits/fstream.tcc /usr/include/c++/5/deque \
 /usr/include/c++/5/bits/stl_deque.h /usr/include/c++/5/bits/deque.tcc \
 /usr/include/expat.h /usr/include/expat_external.h \
 engine/../graphics/ioMod.h  \
 engine/rectangularCollisionStrategy.h engine/../graphics/viewport.h \
 engine/../graphics/../drawable.h engine/../graphics/../data/gameData.h \
 engine/../graphics/components/hud.h /usr/include/c++/5/sstream \
 /usr/include/c++/5/bits/sstream.tcc \
 engine/../graphics/components/../ioMod.h \
 engine/../graphics/components/../../engine/clock.h \
 engine/../graphics/components/../../data/gameData.h \
 engine/../graphics/components/menu.h engine/../graphics/../enums/state.h \
 engine/../graphics/components/image.h
	$(CC) -c $< -o $@ $(CFLAGS)
$(OBJDIR)rectangularCollisionStrategy.o: engine/rectangularCollisionStrategy.cpp \
 /usr/include/stdc-predef.h engine/rectangularCollisionStrategy.h \
 engine/collisionStrategy.h engine/../drawable.h  \
 /usr/include/c++/5/iostream \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/os_defines.h \
 /usr/include/features.h /usr/include/x86_64-linux-gnu/sys/cdefs.h \
 /usr/include/x86_64-linux-gnu/bits/wordsize.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs-64.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/cpu_defines.h \
 /usr/include/c++/5/ostream /usr/include/c++/5/ios \
 /usr/include/c++/5/iosfwd /usr/include/c++/5/bits/stringfwd.h \
 /usr/include/c++/5/bits/memoryfwd.h /usr/include/c++/5/bits/postypes.h \
 /usr/include/c++/5/cwchar /usr/include/wchar.h /usr/include/stdio.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdarg.h \
 /usr/include/x86_64-linux-gnu/bits/wchar.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h /usr/include/xlocale.h \
 /usr/include/c++/5/exception \
 /usr/include/c++/5/bits/atomic_lockfree_defines.h \
 /usr/include/c++/5/bits/exception_ptr.h \
 /usr/include/c++/5/bits/exception_defines.h \
 /usr/include/c++/5/bits/nested_exception.h \
 /usr/include/c++/5/bits/char_traits.h \
 /usr/include/c++/5/bits/stl_algobase.h \
 /usr/include/c++/5/bits/functexcept.h \
 /usr/include/c++/5/bits/cpp_type_traits.h \
 /usr/include/c++/5/ext/type_traits.h \
 /usr/include/c++/5/ext/numeric_traits.h \
 /usr/include/c++/5/bits/stl_pair.h /usr/include/c++/5/bits/move.h \
 /usr/include/c++/5/bits/concept_check.h /usr/include/c++/5/type_traits \
 /usr/include/c++/5/bits/stl_iterator_base_types.h \
 /usr/include/c++/5/bits/stl_iterator_base_funcs.h \
 /usr/include/c++/5/debug/debug.h /usr/include/c++/5/bits/stl_iterator.h \
 /usr/include/c++/5/bits/ptr_traits.h \
 /usr/include/c++/5/bits/predefined_ops.h /usr/include/c++/5/cstdint \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdint.h /usr/include/stdint.h \
 /usr/include/c++/5/bits/localefwd.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++locale.h \
 /usr/include/c++/5/clocale /usr/include/locale.h \
 /usr/include/x86_64-linux-gnu/bits/locale.h /usr/include/c++/5/cctype \
 /usr/include/ctype.h /usr/include/x86_64-linux-gnu/bits/types.h \
 /usr/include/x86_64-linux-gnu/bits/typesizes.h /usr/include/endian.h \
 /usr/include/x86_64-linux-gnu/bits/endian.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap-16.h \
 /usr/include/c++/5/bits/ios_base.h /usr/include/c++/5/ext/atomicity.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr-default.h \
 /usr/include/pthread.h /usr/include/sched.h /usr/include/time.h \
 /usr/include/x86_64-linux-gnu/bits/sched.h \
 /usr/include/x86_64-linux-gnu/bits/time.h \
 /usr/include/x86_64-linux-gnu/bits/timex.h \
 /usr/include/x86_64-linux-gnu/bits/pthreadtypes.h \
 /usr/include/x86_64-linux-gnu/bits/setjmp.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h \
 /usr/include/c++/5/bits/locale_classes.h /usr/include/c++/5/string \
 /usr/include/c++/5/bits/allocator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++allocator.h \
 /usr/include/c++/5/ext/new_allocator.h /usr/include/c++/5/new \
 /usr/include/c++/5/bits/ostream_insert.h \
 /usr/include/c++/5/bits/cxxabi_forced.h \
 /usr/include/c++/5/bits/stl_function.h \
 /usr/include/c++/5/backward/binders.h \
 /usr/include/c++/5/bits/range_access.h \
 /usr/include/c++/5/initializer_list \
 /usr/include/c++/5/bits/basic_string.h \
 /usr/include/c++/5/ext/alloc_traits.h \
 /usr/include/c++/5/bits/alloc_traits.h \
 /usr/include/c++/5/ext/string_conversions.h /usr/include/c++/5/cstdlib \
 /usr/include/stdlib.h /usr/include/x86_64-linux-gnu/bits/waitflags.h \
 /usr/include/x86_64-linux-gnu/bits/waitstatus.h \
 /usr/include/x86_64-linux-gnu/sys/types.h \
 /usr/include/x86_64-linux-gnu/sys/select.h \
 /usr/include/x86_64-linux-gnu/bits/select.h \
 /usr/include/x86_64-linux-gnu/bits/sigset.h \
 /usr/include/x86_64-linux-gnu/sys/sysmacros.h /usr/include/alloca.h \
 /usr/include/x86_64-linux-gnu/bits/stdlib-float.h \
 /usr/include/c++/5/cstdio /usr/include/libio.h /usr/include/_G_config.h \
 /usr/include/x86_64-linux-gnu/bits/stdio_lim.h \
 /usr/include/x86_64-linux-gnu/bits/sys_errlist.h \
 /usr/include/c++/5/cerrno /usr/include/errno.h \
 /usr/include/x86_64-linux-gnu/bits/errno.h /usr/include/linux/errno.h \
 /usr/include/x86_64-linux-gnu/asm/errno.h \
 /usr/include/asm-generic/errno.h /usr/include/asm-generic/errno-base.h \
 /usr/include/c++/5/bits/functional_hash.h \
 /usr/include/c++/5/bits/hash_bytes.h \
 /usr/include/c++/5/bits/basic_string.tcc \
 /usr/include/c++/5/bits/locale_classes.tcc \
 /usr/include/c++/5/system_error \
 /usr/include/x86_64-linux-gnu/c++/5/bits/error_constants.h \
 /usr/include/c++/5/stdexcept /usr/include/c++/5/streambuf \
 /usr/include/c++/5/bits/streambuf.tcc \
 /usr/include/c++/5/bits/basic_ios.h \
 /usr/include/c++/5/bits/locale_facets.h /usr/include/c++/5/cwctype \
 /usr/include/wctype.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_base.h \
 /usr/include/c++/5/bits/streambuf_iterator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_inline.h \
 /usr/include/c++/5/bits/locale_facets.tcc \
 /usr/include/c++/5/bits/basic_ios.tcc \
 /usr/include/c++/5/bits/ostream.tcc /usr/include/c++/5/istream \
 /usr/include/c++/5/bits/istream.tcc engine/../math/vector2f.h \
 engine/../graphics/components/image.h engine/../data/gameData.h \
 /usr/include/c++/5/map /usr/include/c++/5/bits/stl_tree.h \
 /usr/include/c++/5/ext/aligned_buffer.h \
 /usr/include/c++/5/bits/stl_map.h /usr/include/c++/5/tuple \
 /usr/include/c++/5/utility /usr/include/c++/5/bits/stl_relops.h \
 /usr/include/c++/5/array /usr/include/c++/5/bits/uses_allocator.h \
 /usr/include/c++/5/bits/stl_multimap.h engine/../data/../math/vector2f.h \
 engine/../data/parseXML.h /usr/include/c++/5/fstream \
 /usr/include/c++/5/bits/codecvt.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/basic_file.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++io.h \
 /usr/include/c++/5/bits/fstream.tcc /usr/include/c++/5/deque \
 /usr/include/c++/5/bits/stl_construct.h \
 /usr/include/c++/5/bits/stl_uninitialized.h \
 /usr/include/c++/5/bits/stl_deque.h /usr/include/c++/5/bits/deque.tcc \
 /usr/include/expat.h /usr/include/expat_external.h \
 engine/../graphics/ioMod.h 
	$(CC) -c $< -o $@ $(CFLAGS)
$(OBJDIR)state.o: enums/state.cpp /usr/include/stdc-predef.h \
 /usr/include/c++/5/iostream \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/os_defines.h \
 /usr/include/features.h /usr/include/x86_64-linux-gnu/sys/cdefs.h \
 /usr/include/x86_64-linux-gnu/bits/wordsize.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs-64.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/cpu_defines.h \
 /usr/include/c++/5/ostream /usr/include/c++/5/ios \
 /usr/include/c++/5/iosfwd /usr/include/c++/5/bits/stringfwd.h \
 /usr/include/c++/5/bits/memoryfwd.h /usr/include/c++/5/bits/postypes.h \
 /usr/include/c++/5/cwchar /usr/include/wchar.h /usr/include/stdio.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdarg.h \
 /usr/include/x86_64-linux-gnu/bits/wchar.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h /usr/include/xlocale.h \
 /usr/include/c++/5/exception \
 /usr/include/c++/5/bits/atomic_lockfree_defines.h \
 /usr/include/c++/5/bits/exception_ptr.h \
 /usr/include/c++/5/bits/exception_defines.h \
 /usr/include/c++/5/bits/nested_exception.h \
 /usr/include/c++/5/bits/char_traits.h \
 /usr/include/c++/5/bits/stl_algobase.h \
 /usr/include/c++/5/bits/functexcept.h \
 /usr/include/c++/5/bits/cpp_type_traits.h \
 /usr/include/c++/5/ext/type_traits.h \
 /usr/include/c++/5/ext/numeric_traits.h \
 /usr/include/c++/5/bits/stl_pair.h /usr/include/c++/5/bits/move.h \
 /usr/include/c++/5/bits/concept_check.h /usr/include/c++/5/type_traits \
 /usr/include/c++/5/bits/stl_iterator_base_types.h \
 /usr/include/c++/5/bits/stl_iterator_base_funcs.h \
 /usr/include/c++/5/debug/debug.h /usr/include/c++/5/bits/stl_iterator.h \
 /usr/include/c++/5/bits/ptr_traits.h \
 /usr/include/c++/5/bits/predefined_ops.h /usr/include/c++/5/cstdint \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdint.h /usr/include/stdint.h \
 /usr/include/c++/5/bits/localefwd.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++locale.h \
 /usr/include/c++/5/clocale /usr/include/locale.h \
 /usr/include/x86_64-linux-gnu/bits/locale.h /usr/include/c++/5/cctype \
 /usr/include/ctype.h /usr/include/x86_64-linux-gnu/bits/types.h \
 /usr/include/x86_64-linux-gnu/bits/typesizes.h /usr/include/endian.h \
 /usr/include/x86_64-linux-gnu/bits/endian.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap-16.h \
 /usr/include/c++/5/bits/ios_base.h /usr/include/c++/5/ext/atomicity.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr-default.h \
 /usr/include/pthread.h /usr/include/sched.h /usr/include/time.h \
 /usr/include/x86_64-linux-gnu/bits/sched.h \
 /usr/include/x86_64-linux-gnu/bits/time.h \
 /usr/include/x86_64-linux-gnu/bits/timex.h \
 /usr/include/x86_64-linux-gnu/bits/pthreadtypes.h \
 /usr/include/x86_64-linux-gnu/bits/setjmp.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h \
 /usr/include/c++/5/bits/locale_classes.h /usr/include/c++/5/string \
 /usr/include/c++/5/bits/allocator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++allocator.h \
 /usr/include/c++/5/ext/new_allocator.h /usr/include/c++/5/new \
 /usr/include/c++/5/bits/ostream_insert.h \
 /usr/include/c++/5/bits/cxxabi_forced.h \
 /usr/include/c++/5/bits/stl_function.h \
 /usr/include/c++/5/backward/binders.h \
 /usr/include/c++/5/bits/range_access.h \
 /usr/include/c++/5/initializer_list \
 /usr/include/c++/5/bits/basic_string.h \
 /usr/include/c++/5/ext/alloc_traits.h \
 /usr/include/c++/5/bits/alloc_traits.h \
 /usr/include/c++/5/ext/string_conversions.h /usr/include/c++/5/cstdlib \
 /usr/include/stdlib.h /usr/include/x86_64-linux-gnu/bits/waitflags.h \
 /usr/include/x86_64-linux-gnu/bits/waitstatus.h \
 /usr/include/x86_64-linux-gnu/sys/types.h \
 /usr/include/x86_64-linux-gnu/sys/select.h \
 /usr/include/x86_64-linux-gnu/bits/select.h \
 /usr/include/x86_64-linux-gnu/bits/sigset.h \
 /usr/include/x86_64-linux-gnu/sys/sysmacros.h /usr/include/alloca.h \
 /usr/include/x86_64-linux-gnu/bits/stdlib-float.h \
 /usr/include/c++/5/cstdio /usr/include/libio.h /usr/include/_G_config.h \
 /usr/include/x86_64-linux-gnu/bits/stdio_lim.h \
 /usr/include/x86_64-linux-gnu/bits/sys_errlist.h \
 /usr/include/c++/5/cerrno /usr/include/errno.h \
 /usr/include/x86_64-linux-gnu/bits/errno.h /usr/include/linux/errno.h \
 /usr/include/x86_64-linux-gnu/asm/errno.h \
 /usr/include/asm-generic/errno.h /usr/include/asm-generic/errno-base.h \
 /usr/include/c++/5/bits/functional_hash.h \
 /usr/include/c++/5/bits/hash_bytes.h \
 /usr/include/c++/5/bits/basic_string.tcc \
 /usr/include/c++/5/bits/locale_classes.tcc \
 /usr/include/c++/5/system_error \
 /usr/include/x86_64-linux-gnu/c++/5/bits/error_constants.h \
 /usr/include/c++/5/stdexcept /usr/include/c++/5/streambuf \
 /usr/include/c++/5/bits/streambuf.tcc \
 /usr/include/c++/5/bits/basic_ios.h \
 /usr/include/c++/5/bits/locale_facets.h /usr/include/c++/5/cwctype \
 /usr/include/wctype.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_base.h \
 /usr/include/c++/5/bits/streambuf_iterator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_inline.h \
 /usr/include/c++/5/bits/locale_facets.tcc \
 /usr/include/c++/5/bits/basic_ios.tcc \
 /usr/include/c++/5/bits/ostream.tcc /usr/include/c++/5/istream \
 /usr/include/c++/5/bits/istream.tcc enums/state.h
	$(CC) -c $< -o $@ $(CFLAGS)
$(OBJDIR)hud.o: graphics/components/hud.cpp /usr/include/stdc-predef.h  \
 /usr/include/c++/5/sstream /usr/include/c++/5/istream \
 /usr/include/c++/5/ios /usr/include/c++/5/iosfwd \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/os_defines.h \
 /usr/include/features.h /usr/include/x86_64-linux-gnu/sys/cdefs.h \
 /usr/include/x86_64-linux-gnu/bits/wordsize.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs-64.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/cpu_defines.h \
 /usr/include/c++/5/bits/stringfwd.h /usr/include/c++/5/bits/memoryfwd.h \
 /usr/include/c++/5/bits/postypes.h /usr/include/c++/5/cwchar \
 /usr/include/wchar.h /usr/include/stdio.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdarg.h \
 /usr/include/x86_64-linux-gnu/bits/wchar.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h /usr/include/xlocale.h \
 /usr/include/c++/5/exception \
 /usr/include/c++/5/bits/atomic_lockfree_defines.h \
 /usr/include/c++/5/bits/exception_ptr.h \
 /usr/include/c++/5/bits/exception_defines.h \
 /usr/include/c++/5/bits/nested_exception.h \
 /usr/include/c++/5/bits/char_traits.h \
 /usr/include/c++/5/bits/stl_algobase.h \
 /usr/include/c++/5/bits/functexcept.h \
 /usr/include/c++/5/bits/cpp_type_traits.h \
 /usr/include/c++/5/ext/type_traits.h \
 /usr/include/c++/5/ext/numeric_traits.h \
 /usr/include/c++/5/bits/stl_pair.h /usr/include/c++/5/bits/move.h \
 /usr/include/c++/5/bits/concept_check.h /usr/include/c++/5/type_traits \
 /usr/include/c++/5/bits/stl_iterator_base_types.h \
 /usr/include/c++/5/bits/stl_iterator_base_funcs.h \
 /usr/include/c++/5/debug/debug.h /usr/include/c++/5/bits/stl_iterator.h \
 /usr/include/c++/5/bits/ptr_traits.h \
 /usr/include/c++/5/bits/predefined_ops.h /usr/include/c++/5/cstdint \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdint.h /usr/include/stdint.h \
 /usr/include/c++/5/bits/localefwd.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++locale.h \
 /usr/include/c++/5/clocale /usr/include/locale.h \
 /usr/include/x86_64-linux-gnu/bits/locale.h /usr/include/c++/5/cctype \
 /usr/include/ctype.h /usr/include/x86_64-linux-gnu/bits/types.h \
 /usr/include/x86_64-linux-gnu/bits/typesizes.h /usr/include/endian.h \
 /usr/include/x86_64-linux-gnu/bits/endian.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap-16.h \
 /usr/include/c++/5/bits/ios_base.h /usr/include/c++/5/ext/atomicity.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr-default.h \
 /usr/include/pthread.h /usr/include/sched.h /usr/include/time.h \
 /usr/include/x86_64-linux-gnu/bits/sched.h \
 /usr/include/x86_64-linux-gnu/bits/time.h \
 /usr/include/x86_64-linux-gnu/bits/timex.h \
 /usr/include/x86_64-linux-gnu/bits/pthreadtypes.h \
 /usr/include/x86_64-linux-gnu/bits/setjmp.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h \
 /usr/include/c++/5/bits/locale_classes.h /usr/include/c++/5/string \
 /usr/include/c++/5/bits/allocator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++allocator.h \
 /usr/include/c++/5/ext/new_allocator.h /usr/include/c++/5/new \
 /usr/include/c++/5/bits/ostream_insert.h \
 /usr/include/c++/5/bits/cxxabi_forced.h \
 /usr/include/c++/5/bits/stl_function.h \
 /usr/include/c++/5/backward/binders.h \
 /usr/include/c++/5/bits/range_access.h \
 /usr/include/c++/5/initializer_list \
 /usr/include/c++/5/bits/basic_string.h \
 /usr/include/c++/5/ext/alloc_traits.h \
 /usr/include/c++/5/bits/alloc_traits.h \
 /usr/include/c++/5/ext/string_conversions.h /usr/include/c++/5/cstdlib \
 /usr/include/stdlib.h /usr/include/x86_64-linux-gnu/bits/waitflags.h \
 /usr/include/x86_64-linux-gnu/bits/waitstatus.h \
 /usr/include/x86_64-linux-gnu/sys/types.h \
 /usr/include/x86_64-linux-gnu/sys/select.h \
 /usr/include/x86_64-linux-gnu/bits/select.h \
 /usr/include/x86_64-linux-gnu/bits/sigset.h \
 /usr/include/x86_64-linux-gnu/sys/sysmacros.h /usr/include/alloca.h \
 /usr/include/x86_64-linux-gnu/bits/stdlib-float.h \
 /usr/include/c++/5/cstdio /usr/include/libio.h /usr/include/_G_config.h \
 /usr/include/x86_64-linux-gnu/bits/stdio_lim.h \
 /usr/include/x86_64-linux-gnu/bits/sys_errlist.h \
 /usr/include/c++/5/cerrno /usr/include/errno.h \
 /usr/include/x86_64-linux-gnu/bits/errno.h /usr/include/linux/errno.h \
 /usr/include/x86_64-linux-gnu/asm/errno.h \
 /usr/include/asm-generic/errno.h /usr/include/asm-generic/errno-base.h \
 /usr/include/c++/5/bits/functional_hash.h \
 /usr/include/c++/5/bits/hash_bytes.h \
 /usr/include/c++/5/bits/basic_string.tcc \
 /usr/include/c++/5/bits/locale_classes.tcc \
 /usr/include/c++/5/system_error \
 /usr/include/x86_64-linux-gnu/c++/5/bits/error_constants.h \
 /usr/include/c++/5/stdexcept /usr/include/c++/5/streambuf \
 /usr/include/c++/5/bits/streambuf.tcc \
 /usr/include/c++/5/bits/basic_ios.h \
 /usr/include/c++/5/bits/locale_facets.h /usr/include/c++/5/cwctype \
 /usr/include/wctype.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_base.h \
 /usr/include/c++/5/bits/streambuf_iterator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_inline.h \
 /usr/include/c++/5/bits/locale_facets.tcc \
 /usr/include/c++/5/bits/basic_ios.tcc /usr/include/c++/5/ostream \
 /usr/include/c++/5/bits/ostream.tcc /usr/include/c++/5/bits/istream.tcc \
 /usr/include/c++/5/bits/sstream.tcc graphics/components/hud.h \
 graphics/components/../ioMod.h /usr/include/c++/5/iostream  \
 graphics/components/../../engine/clock.h \
 graphics/components/../../data/gameData.h /usr/include/c++/5/map \
 /usr/include/c++/5/bits/stl_tree.h \
 /usr/include/c++/5/ext/aligned_buffer.h \
 /usr/include/c++/5/bits/stl_map.h /usr/include/c++/5/tuple \
 /usr/include/c++/5/utility /usr/include/c++/5/bits/stl_relops.h \
 /usr/include/c++/5/array /usr/include/c++/5/bits/uses_allocator.h \
 /usr/include/c++/5/bits/stl_multimap.h \
 graphics/components/../../data/../math/vector2f.h \
 graphics/components/../../data/parseXML.h /usr/include/c++/5/fstream \
 /usr/include/c++/5/bits/codecvt.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/basic_file.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++io.h \
 /usr/include/c++/5/bits/fstream.tcc /usr/include/c++/5/deque \
 /usr/include/c++/5/bits/stl_construct.h \
 /usr/include/c++/5/bits/stl_uninitialized.h \
 /usr/include/c++/5/bits/stl_deque.h /usr/include/c++/5/bits/deque.tcc \
 /usr/include/expat.h /usr/include/expat_external.h
	$(CC) -c $< -o $@ $(CFLAGS)
$(OBJDIR)image.o: graphics/components/image.cpp /usr/include/stdc-predef.h \
 graphics/components/../../drawable.h  /usr/include/c++/5/iostream \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/os_defines.h \
 /usr/include/features.h /usr/include/x86_64-linux-gnu/sys/cdefs.h \
 /usr/include/x86_64-linux-gnu/bits/wordsize.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs-64.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/cpu_defines.h \
 /usr/include/c++/5/ostream /usr/include/c++/5/ios \
 /usr/include/c++/5/iosfwd /usr/include/c++/5/bits/stringfwd.h \
 /usr/include/c++/5/bits/memoryfwd.h /usr/include/c++/5/bits/postypes.h \
 /usr/include/c++/5/cwchar /usr/include/wchar.h /usr/include/stdio.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdarg.h \
 /usr/include/x86_64-linux-gnu/bits/wchar.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h /usr/include/xlocale.h \
 /usr/include/c++/5/exception \
 /usr/include/c++/5/bits/atomic_lockfree_defines.h \
 /usr/include/c++/5/bits/exception_ptr.h \
 /usr/include/c++/5/bits/exception_defines.h \
 /usr/include/c++/5/bits/nested_exception.h \
 /usr/include/c++/5/bits/char_traits.h \
 /usr/include/c++/5/bits/stl_algobase.h \
 /usr/include/c++/5/bits/functexcept.h \
 /usr/include/c++/5/bits/cpp_type_traits.h \
 /usr/include/c++/5/ext/type_traits.h \
 /usr/include/c++/5/ext/numeric_traits.h \
 /usr/include/c++/5/bits/stl_pair.h /usr/include/c++/5/bits/move.h \
 /usr/include/c++/5/bits/concept_check.h /usr/include/c++/5/type_traits \
 /usr/include/c++/5/bits/stl_iterator_base_types.h \
 /usr/include/c++/5/bits/stl_iterator_base_funcs.h \
 /usr/include/c++/5/debug/debug.h /usr/include/c++/5/bits/stl_iterator.h \
 /usr/include/c++/5/bits/ptr_traits.h \
 /usr/include/c++/5/bits/predefined_ops.h /usr/include/c++/5/cstdint \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdint.h /usr/include/stdint.h \
 /usr/include/c++/5/bits/localefwd.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++locale.h \
 /usr/include/c++/5/clocale /usr/include/locale.h \
 /usr/include/x86_64-linux-gnu/bits/locale.h /usr/include/c++/5/cctype \
 /usr/include/ctype.h /usr/include/x86_64-linux-gnu/bits/types.h \
 /usr/include/x86_64-linux-gnu/bits/typesizes.h /usr/include/endian.h \
 /usr/include/x86_64-linux-gnu/bits/endian.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap-16.h \
 /usr/include/c++/5/bits/ios_base.h /usr/include/c++/5/ext/atomicity.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr-default.h \
 /usr/include/pthread.h /usr/include/sched.h /usr/include/time.h \
 /usr/include/x86_64-linux-gnu/bits/sched.h \
 /usr/include/x86_64-linux-gnu/bits/time.h \
 /usr/include/x86_64-linux-gnu/bits/timex.h \
 /usr/include/x86_64-linux-gnu/bits/pthreadtypes.h \
 /usr/include/x86_64-linux-gnu/bits/setjmp.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h \
 /usr/include/c++/5/bits/locale_classes.h /usr/include/c++/5/string \
 /usr/include/c++/5/bits/allocator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++allocator.h \
 /usr/include/c++/5/ext/new_allocator.h /usr/include/c++/5/new \
 /usr/include/c++/5/bits/ostream_insert.h \
 /usr/include/c++/5/bits/cxxabi_forced.h \
 /usr/include/c++/5/bits/stl_function.h \
 /usr/include/c++/5/backward/binders.h \
 /usr/include/c++/5/bits/range_access.h \
 /usr/include/c++/5/initializer_list \
 /usr/include/c++/5/bits/basic_string.h \
 /usr/include/c++/5/ext/alloc_traits.h \
 /usr/include/c++/5/bits/alloc_traits.h \
 /usr/include/c++/5/ext/string_conversions.h /usr/include/c++/5/cstdlib \
 /usr/include/stdlib.h /usr/include/x86_64-linux-gnu/bits/waitflags.h \
 /usr/include/x86_64-linux-gnu/bits/waitstatus.h \
 /usr/include/x86_64-linux-gnu/sys/types.h \
 /usr/include/x86_64-linux-gnu/sys/select.h \
 /usr/include/x86_64-linux-gnu/bits/select.h \
 /usr/include/x86_64-linux-gnu/bits/sigset.h \
 /usr/include/x86_64-linux-gnu/sys/sysmacros.h /usr/include/alloca.h \
 /usr/include/x86_64-linux-gnu/bits/stdlib-float.h \
 /usr/include/c++/5/cstdio /usr/include/libio.h /usr/include/_G_config.h \
 /usr/include/x86_64-linux-gnu/bits/stdio_lim.h \
 /usr/include/x86_64-linux-gnu/bits/sys_errlist.h \
 /usr/include/c++/5/cerrno /usr/include/errno.h \
 /usr/include/x86_64-linux-gnu/bits/errno.h /usr/include/linux/errno.h \
 /usr/include/x86_64-linux-gnu/asm/errno.h \
 /usr/include/asm-generic/errno.h /usr/include/asm-generic/errno-base.h \
 /usr/include/c++/5/bits/functional_hash.h \
 /usr/include/c++/5/bits/hash_bytes.h \
 /usr/include/c++/5/bits/basic_string.tcc \
 /usr/include/c++/5/bits/locale_classes.tcc \
 /usr/include/c++/5/system_error \
 /usr/include/x86_64-linux-gnu/c++/5/bits/error_constants.h \
 /usr/include/c++/5/stdexcept /usr/include/c++/5/streambuf \
 /usr/include/c++/5/bits/streambuf.tcc \
 /usr/include/c++/5/bits/basic_ios.h \
 /usr/include/c++/5/bits/locale_facets.h /usr/include/c++/5/cwctype \
 /usr/include/wctype.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_base.h \
 /usr/include/c++/5/bits/streambuf_iterator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_inline.h \
 /usr/include/c++/5/bits/locale_facets.tcc \
 /usr/include/c++/5/bits/basic_ios.tcc \
 /usr/include/c++/5/bits/ostream.tcc /usr/include/c++/5/istream \
 /usr/include/c++/5/bits/istream.tcc \
 graphics/components/../../math/vector2f.h \
 graphics/components/../../graphics/components/image.h \
 graphics/components/../../data/gameData.h /usr/include/c++/5/map \
 /usr/include/c++/5/bits/stl_tree.h \
 /usr/include/c++/5/ext/aligned_buffer.h \
 /usr/include/c++/5/bits/stl_map.h /usr/include/c++/5/tuple \
 /usr/include/c++/5/utility /usr/include/c++/5/bits/stl_relops.h \
 /usr/include/c++/5/array /usr/include/c++/5/bits/uses_allocator.h \
 /usr/include/c++/5/bits/stl_multimap.h \
 graphics/components/../../data/../math/vector2f.h \
 graphics/components/../../data/parseXML.h /usr/include/c++/5/fstream \
 /usr/include/c++/5/bits/codecvt.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/basic_file.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++io.h \
 /usr/include/c++/5/bits/fstream.tcc /usr/include/c++/5/deque \
 /usr/include/c++/5/bits/stl_construct.h \
 /usr/include/c++/5/bits/stl_uninitialized.h \
 /usr/include/c++/5/bits/stl_deque.h /usr/include/c++/5/bits/deque.tcc \
 /usr/include/expat.h /usr/include/expat_external.h \
 graphics/components/image.h graphics/components/../ioMod.h  \
 graphics/components/../viewport.h graphics/components/../../drawable.h \
 graphics/components/../../data/gameData.h \
 graphics/components/../components/hud.h /usr/include/c++/5/sstream \
 /usr/include/c++/5/bits/sstream.tcc \
 graphics/components/../components/../ioMod.h \
 graphics/components/../components/../../engine/clock.h \
 graphics/components/../components/../../data/gameData.h \
 graphics/components/../components/menu.h \
 graphics/components/../../enums/state.h \
 graphics/components/../components/image.h \
 graphics/components/../renderContext.h \
 graphics/components/../imageFactory.h /usr/include/c++/5/vector \
 /usr/include/c++/5/bits/stl_vector.h \
 /usr/include/c++/5/bits/stl_bvector.h /usr/include/c++/5/bits/vector.tcc \
 graphics/components/../../sprites/spriteSheet.h  \
 
	$(CC) -c $< -o $@ $(CFLAGS)
$(OBJDIR)world.o: graphics/components/world.cpp /usr/include/stdc-predef.h \
 /usr/include/c++/5/iostream \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/os_defines.h \
 /usr/include/features.h /usr/include/x86_64-linux-gnu/sys/cdefs.h \
 /usr/include/x86_64-linux-gnu/bits/wordsize.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs-64.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/cpu_defines.h \
 /usr/include/c++/5/ostream /usr/include/c++/5/ios \
 /usr/include/c++/5/iosfwd /usr/include/c++/5/bits/stringfwd.h \
 /usr/include/c++/5/bits/memoryfwd.h /usr/include/c++/5/bits/postypes.h \
 /usr/include/c++/5/cwchar /usr/include/wchar.h /usr/include/stdio.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdarg.h \
 /usr/include/x86_64-linux-gnu/bits/wchar.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h /usr/include/xlocale.h \
 /usr/include/c++/5/exception \
 /usr/include/c++/5/bits/atomic_lockfree_defines.h \
 /usr/include/c++/5/bits/exception_ptr.h \
 /usr/include/c++/5/bits/exception_defines.h \
 /usr/include/c++/5/bits/nested_exception.h \
 /usr/include/c++/5/bits/char_traits.h \
 /usr/include/c++/5/bits/stl_algobase.h \
 /usr/include/c++/5/bits/functexcept.h \
 /usr/include/c++/5/bits/cpp_type_traits.h \
 /usr/include/c++/5/ext/type_traits.h \
 /usr/include/c++/5/ext/numeric_traits.h \
 /usr/include/c++/5/bits/stl_pair.h /usr/include/c++/5/bits/move.h \
 /usr/include/c++/5/bits/concept_check.h /usr/include/c++/5/type_traits \
 /usr/include/c++/5/bits/stl_iterator_base_types.h \
 /usr/include/c++/5/bits/stl_iterator_base_funcs.h \
 /usr/include/c++/5/debug/debug.h /usr/include/c++/5/bits/stl_iterator.h \
 /usr/include/c++/5/bits/ptr_traits.h \
 /usr/include/c++/5/bits/predefined_ops.h /usr/include/c++/5/cstdint \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdint.h /usr/include/stdint.h \
 /usr/include/c++/5/bits/localefwd.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++locale.h \
 /usr/include/c++/5/clocale /usr/include/locale.h \
 /usr/include/x86_64-linux-gnu/bits/locale.h /usr/include/c++/5/cctype \
 /usr/include/ctype.h /usr/include/x86_64-linux-gnu/bits/types.h \
 /usr/include/x86_64-linux-gnu/bits/typesizes.h /usr/include/endian.h \
 /usr/include/x86_64-linux-gnu/bits/endian.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap-16.h \
 /usr/include/c++/5/bits/ios_base.h /usr/include/c++/5/ext/atomicity.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr-default.h \
 /usr/include/pthread.h /usr/include/sched.h /usr/include/time.h \
 /usr/include/x86_64-linux-gnu/bits/sched.h \
 /usr/include/x86_64-linux-gnu/bits/time.h \
 /usr/include/x86_64-linux-gnu/bits/timex.h \
 /usr/include/x86_64-linux-gnu/bits/pthreadtypes.h \
 /usr/include/x86_64-linux-gnu/bits/setjmp.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h \
 /usr/include/c++/5/bits/locale_classes.h /usr/include/c++/5/string \
 /usr/include/c++/5/bits/allocator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++allocator.h \
 /usr/include/c++/5/ext/new_allocator.h /usr/include/c++/5/new \
 /usr/include/c++/5/bits/ostream_insert.h \
 /usr/include/c++/5/bits/cxxabi_forced.h \
 /usr/include/c++/5/bits/stl_function.h \
 /usr/include/c++/5/backward/binders.h \
 /usr/include/c++/5/bits/range_access.h \
 /usr/include/c++/5/initializer_list \
 /usr/include/c++/5/bits/basic_string.h \
 /usr/include/c++/5/ext/alloc_traits.h \
 /usr/include/c++/5/bits/alloc_traits.h \
 /usr/include/c++/5/ext/string_conversions.h /usr/include/c++/5/cstdlib \
 /usr/include/stdlib.h /usr/include/x86_64-linux-gnu/bits/waitflags.h \
 /usr/include/x86_64-linux-gnu/bits/waitstatus.h \
 /usr/include/x86_64-linux-gnu/sys/types.h \
 /usr/include/x86_64-linux-gnu/sys/select.h \
 /usr/include/x86_64-linux-gnu/bits/select.h \
 /usr/include/x86_64-linux-gnu/bits/sigset.h \
 /usr/include/x86_64-linux-gnu/sys/sysmacros.h /usr/include/alloca.h \
 /usr/include/x86_64-linux-gnu/bits/stdlib-float.h \
 /usr/include/c++/5/cstdio /usr/include/libio.h /usr/include/_G_config.h \
 /usr/include/x86_64-linux-gnu/bits/stdio_lim.h \
 /usr/include/x86_64-linux-gnu/bits/sys_errlist.h \
 /usr/include/c++/5/cerrno /usr/include/errno.h \
 /usr/include/x86_64-linux-gnu/bits/errno.h /usr/include/linux/errno.h \
 /usr/include/x86_64-linux-gnu/asm/errno.h \
 /usr/include/asm-generic/errno.h /usr/include/asm-generic/errno-base.h \
 /usr/include/c++/5/bits/functional_hash.h \
 /usr/include/c++/5/bits/hash_bytes.h \
 /usr/include/c++/5/bits/basic_string.tcc \
 /usr/include/c++/5/bits/locale_classes.tcc \
 /usr/include/c++/5/system_error \
 /usr/include/x86_64-linux-gnu/c++/5/bits/error_constants.h \
 /usr/include/c++/5/stdexcept /usr/include/c++/5/streambuf \
 /usr/include/c++/5/bits/streambuf.tcc \
 /usr/include/c++/5/bits/basic_ios.h \
 /usr/include/c++/5/bits/locale_facets.h /usr/include/c++/5/cwctype \
 /usr/include/wctype.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_base.h \
 /usr/include/c++/5/bits/streambuf_iterator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_inline.h \
 /usr/include/c++/5/bits/locale_facets.tcc \
 /usr/include/c++/5/bits/basic_ios.tcc \
 /usr/include/c++/5/bits/ostream.tcc /usr/include/c++/5/istream \
 /usr/include/c++/5/bits/istream.tcc graphics/components/world.h \
 graphics/components/image.h  graphics/components/../viewport.h \
 graphics/components/../../drawable.h \
 graphics/components/../../math/vector2f.h \
 graphics/components/../../graphics/components/image.h \
 graphics/components/../../data/gameData.h /usr/include/c++/5/map \
 /usr/include/c++/5/bits/stl_tree.h \
 /usr/include/c++/5/ext/aligned_buffer.h \
 /usr/include/c++/5/bits/stl_map.h /usr/include/c++/5/tuple \
 /usr/include/c++/5/utility /usr/include/c++/5/bits/stl_relops.h \
 /usr/include/c++/5/array /usr/include/c++/5/bits/uses_allocator.h \
 /usr/include/c++/5/bits/stl_multimap.h \
 graphics/components/../../data/../math/vector2f.h \
 graphics/components/../../data/parseXML.h /usr/include/c++/5/fstream \
 /usr/include/c++/5/bits/codecvt.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/basic_file.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++io.h \
 /usr/include/c++/5/bits/fstream.tcc /usr/include/c++/5/deque \
 /usr/include/c++/5/bits/stl_construct.h \
 /usr/include/c++/5/bits/stl_uninitialized.h \
 /usr/include/c++/5/bits/stl_deque.h /usr/include/c++/5/bits/deque.tcc \
 /usr/include/expat.h /usr/include/expat_external.h \
 graphics/components/../../data/gameData.h \
 graphics/components/../components/hud.h /usr/include/c++/5/sstream \
 /usr/include/c++/5/bits/sstream.tcc \
 graphics/components/../components/../ioMod.h  \
 graphics/components/../components/../../engine/clock.h \
 graphics/components/../components/../../data/gameData.h \
 graphics/components/../components/menu.h \
 graphics/components/../../enums/state.h \
 graphics/components/../components/image.h \
 graphics/components/../imageFactory.h /usr/include/c++/5/vector \
 /usr/include/c++/5/bits/stl_vector.h \
 /usr/include/c++/5/bits/stl_bvector.h /usr/include/c++/5/bits/vector.tcc \
 graphics/components/../../sprites/spriteSheet.h  \
  graphics/components/../../data/gameData.h
	$(CC) -c $< -o $@ $(CFLAGS)
$(OBJDIR)menu.o: graphics/components/menu.cpp /usr/include/stdc-predef.h \
 /usr/include/c++/5/string \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/os_defines.h \
 /usr/include/features.h /usr/include/x86_64-linux-gnu/sys/cdefs.h \
 /usr/include/x86_64-linux-gnu/bits/wordsize.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs-64.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/cpu_defines.h \
 /usr/include/c++/5/bits/stringfwd.h /usr/include/c++/5/bits/memoryfwd.h \
 /usr/include/c++/5/bits/char_traits.h \
 /usr/include/c++/5/bits/stl_algobase.h \
 /usr/include/c++/5/bits/functexcept.h \
 /usr/include/c++/5/bits/exception_defines.h \
 /usr/include/c++/5/bits/cpp_type_traits.h \
 /usr/include/c++/5/ext/type_traits.h \
 /usr/include/c++/5/ext/numeric_traits.h \
 /usr/include/c++/5/bits/stl_pair.h /usr/include/c++/5/bits/move.h \
 /usr/include/c++/5/bits/concept_check.h /usr/include/c++/5/type_traits \
 /usr/include/c++/5/bits/stl_iterator_base_types.h \
 /usr/include/c++/5/bits/stl_iterator_base_funcs.h \
 /usr/include/c++/5/debug/debug.h /usr/include/c++/5/bits/stl_iterator.h \
 /usr/include/c++/5/bits/ptr_traits.h \
 /usr/include/c++/5/bits/predefined_ops.h \
 /usr/include/c++/5/bits/postypes.h /usr/include/c++/5/cwchar \
 /usr/include/wchar.h /usr/include/stdio.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdarg.h \
 /usr/include/x86_64-linux-gnu/bits/wchar.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h /usr/include/xlocale.h \
 /usr/include/c++/5/cstdint \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdint.h /usr/include/stdint.h \
 /usr/include/c++/5/bits/allocator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++allocator.h \
 /usr/include/c++/5/ext/new_allocator.h /usr/include/c++/5/new \
 /usr/include/c++/5/exception \
 /usr/include/c++/5/bits/atomic_lockfree_defines.h \
 /usr/include/c++/5/bits/exception_ptr.h \
 /usr/include/c++/5/bits/nested_exception.h \
 /usr/include/c++/5/bits/localefwd.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++locale.h \
 /usr/include/c++/5/clocale /usr/include/locale.h \
 /usr/include/x86_64-linux-gnu/bits/locale.h /usr/include/c++/5/iosfwd \
 /usr/include/c++/5/cctype /usr/include/ctype.h \
 /usr/include/x86_64-linux-gnu/bits/types.h \
 /usr/include/x86_64-linux-gnu/bits/typesizes.h /usr/include/endian.h \
 /usr/include/x86_64-linux-gnu/bits/endian.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap-16.h \
 /usr/include/c++/5/bits/ostream_insert.h \
 /usr/include/c++/5/bits/cxxabi_forced.h \
 /usr/include/c++/5/bits/stl_function.h \
 /usr/include/c++/5/backward/binders.h \
 /usr/include/c++/5/bits/range_access.h \
 /usr/include/c++/5/initializer_list \
 /usr/include/c++/5/bits/basic_string.h \
 /usr/include/c++/5/ext/atomicity.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr-default.h \
 /usr/include/pthread.h /usr/include/sched.h /usr/include/time.h \
 /usr/include/x86_64-linux-gnu/bits/sched.h \
 /usr/include/x86_64-linux-gnu/bits/time.h \
 /usr/include/x86_64-linux-gnu/bits/timex.h \
 /usr/include/x86_64-linux-gnu/bits/pthreadtypes.h \
 /usr/include/x86_64-linux-gnu/bits/setjmp.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h \
 /usr/include/c++/5/ext/alloc_traits.h \
 /usr/include/c++/5/bits/alloc_traits.h \
 /usr/include/c++/5/ext/string_conversions.h /usr/include/c++/5/cstdlib \
 /usr/include/stdlib.h /usr/include/x86_64-linux-gnu/bits/waitflags.h \
 /usr/include/x86_64-linux-gnu/bits/waitstatus.h \
 /usr/include/x86_64-linux-gnu/sys/types.h \
 /usr/include/x86_64-linux-gnu/sys/select.h \
 /usr/include/x86_64-linux-gnu/bits/select.h \
 /usr/include/x86_64-linux-gnu/bits/sigset.h \
 /usr/include/x86_64-linux-gnu/sys/sysmacros.h /usr/include/alloca.h \
 /usr/include/x86_64-linux-gnu/bits/stdlib-float.h \
 /usr/include/c++/5/cstdio /usr/include/libio.h /usr/include/_G_config.h \
 /usr/include/x86_64-linux-gnu/bits/stdio_lim.h \
 /usr/include/x86_64-linux-gnu/bits/sys_errlist.h \
 /usr/include/c++/5/cerrno /usr/include/errno.h \
 /usr/include/x86_64-linux-gnu/bits/errno.h /usr/include/linux/errno.h \
 /usr/include/x86_64-linux-gnu/asm/errno.h \
 /usr/include/asm-generic/errno.h /usr/include/asm-generic/errno-base.h \
 /usr/include/c++/5/bits/functional_hash.h \
 /usr/include/c++/5/bits/hash_bytes.h \
 /usr/include/c++/5/bits/basic_string.tcc graphics/components/menu.h \
  graphics/components/../ioMod.h /usr/include/c++/5/iostream \
 /usr/include/c++/5/ostream /usr/include/c++/5/ios \
 /usr/include/c++/5/bits/ios_base.h \
 /usr/include/c++/5/bits/locale_classes.h \
 /usr/include/c++/5/bits/locale_classes.tcc \
 /usr/include/c++/5/system_error \
 /usr/include/x86_64-linux-gnu/c++/5/bits/error_constants.h \
 /usr/include/c++/5/stdexcept /usr/include/c++/5/streambuf \
 /usr/include/c++/5/bits/streambuf.tcc \
 /usr/include/c++/5/bits/basic_ios.h \
 /usr/include/c++/5/bits/locale_facets.h /usr/include/c++/5/cwctype \
 /usr/include/wctype.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_base.h \
 /usr/include/c++/5/bits/streambuf_iterator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_inline.h \
 /usr/include/c++/5/bits/locale_facets.tcc \
 /usr/include/c++/5/bits/basic_ios.tcc \
 /usr/include/c++/5/bits/ostream.tcc /usr/include/c++/5/istream \
 /usr/include/c++/5/bits/istream.tcc  \
 graphics/components/../../data/gameData.h /usr/include/c++/5/map \
 /usr/include/c++/5/bits/stl_tree.h \
 /usr/include/c++/5/ext/aligned_buffer.h \
 /usr/include/c++/5/bits/stl_map.h /usr/include/c++/5/tuple \
 /usr/include/c++/5/utility /usr/include/c++/5/bits/stl_relops.h \
 /usr/include/c++/5/array /usr/include/c++/5/bits/uses_allocator.h \
 /usr/include/c++/5/bits/stl_multimap.h \
 graphics/components/../../data/../math/vector2f.h \
 graphics/components/../../data/parseXML.h /usr/include/c++/5/fstream \
 /usr/include/c++/5/bits/codecvt.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/basic_file.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++io.h \
 /usr/include/c++/5/bits/fstream.tcc /usr/include/c++/5/deque \
 /usr/include/c++/5/bits/stl_construct.h \
 /usr/include/c++/5/bits/stl_uninitialized.h \
 /usr/include/c++/5/bits/stl_deque.h /usr/include/c++/5/bits/deque.tcc \
 /usr/include/expat.h /usr/include/expat_external.h
	$(CC) -c $< -o $@ $(CFLAGS)
$(OBJDIR)imageFactory.o: graphics/imageFactory.cpp /usr/include/stdc-predef.h \
 graphics/ioMod.h /usr/include/c++/5/iostream \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/os_defines.h \
 /usr/include/features.h /usr/include/x86_64-linux-gnu/sys/cdefs.h \
 /usr/include/x86_64-linux-gnu/bits/wordsize.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs-64.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/cpu_defines.h \
 /usr/include/c++/5/ostream /usr/include/c++/5/ios \
 /usr/include/c++/5/iosfwd /usr/include/c++/5/bits/stringfwd.h \
 /usr/include/c++/5/bits/memoryfwd.h /usr/include/c++/5/bits/postypes.h \
 /usr/include/c++/5/cwchar /usr/include/wchar.h /usr/include/stdio.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdarg.h \
 /usr/include/x86_64-linux-gnu/bits/wchar.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h /usr/include/xlocale.h \
 /usr/include/c++/5/exception \
 /usr/include/c++/5/bits/atomic_lockfree_defines.h \
 /usr/include/c++/5/bits/exception_ptr.h \
 /usr/include/c++/5/bits/exception_defines.h \
 /usr/include/c++/5/bits/nested_exception.h \
 /usr/include/c++/5/bits/char_traits.h \
 /usr/include/c++/5/bits/stl_algobase.h \
 /usr/include/c++/5/bits/functexcept.h \
 /usr/include/c++/5/bits/cpp_type_traits.h \
 /usr/include/c++/5/ext/type_traits.h \
 /usr/include/c++/5/ext/numeric_traits.h \
 /usr/include/c++/5/bits/stl_pair.h /usr/include/c++/5/bits/move.h \
 /usr/include/c++/5/bits/concept_check.h /usr/include/c++/5/type_traits \
 /usr/include/c++/5/bits/stl_iterator_base_types.h \
 /usr/include/c++/5/bits/stl_iterator_base_funcs.h \
 /usr/include/c++/5/debug/debug.h /usr/include/c++/5/bits/stl_iterator.h \
 /usr/include/c++/5/bits/ptr_traits.h \
 /usr/include/c++/5/bits/predefined_ops.h /usr/include/c++/5/cstdint \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdint.h /usr/include/stdint.h \
 /usr/include/c++/5/bits/localefwd.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++locale.h \
 /usr/include/c++/5/clocale /usr/include/locale.h \
 /usr/include/x86_64-linux-gnu/bits/locale.h /usr/include/c++/5/cctype \
 /usr/include/ctype.h /usr/include/x86_64-linux-gnu/bits/types.h \
 /usr/include/x86_64-linux-gnu/bits/typesizes.h /usr/include/endian.h \
 /usr/include/x86_64-linux-gnu/bits/endian.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap-16.h \
 /usr/include/c++/5/bits/ios_base.h /usr/include/c++/5/ext/atomicity.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr-default.h \
 /usr/include/pthread.h /usr/include/sched.h /usr/include/time.h \
 /usr/include/x86_64-linux-gnu/bits/sched.h \
 /usr/include/x86_64-linux-gnu/bits/time.h \
 /usr/include/x86_64-linux-gnu/bits/timex.h \
 /usr/include/x86_64-linux-gnu/bits/pthreadtypes.h \
 /usr/include/x86_64-linux-gnu/bits/setjmp.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h \
 /usr/include/c++/5/bits/locale_classes.h /usr/include/c++/5/string \
 /usr/include/c++/5/bits/allocator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++allocator.h \
 /usr/include/c++/5/ext/new_allocator.h /usr/include/c++/5/new \
 /usr/include/c++/5/bits/ostream_insert.h \
 /usr/include/c++/5/bits/cxxabi_forced.h \
 /usr/include/c++/5/bits/stl_function.h \
 /usr/include/c++/5/backward/binders.h \
 /usr/include/c++/5/bits/range_access.h \
 /usr/include/c++/5/initializer_list \
 /usr/include/c++/5/bits/basic_string.h \
 /usr/include/c++/5/ext/alloc_traits.h \
 /usr/include/c++/5/bits/alloc_traits.h \
 /usr/include/c++/5/ext/string_conversions.h /usr/include/c++/5/cstdlib \
 /usr/include/stdlib.h /usr/include/x86_64-linux-gnu/bits/waitflags.h \
 /usr/include/x86_64-linux-gnu/bits/waitstatus.h \
 /usr/include/x86_64-linux-gnu/sys/types.h \
 /usr/include/x86_64-linux-gnu/sys/select.h \
 /usr/include/x86_64-linux-gnu/bits/select.h \
 /usr/include/x86_64-linux-gnu/bits/sigset.h \
 /usr/include/x86_64-linux-gnu/sys/sysmacros.h /usr/include/alloca.h \
 /usr/include/x86_64-linux-gnu/bits/stdlib-float.h \
 /usr/include/c++/5/cstdio /usr/include/libio.h /usr/include/_G_config.h \
 /usr/include/x86_64-linux-gnu/bits/stdio_lim.h \
 /usr/include/x86_64-linux-gnu/bits/sys_errlist.h \
 /usr/include/c++/5/cerrno /usr/include/errno.h \
 /usr/include/x86_64-linux-gnu/bits/errno.h /usr/include/linux/errno.h \
 /usr/include/x86_64-linux-gnu/asm/errno.h \
 /usr/include/asm-generic/errno.h /usr/include/asm-generic/errno-base.h \
 /usr/include/c++/5/bits/functional_hash.h \
 /usr/include/c++/5/bits/hash_bytes.h \
 /usr/include/c++/5/bits/basic_string.tcc \
 /usr/include/c++/5/bits/locale_classes.tcc \
 /usr/include/c++/5/system_error \
 /usr/include/x86_64-linux-gnu/c++/5/bits/error_constants.h \
 /usr/include/c++/5/stdexcept /usr/include/c++/5/streambuf \
 /usr/include/c++/5/bits/streambuf.tcc \
 /usr/include/c++/5/bits/basic_ios.h \
 /usr/include/c++/5/bits/locale_facets.h /usr/include/c++/5/cwctype \
 /usr/include/wctype.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_base.h \
 /usr/include/c++/5/bits/streambuf_iterator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_inline.h \
 /usr/include/c++/5/bits/locale_facets.tcc \
 /usr/include/c++/5/bits/basic_ios.tcc \
 /usr/include/c++/5/bits/ostream.tcc /usr/include/c++/5/istream \
 /usr/include/c++/5/bits/istream.tcc   \
 graphics/../math/vector2f.h graphics/renderContext.h \
 graphics/imageFactory.h /usr/include/c++/5/vector \
 /usr/include/c++/5/bits/stl_construct.h \
 /usr/include/c++/5/bits/stl_uninitialized.h \
 /usr/include/c++/5/bits/stl_vector.h \
 /usr/include/c++/5/bits/stl_bvector.h /usr/include/c++/5/bits/vector.tcc \
 /usr/include/c++/5/map /usr/include/c++/5/bits/stl_tree.h \
 /usr/include/c++/5/ext/aligned_buffer.h \
 /usr/include/c++/5/bits/stl_map.h /usr/include/c++/5/tuple \
 /usr/include/c++/5/utility /usr/include/c++/5/bits/stl_relops.h \
 /usr/include/c++/5/array /usr/include/c++/5/bits/uses_allocator.h \
 /usr/include/c++/5/bits/stl_multimap.h graphics/components/image.h \
 graphics/../data/gameData.h graphics/../data/../math/vector2f.h \
 graphics/../data/parseXML.h /usr/include/c++/5/fstream \
 /usr/include/c++/5/bits/codecvt.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/basic_file.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++io.h \
 /usr/include/c++/5/bits/fstream.tcc /usr/include/c++/5/deque \
 /usr/include/c++/5/bits/stl_deque.h /usr/include/c++/5/bits/deque.tcc \
 /usr/include/expat.h /usr/include/expat_external.h \
 graphics/../sprites/spriteSheet.h  
	$(CC) -c $< -o $@ $(CFLAGS)
$(OBJDIR)ioMod.o: graphics/ioMod.cpp /usr/include/stdc-predef.h  \
 graphics/ioMod.h /usr/include/c++/5/iostream \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/os_defines.h \
 /usr/include/features.h /usr/include/x86_64-linux-gnu/sys/cdefs.h \
 /usr/include/x86_64-linux-gnu/bits/wordsize.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs-64.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/cpu_defines.h \
 /usr/include/c++/5/ostream /usr/include/c++/5/ios \
 /usr/include/c++/5/iosfwd /usr/include/c++/5/bits/stringfwd.h \
 /usr/include/c++/5/bits/memoryfwd.h /usr/include/c++/5/bits/postypes.h \
 /usr/include/c++/5/cwchar /usr/include/wchar.h /usr/include/stdio.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdarg.h \
 /usr/include/x86_64-linux-gnu/bits/wchar.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h /usr/include/xlocale.h \
 /usr/include/c++/5/exception \
 /usr/include/c++/5/bits/atomic_lockfree_defines.h \
 /usr/include/c++/5/bits/exception_ptr.h \
 /usr/include/c++/5/bits/exception_defines.h \
 /usr/include/c++/5/bits/nested_exception.h \
 /usr/include/c++/5/bits/char_traits.h \
 /usr/include/c++/5/bits/stl_algobase.h \
 /usr/include/c++/5/bits/functexcept.h \
 /usr/include/c++/5/bits/cpp_type_traits.h \
 /usr/include/c++/5/ext/type_traits.h \
 /usr/include/c++/5/ext/numeric_traits.h \
 /usr/include/c++/5/bits/stl_pair.h /usr/include/c++/5/bits/move.h \
 /usr/include/c++/5/bits/concept_check.h /usr/include/c++/5/type_traits \
 /usr/include/c++/5/bits/stl_iterator_base_types.h \
 /usr/include/c++/5/bits/stl_iterator_base_funcs.h \
 /usr/include/c++/5/debug/debug.h /usr/include/c++/5/bits/stl_iterator.h \
 /usr/include/c++/5/bits/ptr_traits.h \
 /usr/include/c++/5/bits/predefined_ops.h /usr/include/c++/5/cstdint \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdint.h /usr/include/stdint.h \
 /usr/include/c++/5/bits/localefwd.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++locale.h \
 /usr/include/c++/5/clocale /usr/include/locale.h \
 /usr/include/x86_64-linux-gnu/bits/locale.h /usr/include/c++/5/cctype \
 /usr/include/ctype.h /usr/include/x86_64-linux-gnu/bits/types.h \
 /usr/include/x86_64-linux-gnu/bits/typesizes.h /usr/include/endian.h \
 /usr/include/x86_64-linux-gnu/bits/endian.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap-16.h \
 /usr/include/c++/5/bits/ios_base.h /usr/include/c++/5/ext/atomicity.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr-default.h \
 /usr/include/pthread.h /usr/include/sched.h /usr/include/time.h \
 /usr/include/x86_64-linux-gnu/bits/sched.h \
 /usr/include/x86_64-linux-gnu/bits/time.h \
 /usr/include/x86_64-linux-gnu/bits/timex.h \
 /usr/include/x86_64-linux-gnu/bits/pthreadtypes.h \
 /usr/include/x86_64-linux-gnu/bits/setjmp.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h \
 /usr/include/c++/5/bits/locale_classes.h /usr/include/c++/5/string \
 /usr/include/c++/5/bits/allocator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++allocator.h \
 /usr/include/c++/5/ext/new_allocator.h /usr/include/c++/5/new \
 /usr/include/c++/5/bits/ostream_insert.h \
 /usr/include/c++/5/bits/cxxabi_forced.h \
 /usr/include/c++/5/bits/stl_function.h \
 /usr/include/c++/5/backward/binders.h \
 /usr/include/c++/5/bits/range_access.h \
 /usr/include/c++/5/initializer_list \
 /usr/include/c++/5/bits/basic_string.h \
 /usr/include/c++/5/ext/alloc_traits.h \
 /usr/include/c++/5/bits/alloc_traits.h \
 /usr/include/c++/5/ext/string_conversions.h /usr/include/c++/5/cstdlib \
 /usr/include/stdlib.h /usr/include/x86_64-linux-gnu/bits/waitflags.h \
 /usr/include/x86_64-linux-gnu/bits/waitstatus.h \
 /usr/include/x86_64-linux-gnu/sys/types.h \
 /usr/include/x86_64-linux-gnu/sys/select.h \
 /usr/include/x86_64-linux-gnu/bits/select.h \
 /usr/include/x86_64-linux-gnu/bits/sigset.h \
 /usr/include/x86_64-linux-gnu/sys/sysmacros.h /usr/include/alloca.h \
 /usr/include/x86_64-linux-gnu/bits/stdlib-float.h \
 /usr/include/c++/5/cstdio /usr/include/libio.h /usr/include/_G_config.h \
 /usr/include/x86_64-linux-gnu/bits/stdio_lim.h \
 /usr/include/x86_64-linux-gnu/bits/sys_errlist.h \
 /usr/include/c++/5/cerrno /usr/include/errno.h \
 /usr/include/x86_64-linux-gnu/bits/errno.h /usr/include/linux/errno.h \
 /usr/include/x86_64-linux-gnu/asm/errno.h \
 /usr/include/asm-generic/errno.h /usr/include/asm-generic/errno-base.h \
 /usr/include/c++/5/bits/functional_hash.h \
 /usr/include/c++/5/bits/hash_bytes.h \
 /usr/include/c++/5/bits/basic_string.tcc \
 /usr/include/c++/5/bits/locale_classes.tcc \
 /usr/include/c++/5/system_error \
 /usr/include/x86_64-linux-gnu/c++/5/bits/error_constants.h \
 /usr/include/c++/5/stdexcept /usr/include/c++/5/streambuf \
 /usr/include/c++/5/bits/streambuf.tcc \
 /usr/include/c++/5/bits/basic_ios.h \
 /usr/include/c++/5/bits/locale_facets.h /usr/include/c++/5/cwctype \
 /usr/include/wctype.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_base.h \
 /usr/include/c++/5/bits/streambuf_iterator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_inline.h \
 /usr/include/c++/5/bits/locale_facets.tcc \
 /usr/include/c++/5/bits/basic_ios.tcc \
 /usr/include/c++/5/bits/ostream.tcc /usr/include/c++/5/istream \
 /usr/include/c++/5/bits/istream.tcc   \
 graphics/../data/gameData.h /usr/include/c++/5/map \
 /usr/include/c++/5/bits/stl_tree.h \
 /usr/include/c++/5/ext/aligned_buffer.h \
 /usr/include/c++/5/bits/stl_map.h /usr/include/c++/5/tuple \
 /usr/include/c++/5/utility /usr/include/c++/5/bits/stl_relops.h \
 /usr/include/c++/5/array /usr/include/c++/5/bits/uses_allocator.h \
 /usr/include/c++/5/bits/stl_multimap.h \
 graphics/../data/../math/vector2f.h graphics/../data/parseXML.h \
 /usr/include/c++/5/fstream /usr/include/c++/5/bits/codecvt.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/basic_file.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++io.h \
 /usr/include/c++/5/bits/fstream.tcc /usr/include/c++/5/deque \
 /usr/include/c++/5/bits/stl_construct.h \
 /usr/include/c++/5/bits/stl_uninitialized.h \
 /usr/include/c++/5/bits/stl_deque.h /usr/include/c++/5/bits/deque.tcc \
 /usr/include/expat.h /usr/include/expat_external.h \
 graphics/renderContext.h graphics/imageFactory.h \
 /usr/include/c++/5/vector /usr/include/c++/5/bits/stl_vector.h \
 /usr/include/c++/5/bits/stl_bvector.h /usr/include/c++/5/bits/vector.tcc \
 graphics/components/image.h graphics/../sprites/spriteSheet.h \
 
	$(CC) -c $< -o $@ $(CFLAGS)
$(OBJDIR)renderContext.o: graphics/renderContext.cpp /usr/include/stdc-predef.h \
 graphics/renderContext.h /usr/include/c++/5/iostream \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/os_defines.h \
 /usr/include/features.h /usr/include/x86_64-linux-gnu/sys/cdefs.h \
 /usr/include/x86_64-linux-gnu/bits/wordsize.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs-64.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/cpu_defines.h \
 /usr/include/c++/5/ostream /usr/include/c++/5/ios \
 /usr/include/c++/5/iosfwd /usr/include/c++/5/bits/stringfwd.h \
 /usr/include/c++/5/bits/memoryfwd.h /usr/include/c++/5/bits/postypes.h \
 /usr/include/c++/5/cwchar /usr/include/wchar.h /usr/include/stdio.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdarg.h \
 /usr/include/x86_64-linux-gnu/bits/wchar.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h /usr/include/xlocale.h \
 /usr/include/c++/5/exception \
 /usr/include/c++/5/bits/atomic_lockfree_defines.h \
 /usr/include/c++/5/bits/exception_ptr.h \
 /usr/include/c++/5/bits/exception_defines.h \
 /usr/include/c++/5/bits/nested_exception.h \
 /usr/include/c++/5/bits/char_traits.h \
 /usr/include/c++/5/bits/stl_algobase.h \
 /usr/include/c++/5/bits/functexcept.h \
 /usr/include/c++/5/bits/cpp_type_traits.h \
 /usr/include/c++/5/ext/type_traits.h \
 /usr/include/c++/5/ext/numeric_traits.h \
 /usr/include/c++/5/bits/stl_pair.h /usr/include/c++/5/bits/move.h \
 /usr/include/c++/5/bits/concept_check.h /usr/include/c++/5/type_traits \
 /usr/include/c++/5/bits/stl_iterator_base_types.h \
 /usr/include/c++/5/bits/stl_iterator_base_funcs.h \
 /usr/include/c++/5/debug/debug.h /usr/include/c++/5/bits/stl_iterator.h \
 /usr/include/c++/5/bits/ptr_traits.h \
 /usr/include/c++/5/bits/predefined_ops.h /usr/include/c++/5/cstdint \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdint.h /usr/include/stdint.h \
 /usr/include/c++/5/bits/localefwd.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++locale.h \
 /usr/include/c++/5/clocale /usr/include/locale.h \
 /usr/include/x86_64-linux-gnu/bits/locale.h /usr/include/c++/5/cctype \
 /usr/include/ctype.h /usr/include/x86_64-linux-gnu/bits/types.h \
 /usr/include/x86_64-linux-gnu/bits/typesizes.h /usr/include/endian.h \
 /usr/include/x86_64-linux-gnu/bits/endian.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap-16.h \
 /usr/include/c++/5/bits/ios_base.h /usr/include/c++/5/ext/atomicity.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr-default.h \
 /usr/include/pthread.h /usr/include/sched.h /usr/include/time.h \
 /usr/include/x86_64-linux-gnu/bits/sched.h \
 /usr/include/x86_64-linux-gnu/bits/time.h \
 /usr/include/x86_64-linux-gnu/bits/timex.h \
 /usr/include/x86_64-linux-gnu/bits/pthreadtypes.h \
 /usr/include/x86_64-linux-gnu/bits/setjmp.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h \
 /usr/include/c++/5/bits/locale_classes.h /usr/include/c++/5/string \
 /usr/include/c++/5/bits/allocator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++allocator.h \
 /usr/include/c++/5/ext/new_allocator.h /usr/include/c++/5/new \
 /usr/include/c++/5/bits/ostream_insert.h \
 /usr/include/c++/5/bits/cxxabi_forced.h \
 /usr/include/c++/5/bits/stl_function.h \
 /usr/include/c++/5/backward/binders.h \
 /usr/include/c++/5/bits/range_access.h \
 /usr/include/c++/5/initializer_list \
 /usr/include/c++/5/bits/basic_string.h \
 /usr/include/c++/5/ext/alloc_traits.h \
 /usr/include/c++/5/bits/alloc_traits.h \
 /usr/include/c++/5/ext/string_conversions.h /usr/include/c++/5/cstdlib \
 /usr/include/stdlib.h /usr/include/x86_64-linux-gnu/bits/waitflags.h \
 /usr/include/x86_64-linux-gnu/bits/waitstatus.h \
 /usr/include/x86_64-linux-gnu/sys/types.h \
 /usr/include/x86_64-linux-gnu/sys/select.h \
 /usr/include/x86_64-linux-gnu/bits/select.h \
 /usr/include/x86_64-linux-gnu/bits/sigset.h \
 /usr/include/x86_64-linux-gnu/sys/sysmacros.h /usr/include/alloca.h \
 /usr/include/x86_64-linux-gnu/bits/stdlib-float.h \
 /usr/include/c++/5/cstdio /usr/include/libio.h /usr/include/_G_config.h \
 /usr/include/x86_64-linux-gnu/bits/stdio_lim.h \
 /usr/include/x86_64-linux-gnu/bits/sys_errlist.h \
 /usr/include/c++/5/cerrno /usr/include/errno.h \
 /usr/include/x86_64-linux-gnu/bits/errno.h /usr/include/linux/errno.h \
 /usr/include/x86_64-linux-gnu/asm/errno.h \
 /usr/include/asm-generic/errno.h /usr/include/asm-generic/errno-base.h \
 /usr/include/c++/5/bits/functional_hash.h \
 /usr/include/c++/5/bits/hash_bytes.h \
 /usr/include/c++/5/bits/basic_string.tcc \
 /usr/include/c++/5/bits/locale_classes.tcc \
 /usr/include/c++/5/system_error \
 /usr/include/x86_64-linux-gnu/c++/5/bits/error_constants.h \
 /usr/include/c++/5/stdexcept /usr/include/c++/5/streambuf \
 /usr/include/c++/5/bits/streambuf.tcc \
 /usr/include/c++/5/bits/basic_ios.h \
 /usr/include/c++/5/bits/locale_facets.h /usr/include/c++/5/cwctype \
 /usr/include/wctype.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_base.h \
 /usr/include/c++/5/bits/streambuf_iterator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_inline.h \
 /usr/include/c++/5/bits/locale_facets.tcc \
 /usr/include/c++/5/bits/basic_ios.tcc \
 /usr/include/c++/5/bits/ostream.tcc /usr/include/c++/5/istream \
 /usr/include/c++/5/bits/istream.tcc  graphics/imageFactory.h \
 /usr/include/c++/5/vector /usr/include/c++/5/bits/stl_construct.h \
 /usr/include/c++/5/bits/stl_uninitialized.h \
 /usr/include/c++/5/bits/stl_vector.h \
 /usr/include/c++/5/bits/stl_bvector.h /usr/include/c++/5/bits/vector.tcc \
 /usr/include/c++/5/map /usr/include/c++/5/bits/stl_tree.h \
 /usr/include/c++/5/ext/aligned_buffer.h \
 /usr/include/c++/5/bits/stl_map.h /usr/include/c++/5/tuple \
 /usr/include/c++/5/utility /usr/include/c++/5/bits/stl_relops.h \
 /usr/include/c++/5/array /usr/include/c++/5/bits/uses_allocator.h \
 /usr/include/c++/5/bits/stl_multimap.h graphics/components/image.h \
 graphics/../data/gameData.h graphics/../data/../math/vector2f.h \
 graphics/../data/parseXML.h /usr/include/c++/5/fstream \
 /usr/include/c++/5/bits/codecvt.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/basic_file.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++io.h \
 /usr/include/c++/5/bits/fstream.tcc /usr/include/c++/5/deque \
 /usr/include/c++/5/bits/stl_deque.h /usr/include/c++/5/bits/deque.tcc \
 /usr/include/expat.h /usr/include/expat_external.h \
 graphics/../sprites/spriteSheet.h  
	$(CC) -c $< -o $@ $(CFLAGS)
$(OBJDIR)viewport.o: graphics/viewport.cpp /usr/include/stdc-predef.h \
 /usr/include/c++/5/sstream /usr/include/c++/5/istream \
 /usr/include/c++/5/ios /usr/include/c++/5/iosfwd \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/os_defines.h \
 /usr/include/features.h /usr/include/x86_64-linux-gnu/sys/cdefs.h \
 /usr/include/x86_64-linux-gnu/bits/wordsize.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs-64.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/cpu_defines.h \
 /usr/include/c++/5/bits/stringfwd.h /usr/include/c++/5/bits/memoryfwd.h \
 /usr/include/c++/5/bits/postypes.h /usr/include/c++/5/cwchar \
 /usr/include/wchar.h /usr/include/stdio.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdarg.h \
 /usr/include/x86_64-linux-gnu/bits/wchar.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h /usr/include/xlocale.h \
 /usr/include/c++/5/exception \
 /usr/include/c++/5/bits/atomic_lockfree_defines.h \
 /usr/include/c++/5/bits/exception_ptr.h \
 /usr/include/c++/5/bits/exception_defines.h \
 /usr/include/c++/5/bits/nested_exception.h \
 /usr/include/c++/5/bits/char_traits.h \
 /usr/include/c++/5/bits/stl_algobase.h \
 /usr/include/c++/5/bits/functexcept.h \
 /usr/include/c++/5/bits/cpp_type_traits.h \
 /usr/include/c++/5/ext/type_traits.h \
 /usr/include/c++/5/ext/numeric_traits.h \
 /usr/include/c++/5/bits/stl_pair.h /usr/include/c++/5/bits/move.h \
 /usr/include/c++/5/bits/concept_check.h /usr/include/c++/5/type_traits \
 /usr/include/c++/5/bits/stl_iterator_base_types.h \
 /usr/include/c++/5/bits/stl_iterator_base_funcs.h \
 /usr/include/c++/5/debug/debug.h /usr/include/c++/5/bits/stl_iterator.h \
 /usr/include/c++/5/bits/ptr_traits.h \
 /usr/include/c++/5/bits/predefined_ops.h /usr/include/c++/5/cstdint \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdint.h /usr/include/stdint.h \
 /usr/include/c++/5/bits/localefwd.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++locale.h \
 /usr/include/c++/5/clocale /usr/include/locale.h \
 /usr/include/x86_64-linux-gnu/bits/locale.h /usr/include/c++/5/cctype \
 /usr/include/ctype.h /usr/include/x86_64-linux-gnu/bits/types.h \
 /usr/include/x86_64-linux-gnu/bits/typesizes.h /usr/include/endian.h \
 /usr/include/x86_64-linux-gnu/bits/endian.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap-16.h \
 /usr/include/c++/5/bits/ios_base.h /usr/include/c++/5/ext/atomicity.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr-default.h \
 /usr/include/pthread.h /usr/include/sched.h /usr/include/time.h \
 /usr/include/x86_64-linux-gnu/bits/sched.h \
 /usr/include/x86_64-linux-gnu/bits/time.h \
 /usr/include/x86_64-linux-gnu/bits/timex.h \
 /usr/include/x86_64-linux-gnu/bits/pthreadtypes.h \
 /usr/include/x86_64-linux-gnu/bits/setjmp.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h \
 /usr/include/c++/5/bits/locale_classes.h /usr/include/c++/5/string \
 /usr/include/c++/5/bits/allocator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++allocator.h \
 /usr/include/c++/5/ext/new_allocator.h /usr/include/c++/5/new \
 /usr/include/c++/5/bits/ostream_insert.h \
 /usr/include/c++/5/bits/cxxabi_forced.h \
 /usr/include/c++/5/bits/stl_function.h \
 /usr/include/c++/5/backward/binders.h \
 /usr/include/c++/5/bits/range_access.h \
 /usr/include/c++/5/initializer_list \
 /usr/include/c++/5/bits/basic_string.h \
 /usr/include/c++/5/ext/alloc_traits.h \
 /usr/include/c++/5/bits/alloc_traits.h \
 /usr/include/c++/5/ext/string_conversions.h /usr/include/c++/5/cstdlib \
 /usr/include/stdlib.h /usr/include/x86_64-linux-gnu/bits/waitflags.h \
 /usr/include/x86_64-linux-gnu/bits/waitstatus.h \
 /usr/include/x86_64-linux-gnu/sys/types.h \
 /usr/include/x86_64-linux-gnu/sys/select.h \
 /usr/include/x86_64-linux-gnu/bits/select.h \
 /usr/include/x86_64-linux-gnu/bits/sigset.h \
 /usr/include/x86_64-linux-gnu/sys/sysmacros.h /usr/include/alloca.h \
 /usr/include/x86_64-linux-gnu/bits/stdlib-float.h \
 /usr/include/c++/5/cstdio /usr/include/libio.h /usr/include/_G_config.h \
 /usr/include/x86_64-linux-gnu/bits/stdio_lim.h \
 /usr/include/x86_64-linux-gnu/bits/sys_errlist.h \
 /usr/include/c++/5/cerrno /usr/include/errno.h \
 /usr/include/x86_64-linux-gnu/bits/errno.h /usr/include/linux/errno.h \
 /usr/include/x86_64-linux-gnu/asm/errno.h \
 /usr/include/asm-generic/errno.h /usr/include/asm-generic/errno-base.h \
 /usr/include/c++/5/bits/functional_hash.h \
 /usr/include/c++/5/bits/hash_bytes.h \
 /usr/include/c++/5/bits/basic_string.tcc \
 /usr/include/c++/5/bits/locale_classes.tcc \
 /usr/include/c++/5/system_error \
 /usr/include/x86_64-linux-gnu/c++/5/bits/error_constants.h \
 /usr/include/c++/5/stdexcept /usr/include/c++/5/streambuf \
 /usr/include/c++/5/bits/streambuf.tcc \
 /usr/include/c++/5/bits/basic_ios.h \
 /usr/include/c++/5/bits/locale_facets.h /usr/include/c++/5/cwctype \
 /usr/include/wctype.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_base.h \
 /usr/include/c++/5/bits/streambuf_iterator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_inline.h \
 /usr/include/c++/5/bits/locale_facets.tcc \
 /usr/include/c++/5/bits/basic_ios.tcc /usr/include/c++/5/ostream \
 /usr/include/c++/5/bits/ostream.tcc /usr/include/c++/5/bits/istream.tcc \
 /usr/include/c++/5/bits/sstream.tcc /usr/include/c++/5/cmath \
 /usr/include/math.h /usr/include/x86_64-linux-gnu/bits/math-vector.h \
 /usr/include/x86_64-linux-gnu/bits/libm-simd-decl-stubs.h \
 /usr/include/x86_64-linux-gnu/bits/huge_val.h \
 /usr/include/x86_64-linux-gnu/bits/huge_valf.h \
 /usr/include/x86_64-linux-gnu/bits/huge_vall.h \
 /usr/include/x86_64-linux-gnu/bits/inf.h \
 /usr/include/x86_64-linux-gnu/bits/nan.h \
 /usr/include/x86_64-linux-gnu/bits/mathdef.h \
 /usr/include/x86_64-linux-gnu/bits/mathcalls.h graphics/viewport.h \
 graphics/../drawable.h  /usr/include/c++/5/iostream \
 graphics/../math/vector2f.h graphics/../graphics/components/image.h \
 graphics/../data/gameData.h /usr/include/c++/5/map \
 /usr/include/c++/5/bits/stl_tree.h \
 /usr/include/c++/5/ext/aligned_buffer.h \
 /usr/include/c++/5/bits/stl_map.h /usr/include/c++/5/tuple \
 /usr/include/c++/5/utility /usr/include/c++/5/bits/stl_relops.h \
 /usr/include/c++/5/array /usr/include/c++/5/bits/uses_allocator.h \
 /usr/include/c++/5/bits/stl_multimap.h \
 graphics/../data/../math/vector2f.h graphics/../data/parseXML.h \
 /usr/include/c++/5/fstream /usr/include/c++/5/bits/codecvt.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/basic_file.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++io.h \
 /usr/include/c++/5/bits/fstream.tcc /usr/include/c++/5/deque \
 /usr/include/c++/5/bits/stl_construct.h \
 /usr/include/c++/5/bits/stl_uninitialized.h \
 /usr/include/c++/5/bits/stl_deque.h /usr/include/c++/5/bits/deque.tcc \
 /usr/include/expat.h /usr/include/expat_external.h \
 graphics/../data/gameData.h graphics/components/hud.h \
 graphics/components/../ioMod.h  \
 graphics/components/../../engine/clock.h \
 graphics/components/../../data/gameData.h graphics/components/menu.h \
 graphics/../enums/state.h graphics/components/image.h graphics/ioMod.h \
 graphics/../engine/clock.h graphics/imageFactory.h \
 /usr/include/c++/5/vector /usr/include/c++/5/bits/stl_vector.h \
 /usr/include/c++/5/bits/stl_bvector.h /usr/include/c++/5/bits/vector.tcc \
 graphics/../sprites/spriteSheet.h  
	$(CC) -c $< -o $@ $(CFLAGS)
$(OBJDIR)main.o: main.cpp /usr/include/stdc-predef.h engine/engine.h \
 /usr/include/c++/5/vector /usr/include/c++/5/bits/stl_algobase.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/os_defines.h \
 /usr/include/features.h /usr/include/x86_64-linux-gnu/sys/cdefs.h \
 /usr/include/x86_64-linux-gnu/bits/wordsize.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs-64.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/cpu_defines.h \
 /usr/include/c++/5/bits/functexcept.h \
 /usr/include/c++/5/bits/exception_defines.h \
 /usr/include/c++/5/bits/cpp_type_traits.h \
 /usr/include/c++/5/ext/type_traits.h \
 /usr/include/c++/5/ext/numeric_traits.h \
 /usr/include/c++/5/bits/stl_pair.h /usr/include/c++/5/bits/move.h \
 /usr/include/c++/5/bits/concept_check.h /usr/include/c++/5/type_traits \
 /usr/include/c++/5/bits/stl_iterator_base_types.h \
 /usr/include/c++/5/bits/stl_iterator_base_funcs.h \
 /usr/include/c++/5/debug/debug.h /usr/include/c++/5/bits/stl_iterator.h \
 /usr/include/c++/5/bits/ptr_traits.h \
 /usr/include/c++/5/bits/predefined_ops.h \
 /usr/include/c++/5/bits/allocator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++allocator.h \
 /usr/include/c++/5/ext/new_allocator.h /usr/include/c++/5/new \
 /usr/include/c++/5/exception \
 /usr/include/c++/5/bits/atomic_lockfree_defines.h \
 /usr/include/c++/5/bits/exception_ptr.h \
 /usr/include/c++/5/bits/nested_exception.h \
 /usr/include/c++/5/bits/memoryfwd.h \
 /usr/include/c++/5/bits/stl_construct.h \
 /usr/include/c++/5/ext/alloc_traits.h \
 /usr/include/c++/5/bits/alloc_traits.h \
 /usr/include/c++/5/bits/stl_uninitialized.h \
 /usr/include/c++/5/bits/stl_vector.h /usr/include/c++/5/initializer_list \
 /usr/include/c++/5/bits/stl_bvector.h \
 /usr/include/c++/5/bits/functional_hash.h \
 /usr/include/c++/5/bits/hash_bytes.h \
 /usr/include/c++/5/bits/range_access.h \
 /usr/include/c++/5/bits/vector.tcc  engine/../graphics/ioMod.h \
 /usr/include/c++/5/iostream /usr/include/c++/5/ostream \
 /usr/include/c++/5/ios /usr/include/c++/5/iosfwd \
 /usr/include/c++/5/bits/stringfwd.h /usr/include/c++/5/bits/postypes.h \
 /usr/include/c++/5/cwchar /usr/include/wchar.h /usr/include/stdio.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdarg.h \
 /usr/include/x86_64-linux-gnu/bits/wchar.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h /usr/include/xlocale.h \
 /usr/include/c++/5/bits/char_traits.h /usr/include/c++/5/cstdint \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdint.h /usr/include/stdint.h \
 /usr/include/c++/5/bits/localefwd.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++locale.h \
 /usr/include/c++/5/clocale /usr/include/locale.h \
 /usr/include/x86_64-linux-gnu/bits/locale.h /usr/include/c++/5/cctype \
 /usr/include/ctype.h /usr/include/x86_64-linux-gnu/bits/types.h \
 /usr/include/x86_64-linux-gnu/bits/typesizes.h /usr/include/endian.h \
 /usr/include/x86_64-linux-gnu/bits/endian.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap-16.h \
 /usr/include/c++/5/bits/ios_base.h /usr/include/c++/5/ext/atomicity.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr-default.h \
 /usr/include/pthread.h /usr/include/sched.h /usr/include/time.h \
 /usr/include/x86_64-linux-gnu/bits/sched.h \
 /usr/include/x86_64-linux-gnu/bits/time.h \
 /usr/include/x86_64-linux-gnu/bits/timex.h \
 /usr/include/x86_64-linux-gnu/bits/pthreadtypes.h \
 /usr/include/x86_64-linux-gnu/bits/setjmp.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h \
 /usr/include/c++/5/bits/locale_classes.h /usr/include/c++/5/string \
 /usr/include/c++/5/bits/ostream_insert.h \
 /usr/include/c++/5/bits/cxxabi_forced.h \
 /usr/include/c++/5/bits/stl_function.h \
 /usr/include/c++/5/backward/binders.h \
 /usr/include/c++/5/bits/basic_string.h \
 /usr/include/c++/5/ext/string_conversions.h /usr/include/c++/5/cstdlib \
 /usr/include/stdlib.h /usr/include/x86_64-linux-gnu/bits/waitflags.h \
 /usr/include/x86_64-linux-gnu/bits/waitstatus.h \
 /usr/include/x86_64-linux-gnu/sys/types.h \
 /usr/include/x86_64-linux-gnu/sys/select.h \
 /usr/include/x86_64-linux-gnu/bits/select.h \
 /usr/include/x86_64-linux-gnu/bits/sigset.h \
 /usr/include/x86_64-linux-gnu/sys/sysmacros.h /usr/include/alloca.h \
 /usr/include/x86_64-linux-gnu/bits/stdlib-float.h \
 /usr/include/c++/5/cstdio /usr/include/libio.h /usr/include/_G_config.h \
 /usr/include/x86_64-linux-gnu/bits/stdio_lim.h \
 /usr/include/x86_64-linux-gnu/bits/sys_errlist.h \
 /usr/include/c++/5/cerrno /usr/include/errno.h \
 /usr/include/x86_64-linux-gnu/bits/errno.h /usr/include/linux/errno.h \
 /usr/include/x86_64-linux-gnu/asm/errno.h \
 /usr/include/asm-generic/errno.h /usr/include/asm-generic/errno-base.h \
 /usr/include/c++/5/bits/basic_string.tcc \
 /usr/include/c++/5/bits/locale_classes.tcc \
 /usr/include/c++/5/system_error \
 /usr/include/x86_64-linux-gnu/c++/5/bits/error_constants.h \
 /usr/include/c++/5/stdexcept /usr/include/c++/5/streambuf \
 /usr/include/c++/5/bits/streambuf.tcc \
 /usr/include/c++/5/bits/basic_ios.h \
 /usr/include/c++/5/bits/locale_facets.h /usr/include/c++/5/cwctype \
 /usr/include/wctype.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_base.h \
 /usr/include/c++/5/bits/streambuf_iterator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_inline.h \
 /usr/include/c++/5/bits/locale_facets.tcc \
 /usr/include/c++/5/bits/basic_ios.tcc \
 /usr/include/c++/5/bits/ostream.tcc /usr/include/c++/5/istream \
 /usr/include/c++/5/bits/istream.tcc  \
 engine/../graphics/renderContext.h engine/../graphics/imageFactory.h \
 /usr/include/c++/5/map /usr/include/c++/5/bits/stl_tree.h \
 /usr/include/c++/5/ext/aligned_buffer.h \
 /usr/include/c++/5/bits/stl_map.h /usr/include/c++/5/tuple \
 /usr/include/c++/5/utility /usr/include/c++/5/bits/stl_relops.h \
 /usr/include/c++/5/array /usr/include/c++/5/bits/uses_allocator.h \
 /usr/include/c++/5/bits/stl_multimap.h \
 engine/../graphics/components/image.h \
 engine/../graphics/../data/gameData.h \
 engine/../graphics/../data/../math/vector2f.h \
 engine/../graphics/../data/parseXML.h /usr/include/c++/5/fstream \
 /usr/include/c++/5/bits/codecvt.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/basic_file.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++io.h \
 /usr/include/c++/5/bits/fstream.tcc /usr/include/c++/5/deque \
 /usr/include/c++/5/bits/stl_deque.h /usr/include/c++/5/bits/deque.tcc \
 /usr/include/expat.h /usr/include/expat_external.h \
 engine/../graphics/../sprites/spriteSheet.h   \
 engine/clock.h engine/../graphics/components/world.h \
 engine/../graphics/components/image.h \
 engine/../graphics/components/../viewport.h \
 engine/../graphics/components/../../drawable.h \
 engine/../graphics/components/../../math/vector2f.h \
 engine/../graphics/components/../../graphics/components/image.h \
 engine/../graphics/components/../../data/gameData.h \
 engine/../graphics/components/../../data/gameData.h \
 engine/../graphics/components/../components/hud.h \
 /usr/include/c++/5/sstream /usr/include/c++/5/bits/sstream.tcc \
 engine/../graphics/components/../components/../ioMod.h \
 engine/../graphics/components/../components/../../engine/clock.h \
 engine/../graphics/components/../components/../../data/gameData.h \
 engine/../graphics/components/../components/menu.h \
 engine/../graphics/components/../../enums/state.h \
 engine/../graphics/components/../components/image.h \
 engine/../graphics/viewport.h engine/../sprites/enemy.h \
 /usr/include/c++/5/functional /usr/include/c++/5/typeinfo \
 engine/../sprites/bidirectionalMultisprite.h \
 engine/../sprites/multisprite.h /usr/include/c++/5/cmath \
 /usr/include/math.h /usr/include/x86_64-linux-gnu/bits/math-vector.h \
 /usr/include/x86_64-linux-gnu/bits/libm-simd-decl-stubs.h \
 /usr/include/x86_64-linux-gnu/bits/huge_val.h \
 /usr/include/x86_64-linux-gnu/bits/huge_valf.h \
 /usr/include/x86_64-linux-gnu/bits/huge_vall.h \
 /usr/include/x86_64-linux-gnu/bits/inf.h \
 /usr/include/x86_64-linux-gnu/bits/nan.h \
 /usr/include/x86_64-linux-gnu/bits/mathdef.h \
 /usr/include/x86_64-linux-gnu/bits/mathcalls.h \
 engine/../sprites/sprite.h engine/../sprites/../drawable.h \
 engine/../sprites/../data/gameData.h \
 engine/../sprites/../math/vector2f.h \
 engine/../sprites/../observable/observable.h \
 engine/../sprites/walkingSprite.h engine/../sprites/explodingSprite.h \
 /usr/include/c++/5/list /usr/include/c++/5/bits/stl_list.h \
 /usr/include/c++/5/bits/list.tcc engine/../sprites/chunk.h \
 engine/../sprites/../graphics/components/image.h \
 engine/../sprites/player.h \
 engine/../sprites/../managers/projectilePool.h \
 engine/../sprites/../managers/../sprites/projectileSprite.h \
 engine/../sprites/../managers/../sprites/sprite.h \
 engine/../sprites/../managers/../math/vector2f.h \
 engine/../sprites/../managers/trapPool.h \
 engine/../sprites/../managers/../sprites/multisprite.h \
 engine/../observable/observable.h engine/../math/vector2f.h \
 engine/perPixelCollisionStrategy.h engine/collisionStrategy.h \
 engine/../drawable.h engine/../enums/state.h
	$(CC) -c $< -o $@ $(CFLAGS)
$(OBJDIR)projectilePool.o: managers/projectilePool.cpp /usr/include/stdc-predef.h \
 /usr/include/c++/5/cmath \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/os_defines.h \
 /usr/include/features.h /usr/include/x86_64-linux-gnu/sys/cdefs.h \
 /usr/include/x86_64-linux-gnu/bits/wordsize.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs-64.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/cpu_defines.h \
 /usr/include/c++/5/bits/cpp_type_traits.h \
 /usr/include/c++/5/ext/type_traits.h /usr/include/math.h \
 /usr/include/x86_64-linux-gnu/bits/math-vector.h \
 /usr/include/x86_64-linux-gnu/bits/libm-simd-decl-stubs.h \
 /usr/include/x86_64-linux-gnu/bits/huge_val.h \
 /usr/include/x86_64-linux-gnu/bits/huge_valf.h \
 /usr/include/x86_64-linux-gnu/bits/huge_vall.h \
 /usr/include/x86_64-linux-gnu/bits/inf.h \
 /usr/include/x86_64-linux-gnu/bits/nan.h \
 /usr/include/x86_64-linux-gnu/bits/mathdef.h \
 /usr/include/x86_64-linux-gnu/bits/mathcalls.h managers/projectilePool.h \
 /usr/include/c++/5/string /usr/include/c++/5/bits/stringfwd.h \
 /usr/include/c++/5/bits/memoryfwd.h \
 /usr/include/c++/5/bits/char_traits.h \
 /usr/include/c++/5/bits/stl_algobase.h \
 /usr/include/c++/5/bits/functexcept.h \
 /usr/include/c++/5/bits/exception_defines.h \
 /usr/include/c++/5/ext/numeric_traits.h \
 /usr/include/c++/5/bits/stl_pair.h /usr/include/c++/5/bits/move.h \
 /usr/include/c++/5/bits/concept_check.h /usr/include/c++/5/type_traits \
 /usr/include/c++/5/bits/stl_iterator_base_types.h \
 /usr/include/c++/5/bits/stl_iterator_base_funcs.h \
 /usr/include/c++/5/debug/debug.h /usr/include/c++/5/bits/stl_iterator.h \
 /usr/include/c++/5/bits/ptr_traits.h \
 /usr/include/c++/5/bits/predefined_ops.h \
 /usr/include/c++/5/bits/postypes.h /usr/include/c++/5/cwchar \
 /usr/include/wchar.h /usr/include/stdio.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdarg.h \
 /usr/include/x86_64-linux-gnu/bits/wchar.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h /usr/include/xlocale.h \
 /usr/include/c++/5/cstdint \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdint.h /usr/include/stdint.h \
 /usr/include/c++/5/bits/allocator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++allocator.h \
 /usr/include/c++/5/ext/new_allocator.h /usr/include/c++/5/new \
 /usr/include/c++/5/exception \
 /usr/include/c++/5/bits/atomic_lockfree_defines.h \
 /usr/include/c++/5/bits/exception_ptr.h \
 /usr/include/c++/5/bits/nested_exception.h \
 /usr/include/c++/5/bits/localefwd.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++locale.h \
 /usr/include/c++/5/clocale /usr/include/locale.h \
 /usr/include/x86_64-linux-gnu/bits/locale.h /usr/include/c++/5/iosfwd \
 /usr/include/c++/5/cctype /usr/include/ctype.h \
 /usr/include/x86_64-linux-gnu/bits/types.h \
 /usr/include/x86_64-linux-gnu/bits/typesizes.h /usr/include/endian.h \
 /usr/include/x86_64-linux-gnu/bits/endian.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap-16.h \
 /usr/include/c++/5/bits/ostream_insert.h \
 /usr/include/c++/5/bits/cxxabi_forced.h \
 /usr/include/c++/5/bits/stl_function.h \
 /usr/include/c++/5/backward/binders.h \
 /usr/include/c++/5/bits/range_access.h \
 /usr/include/c++/5/initializer_list \
 /usr/include/c++/5/bits/basic_string.h \
 /usr/include/c++/5/ext/atomicity.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr-default.h \
 /usr/include/pthread.h /usr/include/sched.h /usr/include/time.h \
 /usr/include/x86_64-linux-gnu/bits/sched.h \
 /usr/include/x86_64-linux-gnu/bits/time.h \
 /usr/include/x86_64-linux-gnu/bits/timex.h \
 /usr/include/x86_64-linux-gnu/bits/pthreadtypes.h \
 /usr/include/x86_64-linux-gnu/bits/setjmp.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h \
 /usr/include/c++/5/ext/alloc_traits.h \
 /usr/include/c++/5/bits/alloc_traits.h \
 /usr/include/c++/5/ext/string_conversions.h /usr/include/c++/5/cstdlib \
 /usr/include/stdlib.h /usr/include/x86_64-linux-gnu/bits/waitflags.h \
 /usr/include/x86_64-linux-gnu/bits/waitstatus.h \
 /usr/include/x86_64-linux-gnu/sys/types.h \
 /usr/include/x86_64-linux-gnu/sys/select.h \
 /usr/include/x86_64-linux-gnu/bits/select.h \
 /usr/include/x86_64-linux-gnu/bits/sigset.h \
 /usr/include/x86_64-linux-gnu/sys/sysmacros.h /usr/include/alloca.h \
 /usr/include/x86_64-linux-gnu/bits/stdlib-float.h \
 /usr/include/c++/5/cstdio /usr/include/libio.h /usr/include/_G_config.h \
 /usr/include/x86_64-linux-gnu/bits/stdio_lim.h \
 /usr/include/x86_64-linux-gnu/bits/sys_errlist.h \
 /usr/include/c++/5/cerrno /usr/include/errno.h \
 /usr/include/x86_64-linux-gnu/bits/errno.h /usr/include/linux/errno.h \
 /usr/include/x86_64-linux-gnu/asm/errno.h \
 /usr/include/asm-generic/errno.h /usr/include/asm-generic/errno-base.h \
 /usr/include/c++/5/bits/functional_hash.h \
 /usr/include/c++/5/bits/hash_bytes.h \
 /usr/include/c++/5/bits/basic_string.tcc /usr/include/c++/5/list \
 /usr/include/c++/5/bits/stl_list.h /usr/include/c++/5/bits/list.tcc \
 managers/../sprites/projectileSprite.h managers/../sprites/sprite.h \
 managers/../sprites/../drawable.h  /usr/include/c++/5/iostream \
 /usr/include/c++/5/ostream /usr/include/c++/5/ios \
 /usr/include/c++/5/bits/ios_base.h \
 /usr/include/c++/5/bits/locale_classes.h \
 /usr/include/c++/5/bits/locale_classes.tcc \
 /usr/include/c++/5/system_error \
 /usr/include/x86_64-linux-gnu/c++/5/bits/error_constants.h \
 /usr/include/c++/5/stdexcept /usr/include/c++/5/streambuf \
 /usr/include/c++/5/bits/streambuf.tcc \
 /usr/include/c++/5/bits/basic_ios.h \
 /usr/include/c++/5/bits/locale_facets.h /usr/include/c++/5/cwctype \
 /usr/include/wctype.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_base.h \
 /usr/include/c++/5/bits/streambuf_iterator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_inline.h \
 /usr/include/c++/5/bits/locale_facets.tcc \
 /usr/include/c++/5/bits/basic_ios.tcc \
 /usr/include/c++/5/bits/ostream.tcc /usr/include/c++/5/istream \
 /usr/include/c++/5/bits/istream.tcc \
 managers/../sprites/../math/vector2f.h \
 managers/../sprites/../graphics/components/image.h \
 managers/../sprites/../data/gameData.h /usr/include/c++/5/map \
 /usr/include/c++/5/bits/stl_tree.h \
 /usr/include/c++/5/ext/aligned_buffer.h \
 /usr/include/c++/5/bits/stl_map.h /usr/include/c++/5/tuple \
 /usr/include/c++/5/utility /usr/include/c++/5/bits/stl_relops.h \
 /usr/include/c++/5/array /usr/include/c++/5/bits/uses_allocator.h \
 /usr/include/c++/5/bits/stl_multimap.h \
 managers/../sprites/../data/../math/vector2f.h \
 managers/../sprites/../data/parseXML.h /usr/include/c++/5/fstream \
 /usr/include/c++/5/bits/codecvt.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/basic_file.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++io.h \
 /usr/include/c++/5/bits/fstream.tcc /usr/include/c++/5/deque \
 /usr/include/c++/5/bits/stl_construct.h \
 /usr/include/c++/5/bits/stl_uninitialized.h \
 /usr/include/c++/5/bits/stl_deque.h /usr/include/c++/5/bits/deque.tcc \
 /usr/include/expat.h /usr/include/expat_external.h \
 managers/../sprites/../data/gameData.h \
 managers/../sprites/../math/vector2f.h managers/../math/vector2f.h
	$(CC) -c $< -o $@ $(CFLAGS)
$(OBJDIR)trapPool.o: managers/trapPool.cpp /usr/include/stdc-predef.h \
 /usr/include/c++/5/cmath \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/os_defines.h \
 /usr/include/features.h /usr/include/x86_64-linux-gnu/sys/cdefs.h \
 /usr/include/x86_64-linux-gnu/bits/wordsize.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs-64.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/cpu_defines.h \
 /usr/include/c++/5/bits/cpp_type_traits.h \
 /usr/include/c++/5/ext/type_traits.h /usr/include/math.h \
 /usr/include/x86_64-linux-gnu/bits/math-vector.h \
 /usr/include/x86_64-linux-gnu/bits/libm-simd-decl-stubs.h \
 /usr/include/x86_64-linux-gnu/bits/huge_val.h \
 /usr/include/x86_64-linux-gnu/bits/huge_valf.h \
 /usr/include/x86_64-linux-gnu/bits/huge_vall.h \
 /usr/include/x86_64-linux-gnu/bits/inf.h \
 /usr/include/x86_64-linux-gnu/bits/nan.h \
 /usr/include/x86_64-linux-gnu/bits/mathdef.h \
 /usr/include/x86_64-linux-gnu/bits/mathcalls.h managers/trapPool.h \
 /usr/include/c++/5/string /usr/include/c++/5/bits/stringfwd.h \
 /usr/include/c++/5/bits/memoryfwd.h \
 /usr/include/c++/5/bits/char_traits.h \
 /usr/include/c++/5/bits/stl_algobase.h \
 /usr/include/c++/5/bits/functexcept.h \
 /usr/include/c++/5/bits/exception_defines.h \
 /usr/include/c++/5/ext/numeric_traits.h \
 /usr/include/c++/5/bits/stl_pair.h /usr/include/c++/5/bits/move.h \
 /usr/include/c++/5/bits/concept_check.h /usr/include/c++/5/type_traits \
 /usr/include/c++/5/bits/stl_iterator_base_types.h \
 /usr/include/c++/5/bits/stl_iterator_base_funcs.h \
 /usr/include/c++/5/debug/debug.h /usr/include/c++/5/bits/stl_iterator.h \
 /usr/include/c++/5/bits/ptr_traits.h \
 /usr/include/c++/5/bits/predefined_ops.h \
 /usr/include/c++/5/bits/postypes.h /usr/include/c++/5/cwchar \
 /usr/include/wchar.h /usr/include/stdio.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdarg.h \
 /usr/include/x86_64-linux-gnu/bits/wchar.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h /usr/include/xlocale.h \
 /usr/include/c++/5/cstdint \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdint.h /usr/include/stdint.h \
 /usr/include/c++/5/bits/allocator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++allocator.h \
 /usr/include/c++/5/ext/new_allocator.h /usr/include/c++/5/new \
 /usr/include/c++/5/exception \
 /usr/include/c++/5/bits/atomic_lockfree_defines.h \
 /usr/include/c++/5/bits/exception_ptr.h \
 /usr/include/c++/5/bits/nested_exception.h \
 /usr/include/c++/5/bits/localefwd.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++locale.h \
 /usr/include/c++/5/clocale /usr/include/locale.h \
 /usr/include/x86_64-linux-gnu/bits/locale.h /usr/include/c++/5/iosfwd \
 /usr/include/c++/5/cctype /usr/include/ctype.h \
 /usr/include/x86_64-linux-gnu/bits/types.h \
 /usr/include/x86_64-linux-gnu/bits/typesizes.h /usr/include/endian.h \
 /usr/include/x86_64-linux-gnu/bits/endian.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap-16.h \
 /usr/include/c++/5/bits/ostream_insert.h \
 /usr/include/c++/5/bits/cxxabi_forced.h \
 /usr/include/c++/5/bits/stl_function.h \
 /usr/include/c++/5/backward/binders.h \
 /usr/include/c++/5/bits/range_access.h \
 /usr/include/c++/5/initializer_list \
 /usr/include/c++/5/bits/basic_string.h \
 /usr/include/c++/5/ext/atomicity.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr-default.h \
 /usr/include/pthread.h /usr/include/sched.h /usr/include/time.h \
 /usr/include/x86_64-linux-gnu/bits/sched.h \
 /usr/include/x86_64-linux-gnu/bits/time.h \
 /usr/include/x86_64-linux-gnu/bits/timex.h \
 /usr/include/x86_64-linux-gnu/bits/pthreadtypes.h \
 /usr/include/x86_64-linux-gnu/bits/setjmp.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h \
 /usr/include/c++/5/ext/alloc_traits.h \
 /usr/include/c++/5/bits/alloc_traits.h \
 /usr/include/c++/5/ext/string_conversions.h /usr/include/c++/5/cstdlib \
 /usr/include/stdlib.h /usr/include/x86_64-linux-gnu/bits/waitflags.h \
 /usr/include/x86_64-linux-gnu/bits/waitstatus.h \
 /usr/include/x86_64-linux-gnu/sys/types.h \
 /usr/include/x86_64-linux-gnu/sys/select.h \
 /usr/include/x86_64-linux-gnu/bits/select.h \
 /usr/include/x86_64-linux-gnu/bits/sigset.h \
 /usr/include/x86_64-linux-gnu/sys/sysmacros.h /usr/include/alloca.h \
 /usr/include/x86_64-linux-gnu/bits/stdlib-float.h \
 /usr/include/c++/5/cstdio /usr/include/libio.h /usr/include/_G_config.h \
 /usr/include/x86_64-linux-gnu/bits/stdio_lim.h \
 /usr/include/x86_64-linux-gnu/bits/sys_errlist.h \
 /usr/include/c++/5/cerrno /usr/include/errno.h \
 /usr/include/x86_64-linux-gnu/bits/errno.h /usr/include/linux/errno.h \
 /usr/include/x86_64-linux-gnu/asm/errno.h \
 /usr/include/asm-generic/errno.h /usr/include/asm-generic/errno-base.h \
 /usr/include/c++/5/bits/functional_hash.h \
 /usr/include/c++/5/bits/hash_bytes.h \
 /usr/include/c++/5/bits/basic_string.tcc /usr/include/c++/5/list \
 /usr/include/c++/5/bits/stl_list.h /usr/include/c++/5/bits/list.tcc \
 managers/../sprites/multisprite.h /usr/include/c++/5/vector \
 /usr/include/c++/5/bits/stl_construct.h \
 /usr/include/c++/5/bits/stl_uninitialized.h \
 /usr/include/c++/5/bits/stl_vector.h \
 /usr/include/c++/5/bits/stl_bvector.h /usr/include/c++/5/bits/vector.tcc \
 managers/../sprites/sprite.h managers/../sprites/../drawable.h  \
 /usr/include/c++/5/iostream /usr/include/c++/5/ostream \
 /usr/include/c++/5/ios /usr/include/c++/5/bits/ios_base.h \
 /usr/include/c++/5/bits/locale_classes.h \
 /usr/include/c++/5/bits/locale_classes.tcc \
 /usr/include/c++/5/system_error \
 /usr/include/x86_64-linux-gnu/c++/5/bits/error_constants.h \
 /usr/include/c++/5/stdexcept /usr/include/c++/5/streambuf \
 /usr/include/c++/5/bits/streambuf.tcc \
 /usr/include/c++/5/bits/basic_ios.h \
 /usr/include/c++/5/bits/locale_facets.h /usr/include/c++/5/cwctype \
 /usr/include/wctype.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_base.h \
 /usr/include/c++/5/bits/streambuf_iterator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_inline.h \
 /usr/include/c++/5/bits/locale_facets.tcc \
 /usr/include/c++/5/bits/basic_ios.tcc \
 /usr/include/c++/5/bits/ostream.tcc /usr/include/c++/5/istream \
 /usr/include/c++/5/bits/istream.tcc \
 managers/../sprites/../math/vector2f.h \
 managers/../sprites/../graphics/components/image.h \
 managers/../sprites/../data/gameData.h /usr/include/c++/5/map \
 /usr/include/c++/5/bits/stl_tree.h \
 /usr/include/c++/5/ext/aligned_buffer.h \
 /usr/include/c++/5/bits/stl_map.h /usr/include/c++/5/tuple \
 /usr/include/c++/5/utility /usr/include/c++/5/bits/stl_relops.h \
 /usr/include/c++/5/array /usr/include/c++/5/bits/uses_allocator.h \
 /usr/include/c++/5/bits/stl_multimap.h \
 managers/../sprites/../data/../math/vector2f.h \
 managers/../sprites/../data/parseXML.h /usr/include/c++/5/fstream \
 /usr/include/c++/5/bits/codecvt.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/basic_file.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++io.h \
 /usr/include/c++/5/bits/fstream.tcc /usr/include/c++/5/deque \
 /usr/include/c++/5/bits/stl_deque.h /usr/include/c++/5/bits/deque.tcc \
 /usr/include/expat.h /usr/include/expat_external.h \
 managers/../sprites/../data/gameData.h \
 managers/../sprites/../math/vector2f.h managers/../math/vector2f.h
	$(CC) -c $< -o $@ $(CFLAGS)
$(OBJDIR)vector2f.o: math/vector2f.cpp /usr/include/stdc-predef.h \
 /usr/include/c++/5/cmath \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/os_defines.h \
 /usr/include/features.h /usr/include/x86_64-linux-gnu/sys/cdefs.h \
 /usr/include/x86_64-linux-gnu/bits/wordsize.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs-64.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/cpu_defines.h \
 /usr/include/c++/5/bits/cpp_type_traits.h \
 /usr/include/c++/5/ext/type_traits.h /usr/include/math.h \
 /usr/include/x86_64-linux-gnu/bits/math-vector.h \
 /usr/include/x86_64-linux-gnu/bits/libm-simd-decl-stubs.h \
 /usr/include/x86_64-linux-gnu/bits/huge_val.h \
 /usr/include/x86_64-linux-gnu/bits/huge_valf.h \
 /usr/include/x86_64-linux-gnu/bits/huge_vall.h \
 /usr/include/x86_64-linux-gnu/bits/inf.h \
 /usr/include/x86_64-linux-gnu/bits/nan.h \
 /usr/include/x86_64-linux-gnu/bits/mathdef.h \
 /usr/include/x86_64-linux-gnu/bits/mathcalls.h /usr/include/c++/5/string \
 /usr/include/c++/5/bits/stringfwd.h /usr/include/c++/5/bits/memoryfwd.h \
 /usr/include/c++/5/bits/char_traits.h \
 /usr/include/c++/5/bits/stl_algobase.h \
 /usr/include/c++/5/bits/functexcept.h \
 /usr/include/c++/5/bits/exception_defines.h \
 /usr/include/c++/5/ext/numeric_traits.h \
 /usr/include/c++/5/bits/stl_pair.h /usr/include/c++/5/bits/move.h \
 /usr/include/c++/5/bits/concept_check.h /usr/include/c++/5/type_traits \
 /usr/include/c++/5/bits/stl_iterator_base_types.h \
 /usr/include/c++/5/bits/stl_iterator_base_funcs.h \
 /usr/include/c++/5/debug/debug.h /usr/include/c++/5/bits/stl_iterator.h \
 /usr/include/c++/5/bits/ptr_traits.h \
 /usr/include/c++/5/bits/predefined_ops.h \
 /usr/include/c++/5/bits/postypes.h /usr/include/c++/5/cwchar \
 /usr/include/wchar.h /usr/include/stdio.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdarg.h \
 /usr/include/x86_64-linux-gnu/bits/wchar.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h /usr/include/xlocale.h \
 /usr/include/c++/5/cstdint \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdint.h /usr/include/stdint.h \
 /usr/include/c++/5/bits/allocator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++allocator.h \
 /usr/include/c++/5/ext/new_allocator.h /usr/include/c++/5/new \
 /usr/include/c++/5/exception \
 /usr/include/c++/5/bits/atomic_lockfree_defines.h \
 /usr/include/c++/5/bits/exception_ptr.h \
 /usr/include/c++/5/bits/nested_exception.h \
 /usr/include/c++/5/bits/localefwd.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++locale.h \
 /usr/include/c++/5/clocale /usr/include/locale.h \
 /usr/include/x86_64-linux-gnu/bits/locale.h /usr/include/c++/5/iosfwd \
 /usr/include/c++/5/cctype /usr/include/ctype.h \
 /usr/include/x86_64-linux-gnu/bits/types.h \
 /usr/include/x86_64-linux-gnu/bits/typesizes.h /usr/include/endian.h \
 /usr/include/x86_64-linux-gnu/bits/endian.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap-16.h \
 /usr/include/c++/5/bits/ostream_insert.h \
 /usr/include/c++/5/bits/cxxabi_forced.h \
 /usr/include/c++/5/bits/stl_function.h \
 /usr/include/c++/5/backward/binders.h \
 /usr/include/c++/5/bits/range_access.h \
 /usr/include/c++/5/initializer_list \
 /usr/include/c++/5/bits/basic_string.h \
 /usr/include/c++/5/ext/atomicity.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr-default.h \
 /usr/include/pthread.h /usr/include/sched.h /usr/include/time.h \
 /usr/include/x86_64-linux-gnu/bits/sched.h \
 /usr/include/x86_64-linux-gnu/bits/time.h \
 /usr/include/x86_64-linux-gnu/bits/timex.h \
 /usr/include/x86_64-linux-gnu/bits/pthreadtypes.h \
 /usr/include/x86_64-linux-gnu/bits/setjmp.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h \
 /usr/include/c++/5/ext/alloc_traits.h \
 /usr/include/c++/5/bits/alloc_traits.h \
 /usr/include/c++/5/ext/string_conversions.h /usr/include/c++/5/cstdlib \
 /usr/include/stdlib.h /usr/include/x86_64-linux-gnu/bits/waitflags.h \
 /usr/include/x86_64-linux-gnu/bits/waitstatus.h \
 /usr/include/x86_64-linux-gnu/sys/types.h \
 /usr/include/x86_64-linux-gnu/sys/select.h \
 /usr/include/x86_64-linux-gnu/bits/select.h \
 /usr/include/x86_64-linux-gnu/bits/sigset.h \
 /usr/include/x86_64-linux-gnu/sys/sysmacros.h /usr/include/alloca.h \
 /usr/include/x86_64-linux-gnu/bits/stdlib-float.h \
 /usr/include/c++/5/cstdio /usr/include/libio.h /usr/include/_G_config.h \
 /usr/include/x86_64-linux-gnu/bits/stdio_lim.h \
 /usr/include/x86_64-linux-gnu/bits/sys_errlist.h \
 /usr/include/c++/5/cerrno /usr/include/errno.h \
 /usr/include/x86_64-linux-gnu/bits/errno.h /usr/include/linux/errno.h \
 /usr/include/x86_64-linux-gnu/asm/errno.h \
 /usr/include/asm-generic/errno.h /usr/include/asm-generic/errno-base.h \
 /usr/include/c++/5/bits/functional_hash.h \
 /usr/include/c++/5/bits/hash_bytes.h \
 /usr/include/c++/5/bits/basic_string.tcc /usr/include/c++/5/iostream \
 /usr/include/c++/5/ostream /usr/include/c++/5/ios \
 /usr/include/c++/5/bits/ios_base.h \
 /usr/include/c++/5/bits/locale_classes.h \
 /usr/include/c++/5/bits/locale_classes.tcc \
 /usr/include/c++/5/system_error \
 /usr/include/x86_64-linux-gnu/c++/5/bits/error_constants.h \
 /usr/include/c++/5/stdexcept /usr/include/c++/5/streambuf \
 /usr/include/c++/5/bits/streambuf.tcc \
 /usr/include/c++/5/bits/basic_ios.h \
 /usr/include/c++/5/bits/locale_facets.h /usr/include/c++/5/cwctype \
 /usr/include/wctype.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_base.h \
 /usr/include/c++/5/bits/streambuf_iterator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_inline.h \
 /usr/include/c++/5/bits/locale_facets.tcc \
 /usr/include/c++/5/bits/basic_ios.tcc \
 /usr/include/c++/5/bits/ostream.tcc /usr/include/c++/5/istream \
 /usr/include/c++/5/bits/istream.tcc math/vector2f.h
	$(CC) -c $< -o $@ $(CFLAGS)
$(OBJDIR)observable.o: observable/observable.cpp /usr/include/stdc-predef.h \
 observable/observable.h /usr/include/c++/5/functional \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/os_defines.h \
 /usr/include/features.h /usr/include/x86_64-linux-gnu/sys/cdefs.h \
 /usr/include/x86_64-linux-gnu/bits/wordsize.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs-64.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/cpu_defines.h \
 /usr/include/c++/5/bits/stl_function.h \
 /usr/include/c++/5/backward/binders.h /usr/include/c++/5/typeinfo \
 /usr/include/c++/5/exception \
 /usr/include/c++/5/bits/atomic_lockfree_defines.h \
 /usr/include/c++/5/bits/exception_ptr.h \
 /usr/include/c++/5/bits/exception_defines.h \
 /usr/include/c++/5/bits/nested_exception.h \
 /usr/include/c++/5/bits/hash_bytes.h /usr/include/c++/5/new \
 /usr/include/c++/5/tuple /usr/include/c++/5/utility \
 /usr/include/c++/5/bits/stl_relops.h /usr/include/c++/5/bits/stl_pair.h \
 /usr/include/c++/5/bits/move.h /usr/include/c++/5/bits/concept_check.h \
 /usr/include/c++/5/type_traits /usr/include/c++/5/initializer_list \
 /usr/include/c++/5/array /usr/include/c++/5/stdexcept \
 /usr/include/c++/5/string /usr/include/c++/5/bits/stringfwd.h \
 /usr/include/c++/5/bits/memoryfwd.h \
 /usr/include/c++/5/bits/char_traits.h \
 /usr/include/c++/5/bits/stl_algobase.h \
 /usr/include/c++/5/bits/functexcept.h \
 /usr/include/c++/5/bits/cpp_type_traits.h \
 /usr/include/c++/5/ext/type_traits.h \
 /usr/include/c++/5/ext/numeric_traits.h \
 /usr/include/c++/5/bits/stl_iterator_base_types.h \
 /usr/include/c++/5/bits/stl_iterator_base_funcs.h \
 /usr/include/c++/5/debug/debug.h /usr/include/c++/5/bits/stl_iterator.h \
 /usr/include/c++/5/bits/ptr_traits.h \
 /usr/include/c++/5/bits/predefined_ops.h \
 /usr/include/c++/5/bits/postypes.h /usr/include/c++/5/cwchar \
 /usr/include/wchar.h /usr/include/stdio.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdarg.h \
 /usr/include/x86_64-linux-gnu/bits/wchar.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h /usr/include/xlocale.h \
 /usr/include/c++/5/cstdint \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdint.h /usr/include/stdint.h \
 /usr/include/c++/5/bits/allocator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++allocator.h \
 /usr/include/c++/5/ext/new_allocator.h \
 /usr/include/c++/5/bits/localefwd.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++locale.h \
 /usr/include/c++/5/clocale /usr/include/locale.h \
 /usr/include/x86_64-linux-gnu/bits/locale.h /usr/include/c++/5/iosfwd \
 /usr/include/c++/5/cctype /usr/include/ctype.h \
 /usr/include/x86_64-linux-gnu/bits/types.h \
 /usr/include/x86_64-linux-gnu/bits/typesizes.h /usr/include/endian.h \
 /usr/include/x86_64-linux-gnu/bits/endian.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap-16.h \
 /usr/include/c++/5/bits/ostream_insert.h \
 /usr/include/c++/5/bits/cxxabi_forced.h \
 /usr/include/c++/5/bits/range_access.h \
 /usr/include/c++/5/bits/basic_string.h \
 /usr/include/c++/5/ext/atomicity.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr-default.h \
 /usr/include/pthread.h /usr/include/sched.h /usr/include/time.h \
 /usr/include/x86_64-linux-gnu/bits/sched.h \
 /usr/include/x86_64-linux-gnu/bits/time.h \
 /usr/include/x86_64-linux-gnu/bits/timex.h \
 /usr/include/x86_64-linux-gnu/bits/pthreadtypes.h \
 /usr/include/x86_64-linux-gnu/bits/setjmp.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h \
 /usr/include/c++/5/ext/alloc_traits.h \
 /usr/include/c++/5/bits/alloc_traits.h \
 /usr/include/c++/5/ext/string_conversions.h /usr/include/c++/5/cstdlib \
 /usr/include/stdlib.h /usr/include/x86_64-linux-gnu/bits/waitflags.h \
 /usr/include/x86_64-linux-gnu/bits/waitstatus.h \
 /usr/include/x86_64-linux-gnu/sys/types.h \
 /usr/include/x86_64-linux-gnu/sys/select.h \
 /usr/include/x86_64-linux-gnu/bits/select.h \
 /usr/include/x86_64-linux-gnu/bits/sigset.h \
 /usr/include/x86_64-linux-gnu/sys/sysmacros.h /usr/include/alloca.h \
 /usr/include/x86_64-linux-gnu/bits/stdlib-float.h \
 /usr/include/c++/5/cstdio /usr/include/libio.h /usr/include/_G_config.h \
 /usr/include/x86_64-linux-gnu/bits/stdio_lim.h \
 /usr/include/x86_64-linux-gnu/bits/sys_errlist.h \
 /usr/include/c++/5/cerrno /usr/include/errno.h \
 /usr/include/x86_64-linux-gnu/bits/errno.h /usr/include/linux/errno.h \
 /usr/include/x86_64-linux-gnu/asm/errno.h \
 /usr/include/asm-generic/errno.h /usr/include/asm-generic/errno-base.h \
 /usr/include/c++/5/bits/functional_hash.h \
 /usr/include/c++/5/bits/basic_string.tcc \
 /usr/include/c++/5/bits/uses_allocator.h /usr/include/c++/5/map \
 /usr/include/c++/5/bits/stl_tree.h \
 /usr/include/c++/5/ext/aligned_buffer.h \
 /usr/include/c++/5/bits/stl_map.h /usr/include/c++/5/bits/stl_multimap.h \
 observable/observable.tpp /usr/include/c++/5/vector \
 /usr/include/c++/5/bits/stl_construct.h \
 /usr/include/c++/5/bits/stl_uninitialized.h \
 /usr/include/c++/5/bits/stl_vector.h \
 /usr/include/c++/5/bits/stl_bvector.h /usr/include/c++/5/bits/vector.tcc \
 observable/../math/vector2f.h /usr/include/c++/5/iostream \
 /usr/include/c++/5/ostream /usr/include/c++/5/ios \
 /usr/include/c++/5/bits/ios_base.h \
 /usr/include/c++/5/bits/locale_classes.h \
 /usr/include/c++/5/bits/locale_classes.tcc \
 /usr/include/c++/5/system_error \
 /usr/include/x86_64-linux-gnu/c++/5/bits/error_constants.h \
 /usr/include/c++/5/streambuf /usr/include/c++/5/bits/streambuf.tcc \
 /usr/include/c++/5/bits/basic_ios.h \
 /usr/include/c++/5/bits/locale_facets.h /usr/include/c++/5/cwctype \
 /usr/include/wctype.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_base.h \
 /usr/include/c++/5/bits/streambuf_iterator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_inline.h \
 /usr/include/c++/5/bits/locale_facets.tcc \
 /usr/include/c++/5/bits/basic_ios.tcc \
 /usr/include/c++/5/bits/ostream.tcc /usr/include/c++/5/istream \
 /usr/include/c++/5/bits/istream.tcc
	$(CC) -c $< -o $@ $(CFLAGS)
$(OBJDIR)bidirectionalMultisprite.o: sprites/bidirectionalMultisprite.cpp \
 /usr/include/stdc-predef.h sprites/sprite.h /usr/include/c++/5/string \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/os_defines.h \
 /usr/include/features.h /usr/include/x86_64-linux-gnu/sys/cdefs.h \
 /usr/include/x86_64-linux-gnu/bits/wordsize.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs-64.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/cpu_defines.h \
 /usr/include/c++/5/bits/stringfwd.h /usr/include/c++/5/bits/memoryfwd.h \
 /usr/include/c++/5/bits/char_traits.h \
 /usr/include/c++/5/bits/stl_algobase.h \
 /usr/include/c++/5/bits/functexcept.h \
 /usr/include/c++/5/bits/exception_defines.h \
 /usr/include/c++/5/bits/cpp_type_traits.h \
 /usr/include/c++/5/ext/type_traits.h \
 /usr/include/c++/5/ext/numeric_traits.h \
 /usr/include/c++/5/bits/stl_pair.h /usr/include/c++/5/bits/move.h \
 /usr/include/c++/5/bits/concept_check.h /usr/include/c++/5/type_traits \
 /usr/include/c++/5/bits/stl_iterator_base_types.h \
 /usr/include/c++/5/bits/stl_iterator_base_funcs.h \
 /usr/include/c++/5/debug/debug.h /usr/include/c++/5/bits/stl_iterator.h \
 /usr/include/c++/5/bits/ptr_traits.h \
 /usr/include/c++/5/bits/predefined_ops.h \
 /usr/include/c++/5/bits/postypes.h /usr/include/c++/5/cwchar \
 /usr/include/wchar.h /usr/include/stdio.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdarg.h \
 /usr/include/x86_64-linux-gnu/bits/wchar.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h /usr/include/xlocale.h \
 /usr/include/c++/5/cstdint \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdint.h /usr/include/stdint.h \
 /usr/include/c++/5/bits/allocator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++allocator.h \
 /usr/include/c++/5/ext/new_allocator.h /usr/include/c++/5/new \
 /usr/include/c++/5/exception \
 /usr/include/c++/5/bits/atomic_lockfree_defines.h \
 /usr/include/c++/5/bits/exception_ptr.h \
 /usr/include/c++/5/bits/nested_exception.h \
 /usr/include/c++/5/bits/localefwd.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++locale.h \
 /usr/include/c++/5/clocale /usr/include/locale.h \
 /usr/include/x86_64-linux-gnu/bits/locale.h /usr/include/c++/5/iosfwd \
 /usr/include/c++/5/cctype /usr/include/ctype.h \
 /usr/include/x86_64-linux-gnu/bits/types.h \
 /usr/include/x86_64-linux-gnu/bits/typesizes.h /usr/include/endian.h \
 /usr/include/x86_64-linux-gnu/bits/endian.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap-16.h \
 /usr/include/c++/5/bits/ostream_insert.h \
 /usr/include/c++/5/bits/cxxabi_forced.h \
 /usr/include/c++/5/bits/stl_function.h \
 /usr/include/c++/5/backward/binders.h \
 /usr/include/c++/5/bits/range_access.h \
 /usr/include/c++/5/initializer_list \
 /usr/include/c++/5/bits/basic_string.h \
 /usr/include/c++/5/ext/atomicity.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr-default.h \
 /usr/include/pthread.h /usr/include/sched.h /usr/include/time.h \
 /usr/include/x86_64-linux-gnu/bits/sched.h \
 /usr/include/x86_64-linux-gnu/bits/time.h \
 /usr/include/x86_64-linux-gnu/bits/timex.h \
 /usr/include/x86_64-linux-gnu/bits/pthreadtypes.h \
 /usr/include/x86_64-linux-gnu/bits/setjmp.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h \
 /usr/include/c++/5/ext/alloc_traits.h \
 /usr/include/c++/5/bits/alloc_traits.h \
 /usr/include/c++/5/ext/string_conversions.h /usr/include/c++/5/cstdlib \
 /usr/include/stdlib.h /usr/include/x86_64-linux-gnu/bits/waitflags.h \
 /usr/include/x86_64-linux-gnu/bits/waitstatus.h \
 /usr/include/x86_64-linux-gnu/sys/types.h \
 /usr/include/x86_64-linux-gnu/sys/select.h \
 /usr/include/x86_64-linux-gnu/bits/select.h \
 /usr/include/x86_64-linux-gnu/bits/sigset.h \
 /usr/include/x86_64-linux-gnu/sys/sysmacros.h /usr/include/alloca.h \
 /usr/include/x86_64-linux-gnu/bits/stdlib-float.h \
 /usr/include/c++/5/cstdio /usr/include/libio.h /usr/include/_G_config.h \
 /usr/include/x86_64-linux-gnu/bits/stdio_lim.h \
 /usr/include/x86_64-linux-gnu/bits/sys_errlist.h \
 /usr/include/c++/5/cerrno /usr/include/errno.h \
 /usr/include/x86_64-linux-gnu/bits/errno.h /usr/include/linux/errno.h \
 /usr/include/x86_64-linux-gnu/asm/errno.h \
 /usr/include/asm-generic/errno.h /usr/include/asm-generic/errno-base.h \
 /usr/include/c++/5/bits/functional_hash.h \
 /usr/include/c++/5/bits/hash_bytes.h \
 /usr/include/c++/5/bits/basic_string.tcc sprites/../drawable.h  \
 /usr/include/c++/5/iostream /usr/include/c++/5/ostream \
 /usr/include/c++/5/ios /usr/include/c++/5/bits/ios_base.h \
 /usr/include/c++/5/bits/locale_classes.h \
 /usr/include/c++/5/bits/locale_classes.tcc \
 /usr/include/c++/5/system_error \
 /usr/include/x86_64-linux-gnu/c++/5/bits/error_constants.h \
 /usr/include/c++/5/stdexcept /usr/include/c++/5/streambuf \
 /usr/include/c++/5/bits/streambuf.tcc \
 /usr/include/c++/5/bits/basic_ios.h \
 /usr/include/c++/5/bits/locale_facets.h /usr/include/c++/5/cwctype \
 /usr/include/wctype.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_base.h \
 /usr/include/c++/5/bits/streambuf_iterator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_inline.h \
 /usr/include/c++/5/bits/locale_facets.tcc \
 /usr/include/c++/5/bits/basic_ios.tcc \
 /usr/include/c++/5/bits/ostream.tcc /usr/include/c++/5/istream \
 /usr/include/c++/5/bits/istream.tcc sprites/../math/vector2f.h \
 sprites/../graphics/components/image.h sprites/../data/gameData.h \
 /usr/include/c++/5/map /usr/include/c++/5/bits/stl_tree.h \
 /usr/include/c++/5/ext/aligned_buffer.h \
 /usr/include/c++/5/bits/stl_map.h /usr/include/c++/5/tuple \
 /usr/include/c++/5/utility /usr/include/c++/5/bits/stl_relops.h \
 /usr/include/c++/5/array /usr/include/c++/5/bits/uses_allocator.h \
 /usr/include/c++/5/bits/stl_multimap.h \
 sprites/../data/../math/vector2f.h sprites/../data/parseXML.h \
 /usr/include/c++/5/fstream /usr/include/c++/5/bits/codecvt.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/basic_file.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++io.h \
 /usr/include/c++/5/bits/fstream.tcc /usr/include/c++/5/deque \
 /usr/include/c++/5/bits/stl_construct.h \
 /usr/include/c++/5/bits/stl_uninitialized.h \
 /usr/include/c++/5/bits/stl_deque.h /usr/include/c++/5/bits/deque.tcc \
 /usr/include/expat.h /usr/include/expat_external.h \
 sprites/../data/gameData.h sprites/../math/vector2f.h \
 sprites/bidirectionalMultisprite.h /usr/include/c++/5/vector \
 /usr/include/c++/5/bits/stl_vector.h \
 /usr/include/c++/5/bits/stl_bvector.h /usr/include/c++/5/bits/vector.tcc \
 sprites/multisprite.h /usr/include/c++/5/cmath /usr/include/math.h \
 /usr/include/x86_64-linux-gnu/bits/math-vector.h \
 /usr/include/x86_64-linux-gnu/bits/libm-simd-decl-stubs.h \
 /usr/include/x86_64-linux-gnu/bits/huge_val.h \
 /usr/include/x86_64-linux-gnu/bits/huge_valf.h \
 /usr/include/x86_64-linux-gnu/bits/huge_vall.h \
 /usr/include/x86_64-linux-gnu/bits/inf.h \
 /usr/include/x86_64-linux-gnu/bits/nan.h \
 /usr/include/x86_64-linux-gnu/bits/mathdef.h \
 /usr/include/x86_64-linux-gnu/bits/mathcalls.h \
 sprites/../graphics/imageFactory.h \
 sprites/../graphics/components/image.h \
 sprites/../graphics/../data/gameData.h \
 sprites/../graphics/../sprites/spriteSheet.h  
	$(CC) -c $< -o $@ $(CFLAGS)
$(OBJDIR)chunk.o: sprites/chunk.cpp /usr/include/stdc-predef.h \
 /usr/include/c++/5/iostream \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/os_defines.h \
 /usr/include/features.h /usr/include/x86_64-linux-gnu/sys/cdefs.h \
 /usr/include/x86_64-linux-gnu/bits/wordsize.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs-64.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/cpu_defines.h \
 /usr/include/c++/5/ostream /usr/include/c++/5/ios \
 /usr/include/c++/5/iosfwd /usr/include/c++/5/bits/stringfwd.h \
 /usr/include/c++/5/bits/memoryfwd.h /usr/include/c++/5/bits/postypes.h \
 /usr/include/c++/5/cwchar /usr/include/wchar.h /usr/include/stdio.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdarg.h \
 /usr/include/x86_64-linux-gnu/bits/wchar.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h /usr/include/xlocale.h \
 /usr/include/c++/5/exception \
 /usr/include/c++/5/bits/atomic_lockfree_defines.h \
 /usr/include/c++/5/bits/exception_ptr.h \
 /usr/include/c++/5/bits/exception_defines.h \
 /usr/include/c++/5/bits/nested_exception.h \
 /usr/include/c++/5/bits/char_traits.h \
 /usr/include/c++/5/bits/stl_algobase.h \
 /usr/include/c++/5/bits/functexcept.h \
 /usr/include/c++/5/bits/cpp_type_traits.h \
 /usr/include/c++/5/ext/type_traits.h \
 /usr/include/c++/5/ext/numeric_traits.h \
 /usr/include/c++/5/bits/stl_pair.h /usr/include/c++/5/bits/move.h \
 /usr/include/c++/5/bits/concept_check.h /usr/include/c++/5/type_traits \
 /usr/include/c++/5/bits/stl_iterator_base_types.h \
 /usr/include/c++/5/bits/stl_iterator_base_funcs.h \
 /usr/include/c++/5/debug/debug.h /usr/include/c++/5/bits/stl_iterator.h \
 /usr/include/c++/5/bits/ptr_traits.h \
 /usr/include/c++/5/bits/predefined_ops.h /usr/include/c++/5/cstdint \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdint.h /usr/include/stdint.h \
 /usr/include/c++/5/bits/localefwd.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++locale.h \
 /usr/include/c++/5/clocale /usr/include/locale.h \
 /usr/include/x86_64-linux-gnu/bits/locale.h /usr/include/c++/5/cctype \
 /usr/include/ctype.h /usr/include/x86_64-linux-gnu/bits/types.h \
 /usr/include/x86_64-linux-gnu/bits/typesizes.h /usr/include/endian.h \
 /usr/include/x86_64-linux-gnu/bits/endian.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap-16.h \
 /usr/include/c++/5/bits/ios_base.h /usr/include/c++/5/ext/atomicity.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr-default.h \
 /usr/include/pthread.h /usr/include/sched.h /usr/include/time.h \
 /usr/include/x86_64-linux-gnu/bits/sched.h \
 /usr/include/x86_64-linux-gnu/bits/time.h \
 /usr/include/x86_64-linux-gnu/bits/timex.h \
 /usr/include/x86_64-linux-gnu/bits/pthreadtypes.h \
 /usr/include/x86_64-linux-gnu/bits/setjmp.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h \
 /usr/include/c++/5/bits/locale_classes.h /usr/include/c++/5/string \
 /usr/include/c++/5/bits/allocator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++allocator.h \
 /usr/include/c++/5/ext/new_allocator.h /usr/include/c++/5/new \
 /usr/include/c++/5/bits/ostream_insert.h \
 /usr/include/c++/5/bits/cxxabi_forced.h \
 /usr/include/c++/5/bits/stl_function.h \
 /usr/include/c++/5/backward/binders.h \
 /usr/include/c++/5/bits/range_access.h \
 /usr/include/c++/5/initializer_list \
 /usr/include/c++/5/bits/basic_string.h \
 /usr/include/c++/5/ext/alloc_traits.h \
 /usr/include/c++/5/bits/alloc_traits.h \
 /usr/include/c++/5/ext/string_conversions.h /usr/include/c++/5/cstdlib \
 /usr/include/stdlib.h /usr/include/x86_64-linux-gnu/bits/waitflags.h \
 /usr/include/x86_64-linux-gnu/bits/waitstatus.h \
 /usr/include/x86_64-linux-gnu/sys/types.h \
 /usr/include/x86_64-linux-gnu/sys/select.h \
 /usr/include/x86_64-linux-gnu/bits/select.h \
 /usr/include/x86_64-linux-gnu/bits/sigset.h \
 /usr/include/x86_64-linux-gnu/sys/sysmacros.h /usr/include/alloca.h \
 /usr/include/x86_64-linux-gnu/bits/stdlib-float.h \
 /usr/include/c++/5/cstdio /usr/include/libio.h /usr/include/_G_config.h \
 /usr/include/x86_64-linux-gnu/bits/stdio_lim.h \
 /usr/include/x86_64-linux-gnu/bits/sys_errlist.h \
 /usr/include/c++/5/cerrno /usr/include/errno.h \
 /usr/include/x86_64-linux-gnu/bits/errno.h /usr/include/linux/errno.h \
 /usr/include/x86_64-linux-gnu/asm/errno.h \
 /usr/include/asm-generic/errno.h /usr/include/asm-generic/errno-base.h \
 /usr/include/c++/5/bits/functional_hash.h \
 /usr/include/c++/5/bits/hash_bytes.h \
 /usr/include/c++/5/bits/basic_string.tcc \
 /usr/include/c++/5/bits/locale_classes.tcc \
 /usr/include/c++/5/system_error \
 /usr/include/x86_64-linux-gnu/c++/5/bits/error_constants.h \
 /usr/include/c++/5/stdexcept /usr/include/c++/5/streambuf \
 /usr/include/c++/5/bits/streambuf.tcc \
 /usr/include/c++/5/bits/basic_ios.h \
 /usr/include/c++/5/bits/locale_facets.h /usr/include/c++/5/cwctype \
 /usr/include/wctype.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_base.h \
 /usr/include/c++/5/bits/streambuf_iterator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_inline.h \
 /usr/include/c++/5/bits/locale_facets.tcc \
 /usr/include/c++/5/bits/basic_ios.tcc \
 /usr/include/c++/5/bits/ostream.tcc /usr/include/c++/5/istream \
 /usr/include/c++/5/bits/istream.tcc /usr/include/c++/5/cmath \
 /usr/include/math.h /usr/include/x86_64-linux-gnu/bits/math-vector.h \
 /usr/include/x86_64-linux-gnu/bits/libm-simd-decl-stubs.h \
 /usr/include/x86_64-linux-gnu/bits/huge_val.h \
 /usr/include/x86_64-linux-gnu/bits/huge_valf.h \
 /usr/include/x86_64-linux-gnu/bits/huge_vall.h \
 /usr/include/x86_64-linux-gnu/bits/inf.h \
 /usr/include/x86_64-linux-gnu/bits/nan.h \
 /usr/include/x86_64-linux-gnu/bits/mathdef.h \
 /usr/include/x86_64-linux-gnu/bits/mathcalls.h sprites/chunk.h \
 sprites/sprite.h sprites/../drawable.h  sprites/../math/vector2f.h \
 sprites/../graphics/components/image.h sprites/../data/gameData.h \
 /usr/include/c++/5/map /usr/include/c++/5/bits/stl_tree.h \
 /usr/include/c++/5/ext/aligned_buffer.h \
 /usr/include/c++/5/bits/stl_map.h /usr/include/c++/5/tuple \
 /usr/include/c++/5/utility /usr/include/c++/5/bits/stl_relops.h \
 /usr/include/c++/5/array /usr/include/c++/5/bits/uses_allocator.h \
 /usr/include/c++/5/bits/stl_multimap.h \
 sprites/../data/../math/vector2f.h sprites/../data/parseXML.h \
 /usr/include/c++/5/fstream /usr/include/c++/5/bits/codecvt.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/basic_file.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++io.h \
 /usr/include/c++/5/bits/fstream.tcc /usr/include/c++/5/deque \
 /usr/include/c++/5/bits/stl_construct.h \
 /usr/include/c++/5/bits/stl_uninitialized.h \
 /usr/include/c++/5/bits/stl_deque.h /usr/include/c++/5/bits/deque.tcc \
 /usr/include/expat.h /usr/include/expat_external.h \
 sprites/../data/gameData.h sprites/../math/vector2f.h \
 sprites/../graphics/components/image.h
	$(CC) -c $< -o $@ $(CFLAGS)
$(OBJDIR)enemy.o: sprites/enemy.cpp /usr/include/stdc-predef.h \
 /usr/include/c++/5/utility \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/os_defines.h \
 /usr/include/features.h /usr/include/x86_64-linux-gnu/sys/cdefs.h \
 /usr/include/x86_64-linux-gnu/bits/wordsize.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs-64.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/cpu_defines.h \
 /usr/include/c++/5/bits/stl_relops.h /usr/include/c++/5/bits/stl_pair.h \
 /usr/include/c++/5/bits/move.h /usr/include/c++/5/bits/concept_check.h \
 /usr/include/c++/5/type_traits /usr/include/c++/5/initializer_list \
 /usr/include/c++/5/cmath /usr/include/c++/5/bits/cpp_type_traits.h \
 /usr/include/c++/5/ext/type_traits.h /usr/include/math.h \
 /usr/include/x86_64-linux-gnu/bits/math-vector.h \
 /usr/include/x86_64-linux-gnu/bits/libm-simd-decl-stubs.h \
 /usr/include/x86_64-linux-gnu/bits/huge_val.h \
 /usr/include/x86_64-linux-gnu/bits/huge_valf.h \
 /usr/include/x86_64-linux-gnu/bits/huge_vall.h \
 /usr/include/x86_64-linux-gnu/bits/inf.h \
 /usr/include/x86_64-linux-gnu/bits/nan.h \
 /usr/include/x86_64-linux-gnu/bits/mathdef.h \
 /usr/include/x86_64-linux-gnu/bits/mathcalls.h /usr/include/c++/5/string \
 /usr/include/c++/5/bits/stringfwd.h /usr/include/c++/5/bits/memoryfwd.h \
 /usr/include/c++/5/bits/char_traits.h \
 /usr/include/c++/5/bits/stl_algobase.h \
 /usr/include/c++/5/bits/functexcept.h \
 /usr/include/c++/5/bits/exception_defines.h \
 /usr/include/c++/5/ext/numeric_traits.h \
 /usr/include/c++/5/bits/stl_iterator_base_types.h \
 /usr/include/c++/5/bits/stl_iterator_base_funcs.h \
 /usr/include/c++/5/debug/debug.h /usr/include/c++/5/bits/stl_iterator.h \
 /usr/include/c++/5/bits/ptr_traits.h \
 /usr/include/c++/5/bits/predefined_ops.h \
 /usr/include/c++/5/bits/postypes.h /usr/include/c++/5/cwchar \
 /usr/include/wchar.h /usr/include/stdio.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdarg.h \
 /usr/include/x86_64-linux-gnu/bits/wchar.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h /usr/include/xlocale.h \
 /usr/include/c++/5/cstdint \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdint.h /usr/include/stdint.h \
 /usr/include/c++/5/bits/allocator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++allocator.h \
 /usr/include/c++/5/ext/new_allocator.h /usr/include/c++/5/new \
 /usr/include/c++/5/exception \
 /usr/include/c++/5/bits/atomic_lockfree_defines.h \
 /usr/include/c++/5/bits/exception_ptr.h \
 /usr/include/c++/5/bits/nested_exception.h \
 /usr/include/c++/5/bits/localefwd.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++locale.h \
 /usr/include/c++/5/clocale /usr/include/locale.h \
 /usr/include/x86_64-linux-gnu/bits/locale.h /usr/include/c++/5/iosfwd \
 /usr/include/c++/5/cctype /usr/include/ctype.h \
 /usr/include/x86_64-linux-gnu/bits/types.h \
 /usr/include/x86_64-linux-gnu/bits/typesizes.h /usr/include/endian.h \
 /usr/include/x86_64-linux-gnu/bits/endian.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap-16.h \
 /usr/include/c++/5/bits/ostream_insert.h \
 /usr/include/c++/5/bits/cxxabi_forced.h \
 /usr/include/c++/5/bits/stl_function.h \
 /usr/include/c++/5/backward/binders.h \
 /usr/include/c++/5/bits/range_access.h \
 /usr/include/c++/5/bits/basic_string.h \
 /usr/include/c++/5/ext/atomicity.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr-default.h \
 /usr/include/pthread.h /usr/include/sched.h /usr/include/time.h \
 /usr/include/x86_64-linux-gnu/bits/sched.h \
 /usr/include/x86_64-linux-gnu/bits/time.h \
 /usr/include/x86_64-linux-gnu/bits/timex.h \
 /usr/include/x86_64-linux-gnu/bits/pthreadtypes.h \
 /usr/include/x86_64-linux-gnu/bits/setjmp.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h \
 /usr/include/c++/5/ext/alloc_traits.h \
 /usr/include/c++/5/bits/alloc_traits.h \
 /usr/include/c++/5/ext/string_conversions.h /usr/include/c++/5/cstdlib \
 /usr/include/stdlib.h /usr/include/x86_64-linux-gnu/bits/waitflags.h \
 /usr/include/x86_64-linux-gnu/bits/waitstatus.h \
 /usr/include/x86_64-linux-gnu/sys/types.h \
 /usr/include/x86_64-linux-gnu/sys/select.h \
 /usr/include/x86_64-linux-gnu/bits/select.h \
 /usr/include/x86_64-linux-gnu/bits/sigset.h \
 /usr/include/x86_64-linux-gnu/sys/sysmacros.h /usr/include/alloca.h \
 /usr/include/x86_64-linux-gnu/bits/stdlib-float.h \
 /usr/include/c++/5/cstdio /usr/include/libio.h /usr/include/_G_config.h \
 /usr/include/x86_64-linux-gnu/bits/stdio_lim.h \
 /usr/include/x86_64-linux-gnu/bits/sys_errlist.h \
 /usr/include/c++/5/cerrno /usr/include/errno.h \
 /usr/include/x86_64-linux-gnu/bits/errno.h /usr/include/linux/errno.h \
 /usr/include/x86_64-linux-gnu/asm/errno.h \
 /usr/include/asm-generic/errno.h /usr/include/asm-generic/errno-base.h \
 /usr/include/c++/5/bits/functional_hash.h \
 /usr/include/c++/5/bits/hash_bytes.h \
 /usr/include/c++/5/bits/basic_string.tcc sprites/sprite.h \
 sprites/../drawable.h  /usr/include/c++/5/iostream \
 /usr/include/c++/5/ostream /usr/include/c++/5/ios \
 /usr/include/c++/5/bits/ios_base.h \
 /usr/include/c++/5/bits/locale_classes.h \
 /usr/include/c++/5/bits/locale_classes.tcc \
 /usr/include/c++/5/system_error \
 /usr/include/x86_64-linux-gnu/c++/5/bits/error_constants.h \
 /usr/include/c++/5/stdexcept /usr/include/c++/5/streambuf \
 /usr/include/c++/5/bits/streambuf.tcc \
 /usr/include/c++/5/bits/basic_ios.h \
 /usr/include/c++/5/bits/locale_facets.h /usr/include/c++/5/cwctype \
 /usr/include/wctype.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_base.h \
 /usr/include/c++/5/bits/streambuf_iterator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_inline.h \
 /usr/include/c++/5/bits/locale_facets.tcc \
 /usr/include/c++/5/bits/basic_ios.tcc \
 /usr/include/c++/5/bits/ostream.tcc /usr/include/c++/5/istream \
 /usr/include/c++/5/bits/istream.tcc sprites/../math/vector2f.h \
 sprites/../graphics/components/image.h sprites/../data/gameData.h \
 /usr/include/c++/5/map /usr/include/c++/5/bits/stl_tree.h \
 /usr/include/c++/5/ext/aligned_buffer.h \
 /usr/include/c++/5/bits/stl_map.h /usr/include/c++/5/tuple \
 /usr/include/c++/5/array /usr/include/c++/5/bits/uses_allocator.h \
 /usr/include/c++/5/bits/stl_multimap.h \
 sprites/../data/../math/vector2f.h sprites/../data/parseXML.h \
 /usr/include/c++/5/fstream /usr/include/c++/5/bits/codecvt.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/basic_file.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++io.h \
 /usr/include/c++/5/bits/fstream.tcc /usr/include/c++/5/deque \
 /usr/include/c++/5/bits/stl_construct.h \
 /usr/include/c++/5/bits/stl_uninitialized.h \
 /usr/include/c++/5/bits/stl_deque.h /usr/include/c++/5/bits/deque.tcc \
 /usr/include/expat.h /usr/include/expat_external.h \
 sprites/../data/gameData.h sprites/../math/vector2f.h \
 sprites/walkingSprite.h sprites/enemy.h /usr/include/c++/5/functional \
 /usr/include/c++/5/typeinfo sprites/bidirectionalMultisprite.h \
 /usr/include/c++/5/vector /usr/include/c++/5/bits/stl_vector.h \
 /usr/include/c++/5/bits/stl_bvector.h /usr/include/c++/5/bits/vector.tcc \
 sprites/multisprite.h sprites/../observable/observable.h \
 sprites/explodingSprite.h /usr/include/c++/5/list \
 /usr/include/c++/5/bits/stl_list.h /usr/include/c++/5/bits/list.tcc \
 sprites/chunk.h sprites/../graphics/components/image.h
	$(CC) -c $< -o $@ $(CFLAGS)
$(OBJDIR)explodingSprite.o: sprites/explodingSprite.cpp /usr/include/stdc-predef.h \
 /usr/include/c++/5/iostream \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/os_defines.h \
 /usr/include/features.h /usr/include/x86_64-linux-gnu/sys/cdefs.h \
 /usr/include/x86_64-linux-gnu/bits/wordsize.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs-64.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/cpu_defines.h \
 /usr/include/c++/5/ostream /usr/include/c++/5/ios \
 /usr/include/c++/5/iosfwd /usr/include/c++/5/bits/stringfwd.h \
 /usr/include/c++/5/bits/memoryfwd.h /usr/include/c++/5/bits/postypes.h \
 /usr/include/c++/5/cwchar /usr/include/wchar.h /usr/include/stdio.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdarg.h \
 /usr/include/x86_64-linux-gnu/bits/wchar.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h /usr/include/xlocale.h \
 /usr/include/c++/5/exception \
 /usr/include/c++/5/bits/atomic_lockfree_defines.h \
 /usr/include/c++/5/bits/exception_ptr.h \
 /usr/include/c++/5/bits/exception_defines.h \
 /usr/include/c++/5/bits/nested_exception.h \
 /usr/include/c++/5/bits/char_traits.h \
 /usr/include/c++/5/bits/stl_algobase.h \
 /usr/include/c++/5/bits/functexcept.h \
 /usr/include/c++/5/bits/cpp_type_traits.h \
 /usr/include/c++/5/ext/type_traits.h \
 /usr/include/c++/5/ext/numeric_traits.h \
 /usr/include/c++/5/bits/stl_pair.h /usr/include/c++/5/bits/move.h \
 /usr/include/c++/5/bits/concept_check.h /usr/include/c++/5/type_traits \
 /usr/include/c++/5/bits/stl_iterator_base_types.h \
 /usr/include/c++/5/bits/stl_iterator_base_funcs.h \
 /usr/include/c++/5/debug/debug.h /usr/include/c++/5/bits/stl_iterator.h \
 /usr/include/c++/5/bits/ptr_traits.h \
 /usr/include/c++/5/bits/predefined_ops.h /usr/include/c++/5/cstdint \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdint.h /usr/include/stdint.h \
 /usr/include/c++/5/bits/localefwd.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++locale.h \
 /usr/include/c++/5/clocale /usr/include/locale.h \
 /usr/include/x86_64-linux-gnu/bits/locale.h /usr/include/c++/5/cctype \
 /usr/include/ctype.h /usr/include/x86_64-linux-gnu/bits/types.h \
 /usr/include/x86_64-linux-gnu/bits/typesizes.h /usr/include/endian.h \
 /usr/include/x86_64-linux-gnu/bits/endian.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap-16.h \
 /usr/include/c++/5/bits/ios_base.h /usr/include/c++/5/ext/atomicity.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr-default.h \
 /usr/include/pthread.h /usr/include/sched.h /usr/include/time.h \
 /usr/include/x86_64-linux-gnu/bits/sched.h \
 /usr/include/x86_64-linux-gnu/bits/time.h \
 /usr/include/x86_64-linux-gnu/bits/timex.h \
 /usr/include/x86_64-linux-gnu/bits/pthreadtypes.h \
 /usr/include/x86_64-linux-gnu/bits/setjmp.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h \
 /usr/include/c++/5/bits/locale_classes.h /usr/include/c++/5/string \
 /usr/include/c++/5/bits/allocator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++allocator.h \
 /usr/include/c++/5/ext/new_allocator.h /usr/include/c++/5/new \
 /usr/include/c++/5/bits/ostream_insert.h \
 /usr/include/c++/5/bits/cxxabi_forced.h \
 /usr/include/c++/5/bits/stl_function.h \
 /usr/include/c++/5/backward/binders.h \
 /usr/include/c++/5/bits/range_access.h \
 /usr/include/c++/5/initializer_list \
 /usr/include/c++/5/bits/basic_string.h \
 /usr/include/c++/5/ext/alloc_traits.h \
 /usr/include/c++/5/bits/alloc_traits.h \
 /usr/include/c++/5/ext/string_conversions.h /usr/include/c++/5/cstdlib \
 /usr/include/stdlib.h /usr/include/x86_64-linux-gnu/bits/waitflags.h \
 /usr/include/x86_64-linux-gnu/bits/waitstatus.h \
 /usr/include/x86_64-linux-gnu/sys/types.h \
 /usr/include/x86_64-linux-gnu/sys/select.h \
 /usr/include/x86_64-linux-gnu/bits/select.h \
 /usr/include/x86_64-linux-gnu/bits/sigset.h \
 /usr/include/x86_64-linux-gnu/sys/sysmacros.h /usr/include/alloca.h \
 /usr/include/x86_64-linux-gnu/bits/stdlib-float.h \
 /usr/include/c++/5/cstdio /usr/include/libio.h /usr/include/_G_config.h \
 /usr/include/x86_64-linux-gnu/bits/stdio_lim.h \
 /usr/include/x86_64-linux-gnu/bits/sys_errlist.h \
 /usr/include/c++/5/cerrno /usr/include/errno.h \
 /usr/include/x86_64-linux-gnu/bits/errno.h /usr/include/linux/errno.h \
 /usr/include/x86_64-linux-gnu/asm/errno.h \
 /usr/include/asm-generic/errno.h /usr/include/asm-generic/errno-base.h \
 /usr/include/c++/5/bits/functional_hash.h \
 /usr/include/c++/5/bits/hash_bytes.h \
 /usr/include/c++/5/bits/basic_string.tcc \
 /usr/include/c++/5/bits/locale_classes.tcc \
 /usr/include/c++/5/system_error \
 /usr/include/x86_64-linux-gnu/c++/5/bits/error_constants.h \
 /usr/include/c++/5/stdexcept /usr/include/c++/5/streambuf \
 /usr/include/c++/5/bits/streambuf.tcc \
 /usr/include/c++/5/bits/basic_ios.h \
 /usr/include/c++/5/bits/locale_facets.h /usr/include/c++/5/cwctype \
 /usr/include/wctype.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_base.h \
 /usr/include/c++/5/bits/streambuf_iterator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_inline.h \
 /usr/include/c++/5/bits/locale_facets.tcc \
 /usr/include/c++/5/bits/basic_ios.tcc \
 /usr/include/c++/5/bits/ostream.tcc /usr/include/c++/5/istream \
 /usr/include/c++/5/bits/istream.tcc /usr/include/c++/5/cmath \
 /usr/include/math.h /usr/include/x86_64-linux-gnu/bits/math-vector.h \
 /usr/include/x86_64-linux-gnu/bits/libm-simd-decl-stubs.h \
 /usr/include/x86_64-linux-gnu/bits/huge_val.h \
 /usr/include/x86_64-linux-gnu/bits/huge_valf.h \
 /usr/include/x86_64-linux-gnu/bits/huge_vall.h \
 /usr/include/x86_64-linux-gnu/bits/inf.h \
 /usr/include/x86_64-linux-gnu/bits/nan.h \
 /usr/include/x86_64-linux-gnu/bits/mathdef.h \
 /usr/include/x86_64-linux-gnu/bits/mathcalls.h sprites/explodingSprite.h \
 /usr/include/c++/5/vector /usr/include/c++/5/bits/stl_construct.h \
 /usr/include/c++/5/bits/stl_uninitialized.h \
 /usr/include/c++/5/bits/stl_vector.h \
 /usr/include/c++/5/bits/stl_bvector.h /usr/include/c++/5/bits/vector.tcc \
 /usr/include/c++/5/list /usr/include/c++/5/bits/stl_list.h \
 /usr/include/c++/5/bits/list.tcc sprites/chunk.h sprites/sprite.h \
 sprites/../drawable.h  sprites/../math/vector2f.h \
 sprites/../graphics/components/image.h sprites/../data/gameData.h \
 /usr/include/c++/5/map /usr/include/c++/5/bits/stl_tree.h \
 /usr/include/c++/5/ext/aligned_buffer.h \
 /usr/include/c++/5/bits/stl_map.h /usr/include/c++/5/tuple \
 /usr/include/c++/5/utility /usr/include/c++/5/bits/stl_relops.h \
 /usr/include/c++/5/array /usr/include/c++/5/bits/uses_allocator.h \
 /usr/include/c++/5/bits/stl_multimap.h \
 sprites/../data/../math/vector2f.h sprites/../data/parseXML.h \
 /usr/include/c++/5/fstream /usr/include/c++/5/bits/codecvt.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/basic_file.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++io.h \
 /usr/include/c++/5/bits/fstream.tcc /usr/include/c++/5/deque \
 /usr/include/c++/5/bits/stl_deque.h /usr/include/c++/5/bits/deque.tcc \
 /usr/include/expat.h /usr/include/expat_external.h \
 sprites/../data/gameData.h sprites/../math/vector2f.h \
 sprites/../graphics/components/image.h
	$(CC) -c $< -o $@ $(CFLAGS)
$(OBJDIR)multisprite.o: sprites/multisprite.cpp /usr/include/stdc-predef.h \
 sprites/multisprite.h /usr/include/c++/5/string \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/os_defines.h \
 /usr/include/features.h /usr/include/x86_64-linux-gnu/sys/cdefs.h \
 /usr/include/x86_64-linux-gnu/bits/wordsize.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs-64.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/cpu_defines.h \
 /usr/include/c++/5/bits/stringfwd.h /usr/include/c++/5/bits/memoryfwd.h \
 /usr/include/c++/5/bits/char_traits.h \
 /usr/include/c++/5/bits/stl_algobase.h \
 /usr/include/c++/5/bits/functexcept.h \
 /usr/include/c++/5/bits/exception_defines.h \
 /usr/include/c++/5/bits/cpp_type_traits.h \
 /usr/include/c++/5/ext/type_traits.h \
 /usr/include/c++/5/ext/numeric_traits.h \
 /usr/include/c++/5/bits/stl_pair.h /usr/include/c++/5/bits/move.h \
 /usr/include/c++/5/bits/concept_check.h /usr/include/c++/5/type_traits \
 /usr/include/c++/5/bits/stl_iterator_base_types.h \
 /usr/include/c++/5/bits/stl_iterator_base_funcs.h \
 /usr/include/c++/5/debug/debug.h /usr/include/c++/5/bits/stl_iterator.h \
 /usr/include/c++/5/bits/ptr_traits.h \
 /usr/include/c++/5/bits/predefined_ops.h \
 /usr/include/c++/5/bits/postypes.h /usr/include/c++/5/cwchar \
 /usr/include/wchar.h /usr/include/stdio.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdarg.h \
 /usr/include/x86_64-linux-gnu/bits/wchar.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h /usr/include/xlocale.h \
 /usr/include/c++/5/cstdint \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdint.h /usr/include/stdint.h \
 /usr/include/c++/5/bits/allocator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++allocator.h \
 /usr/include/c++/5/ext/new_allocator.h /usr/include/c++/5/new \
 /usr/include/c++/5/exception \
 /usr/include/c++/5/bits/atomic_lockfree_defines.h \
 /usr/include/c++/5/bits/exception_ptr.h \
 /usr/include/c++/5/bits/nested_exception.h \
 /usr/include/c++/5/bits/localefwd.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++locale.h \
 /usr/include/c++/5/clocale /usr/include/locale.h \
 /usr/include/x86_64-linux-gnu/bits/locale.h /usr/include/c++/5/iosfwd \
 /usr/include/c++/5/cctype /usr/include/ctype.h \
 /usr/include/x86_64-linux-gnu/bits/types.h \
 /usr/include/x86_64-linux-gnu/bits/typesizes.h /usr/include/endian.h \
 /usr/include/x86_64-linux-gnu/bits/endian.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap-16.h \
 /usr/include/c++/5/bits/ostream_insert.h \
 /usr/include/c++/5/bits/cxxabi_forced.h \
 /usr/include/c++/5/bits/stl_function.h \
 /usr/include/c++/5/backward/binders.h \
 /usr/include/c++/5/bits/range_access.h \
 /usr/include/c++/5/initializer_list \
 /usr/include/c++/5/bits/basic_string.h \
 /usr/include/c++/5/ext/atomicity.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr-default.h \
 /usr/include/pthread.h /usr/include/sched.h /usr/include/time.h \
 /usr/include/x86_64-linux-gnu/bits/sched.h \
 /usr/include/x86_64-linux-gnu/bits/time.h \
 /usr/include/x86_64-linux-gnu/bits/timex.h \
 /usr/include/x86_64-linux-gnu/bits/pthreadtypes.h \
 /usr/include/x86_64-linux-gnu/bits/setjmp.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h \
 /usr/include/c++/5/ext/alloc_traits.h \
 /usr/include/c++/5/bits/alloc_traits.h \
 /usr/include/c++/5/ext/string_conversions.h /usr/include/c++/5/cstdlib \
 /usr/include/stdlib.h /usr/include/x86_64-linux-gnu/bits/waitflags.h \
 /usr/include/x86_64-linux-gnu/bits/waitstatus.h \
 /usr/include/x86_64-linux-gnu/sys/types.h \
 /usr/include/x86_64-linux-gnu/sys/select.h \
 /usr/include/x86_64-linux-gnu/bits/select.h \
 /usr/include/x86_64-linux-gnu/bits/sigset.h \
 /usr/include/x86_64-linux-gnu/sys/sysmacros.h /usr/include/alloca.h \
 /usr/include/x86_64-linux-gnu/bits/stdlib-float.h \
 /usr/include/c++/5/cstdio /usr/include/libio.h /usr/include/_G_config.h \
 /usr/include/x86_64-linux-gnu/bits/stdio_lim.h \
 /usr/include/x86_64-linux-gnu/bits/sys_errlist.h \
 /usr/include/c++/5/cerrno /usr/include/errno.h \
 /usr/include/x86_64-linux-gnu/bits/errno.h /usr/include/linux/errno.h \
 /usr/include/x86_64-linux-gnu/asm/errno.h \
 /usr/include/asm-generic/errno.h /usr/include/asm-generic/errno-base.h \
 /usr/include/c++/5/bits/functional_hash.h \
 /usr/include/c++/5/bits/hash_bytes.h \
 /usr/include/c++/5/bits/basic_string.tcc /usr/include/c++/5/vector \
 /usr/include/c++/5/bits/stl_construct.h \
 /usr/include/c++/5/bits/stl_uninitialized.h \
 /usr/include/c++/5/bits/stl_vector.h \
 /usr/include/c++/5/bits/stl_bvector.h /usr/include/c++/5/bits/vector.tcc \
 /usr/include/c++/5/cmath /usr/include/math.h \
 /usr/include/x86_64-linux-gnu/bits/math-vector.h \
 /usr/include/x86_64-linux-gnu/bits/libm-simd-decl-stubs.h \
 /usr/include/x86_64-linux-gnu/bits/huge_val.h \
 /usr/include/x86_64-linux-gnu/bits/huge_valf.h \
 /usr/include/x86_64-linux-gnu/bits/huge_vall.h \
 /usr/include/x86_64-linux-gnu/bits/inf.h \
 /usr/include/x86_64-linux-gnu/bits/nan.h \
 /usr/include/x86_64-linux-gnu/bits/mathdef.h \
 /usr/include/x86_64-linux-gnu/bits/mathcalls.h sprites/sprite.h \
 sprites/../drawable.h  /usr/include/c++/5/iostream \
 /usr/include/c++/5/ostream /usr/include/c++/5/ios \
 /usr/include/c++/5/bits/ios_base.h \
 /usr/include/c++/5/bits/locale_classes.h \
 /usr/include/c++/5/bits/locale_classes.tcc \
 /usr/include/c++/5/system_error \
 /usr/include/x86_64-linux-gnu/c++/5/bits/error_constants.h \
 /usr/include/c++/5/stdexcept /usr/include/c++/5/streambuf \
 /usr/include/c++/5/bits/streambuf.tcc \
 /usr/include/c++/5/bits/basic_ios.h \
 /usr/include/c++/5/bits/locale_facets.h /usr/include/c++/5/cwctype \
 /usr/include/wctype.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_base.h \
 /usr/include/c++/5/bits/streambuf_iterator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_inline.h \
 /usr/include/c++/5/bits/locale_facets.tcc \
 /usr/include/c++/5/bits/basic_ios.tcc \
 /usr/include/c++/5/bits/ostream.tcc /usr/include/c++/5/istream \
 /usr/include/c++/5/bits/istream.tcc sprites/../math/vector2f.h \
 sprites/../graphics/components/image.h sprites/../data/gameData.h \
 /usr/include/c++/5/map /usr/include/c++/5/bits/stl_tree.h \
 /usr/include/c++/5/ext/aligned_buffer.h \
 /usr/include/c++/5/bits/stl_map.h /usr/include/c++/5/tuple \
 /usr/include/c++/5/utility /usr/include/c++/5/bits/stl_relops.h \
 /usr/include/c++/5/array /usr/include/c++/5/bits/uses_allocator.h \
 /usr/include/c++/5/bits/stl_multimap.h \
 sprites/../data/../math/vector2f.h sprites/../data/parseXML.h \
 /usr/include/c++/5/fstream /usr/include/c++/5/bits/codecvt.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/basic_file.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++io.h \
 /usr/include/c++/5/bits/fstream.tcc /usr/include/c++/5/deque \
 /usr/include/c++/5/bits/stl_deque.h /usr/include/c++/5/bits/deque.tcc \
 /usr/include/expat.h /usr/include/expat_external.h \
 sprites/../data/gameData.h sprites/../math/vector2f.h \
 sprites/../graphics/imageFactory.h \
 sprites/../graphics/components/image.h \
 sprites/../graphics/../data/gameData.h \
 sprites/../graphics/../sprites/spriteSheet.h  
	$(CC) -c $< -o $@ $(CFLAGS)
$(OBJDIR)player.o: sprites/player.cpp /usr/include/stdc-predef.h \
 sprites/walkingSprite.h sprites/sprite.h /usr/include/c++/5/string \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/os_defines.h \
 /usr/include/features.h /usr/include/x86_64-linux-gnu/sys/cdefs.h \
 /usr/include/x86_64-linux-gnu/bits/wordsize.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs-64.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/cpu_defines.h \
 /usr/include/c++/5/bits/stringfwd.h /usr/include/c++/5/bits/memoryfwd.h \
 /usr/include/c++/5/bits/char_traits.h \
 /usr/include/c++/5/bits/stl_algobase.h \
 /usr/include/c++/5/bits/functexcept.h \
 /usr/include/c++/5/bits/exception_defines.h \
 /usr/include/c++/5/bits/cpp_type_traits.h \
 /usr/include/c++/5/ext/type_traits.h \
 /usr/include/c++/5/ext/numeric_traits.h \
 /usr/include/c++/5/bits/stl_pair.h /usr/include/c++/5/bits/move.h \
 /usr/include/c++/5/bits/concept_check.h /usr/include/c++/5/type_traits \
 /usr/include/c++/5/bits/stl_iterator_base_types.h \
 /usr/include/c++/5/bits/stl_iterator_base_funcs.h \
 /usr/include/c++/5/debug/debug.h /usr/include/c++/5/bits/stl_iterator.h \
 /usr/include/c++/5/bits/ptr_traits.h \
 /usr/include/c++/5/bits/predefined_ops.h \
 /usr/include/c++/5/bits/postypes.h /usr/include/c++/5/cwchar \
 /usr/include/wchar.h /usr/include/stdio.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdarg.h \
 /usr/include/x86_64-linux-gnu/bits/wchar.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h /usr/include/xlocale.h \
 /usr/include/c++/5/cstdint \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdint.h /usr/include/stdint.h \
 /usr/include/c++/5/bits/allocator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++allocator.h \
 /usr/include/c++/5/ext/new_allocator.h /usr/include/c++/5/new \
 /usr/include/c++/5/exception \
 /usr/include/c++/5/bits/atomic_lockfree_defines.h \
 /usr/include/c++/5/bits/exception_ptr.h \
 /usr/include/c++/5/bits/nested_exception.h \
 /usr/include/c++/5/bits/localefwd.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++locale.h \
 /usr/include/c++/5/clocale /usr/include/locale.h \
 /usr/include/x86_64-linux-gnu/bits/locale.h /usr/include/c++/5/iosfwd \
 /usr/include/c++/5/cctype /usr/include/ctype.h \
 /usr/include/x86_64-linux-gnu/bits/types.h \
 /usr/include/x86_64-linux-gnu/bits/typesizes.h /usr/include/endian.h \
 /usr/include/x86_64-linux-gnu/bits/endian.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap-16.h \
 /usr/include/c++/5/bits/ostream_insert.h \
 /usr/include/c++/5/bits/cxxabi_forced.h \
 /usr/include/c++/5/bits/stl_function.h \
 /usr/include/c++/5/backward/binders.h \
 /usr/include/c++/5/bits/range_access.h \
 /usr/include/c++/5/initializer_list \
 /usr/include/c++/5/bits/basic_string.h \
 /usr/include/c++/5/ext/atomicity.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr-default.h \
 /usr/include/pthread.h /usr/include/sched.h /usr/include/time.h \
 /usr/include/x86_64-linux-gnu/bits/sched.h \
 /usr/include/x86_64-linux-gnu/bits/time.h \
 /usr/include/x86_64-linux-gnu/bits/timex.h \
 /usr/include/x86_64-linux-gnu/bits/pthreadtypes.h \
 /usr/include/x86_64-linux-gnu/bits/setjmp.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h \
 /usr/include/c++/5/ext/alloc_traits.h \
 /usr/include/c++/5/bits/alloc_traits.h \
 /usr/include/c++/5/ext/string_conversions.h /usr/include/c++/5/cstdlib \
 /usr/include/stdlib.h /usr/include/x86_64-linux-gnu/bits/waitflags.h \
 /usr/include/x86_64-linux-gnu/bits/waitstatus.h \
 /usr/include/x86_64-linux-gnu/sys/types.h \
 /usr/include/x86_64-linux-gnu/sys/select.h \
 /usr/include/x86_64-linux-gnu/bits/select.h \
 /usr/include/x86_64-linux-gnu/bits/sigset.h \
 /usr/include/x86_64-linux-gnu/sys/sysmacros.h /usr/include/alloca.h \
 /usr/include/x86_64-linux-gnu/bits/stdlib-float.h \
 /usr/include/c++/5/cstdio /usr/include/libio.h /usr/include/_G_config.h \
 /usr/include/x86_64-linux-gnu/bits/stdio_lim.h \
 /usr/include/x86_64-linux-gnu/bits/sys_errlist.h \
 /usr/include/c++/5/cerrno /usr/include/errno.h \
 /usr/include/x86_64-linux-gnu/bits/errno.h /usr/include/linux/errno.h \
 /usr/include/x86_64-linux-gnu/asm/errno.h \
 /usr/include/asm-generic/errno.h /usr/include/asm-generic/errno-base.h \
 /usr/include/c++/5/bits/functional_hash.h \
 /usr/include/c++/5/bits/hash_bytes.h \
 /usr/include/c++/5/bits/basic_string.tcc sprites/../drawable.h  \
 /usr/include/c++/5/iostream /usr/include/c++/5/ostream \
 /usr/include/c++/5/ios /usr/include/c++/5/bits/ios_base.h \
 /usr/include/c++/5/bits/locale_classes.h \
 /usr/include/c++/5/bits/locale_classes.tcc \
 /usr/include/c++/5/system_error \
 /usr/include/x86_64-linux-gnu/c++/5/bits/error_constants.h \
 /usr/include/c++/5/stdexcept /usr/include/c++/5/streambuf \
 /usr/include/c++/5/bits/streambuf.tcc \
 /usr/include/c++/5/bits/basic_ios.h \
 /usr/include/c++/5/bits/locale_facets.h /usr/include/c++/5/cwctype \
 /usr/include/wctype.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_base.h \
 /usr/include/c++/5/bits/streambuf_iterator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_inline.h \
 /usr/include/c++/5/bits/locale_facets.tcc \
 /usr/include/c++/5/bits/basic_ios.tcc \
 /usr/include/c++/5/bits/ostream.tcc /usr/include/c++/5/istream \
 /usr/include/c++/5/bits/istream.tcc sprites/../math/vector2f.h \
 sprites/../graphics/components/image.h sprites/../data/gameData.h \
 /usr/include/c++/5/map /usr/include/c++/5/bits/stl_tree.h \
 /usr/include/c++/5/ext/aligned_buffer.h \
 /usr/include/c++/5/bits/stl_map.h /usr/include/c++/5/tuple \
 /usr/include/c++/5/utility /usr/include/c++/5/bits/stl_relops.h \
 /usr/include/c++/5/array /usr/include/c++/5/bits/uses_allocator.h \
 /usr/include/c++/5/bits/stl_multimap.h \
 sprites/../data/../math/vector2f.h sprites/../data/parseXML.h \
 /usr/include/c++/5/fstream /usr/include/c++/5/bits/codecvt.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/basic_file.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++io.h \
 /usr/include/c++/5/bits/fstream.tcc /usr/include/c++/5/deque \
 /usr/include/c++/5/bits/stl_construct.h \
 /usr/include/c++/5/bits/stl_uninitialized.h \
 /usr/include/c++/5/bits/stl_deque.h /usr/include/c++/5/bits/deque.tcc \
 /usr/include/expat.h /usr/include/expat_external.h \
 sprites/../data/gameData.h sprites/../math/vector2f.h \
 sprites/bidirectionalMultisprite.h /usr/include/c++/5/vector \
 /usr/include/c++/5/bits/stl_vector.h \
 /usr/include/c++/5/bits/stl_bvector.h /usr/include/c++/5/bits/vector.tcc \
 sprites/multisprite.h /usr/include/c++/5/cmath /usr/include/math.h \
 /usr/include/x86_64-linux-gnu/bits/math-vector.h \
 /usr/include/x86_64-linux-gnu/bits/libm-simd-decl-stubs.h \
 /usr/include/x86_64-linux-gnu/bits/huge_val.h \
 /usr/include/x86_64-linux-gnu/bits/huge_valf.h \
 /usr/include/x86_64-linux-gnu/bits/huge_vall.h \
 /usr/include/x86_64-linux-gnu/bits/inf.h \
 /usr/include/x86_64-linux-gnu/bits/nan.h \
 /usr/include/x86_64-linux-gnu/bits/mathdef.h \
 /usr/include/x86_64-linux-gnu/bits/mathcalls.h sprites/player.h \
 /usr/include/c++/5/list /usr/include/c++/5/bits/stl_list.h \
 /usr/include/c++/5/bits/list.tcc sprites/../managers/projectilePool.h \
 sprites/../managers/../sprites/projectileSprite.h \
 sprites/../managers/../sprites/sprite.h \
 sprites/../managers/../math/vector2f.h sprites/../managers/trapPool.h \
 sprites/../managers/../sprites/multisprite.h
	$(CC) -c $< -o $@ $(CFLAGS)
$(OBJDIR)projectileSprite.o: sprites/projectileSprite.cpp \
 /usr/include/stdc-predef.h sprites/projectileSprite.h \
 /usr/include/c++/5/string \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/os_defines.h \
 /usr/include/features.h /usr/include/x86_64-linux-gnu/sys/cdefs.h \
 /usr/include/x86_64-linux-gnu/bits/wordsize.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs-64.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/cpu_defines.h \
 /usr/include/c++/5/bits/stringfwd.h /usr/include/c++/5/bits/memoryfwd.h \
 /usr/include/c++/5/bits/char_traits.h \
 /usr/include/c++/5/bits/stl_algobase.h \
 /usr/include/c++/5/bits/functexcept.h \
 /usr/include/c++/5/bits/exception_defines.h \
 /usr/include/c++/5/bits/cpp_type_traits.h \
 /usr/include/c++/5/ext/type_traits.h \
 /usr/include/c++/5/ext/numeric_traits.h \
 /usr/include/c++/5/bits/stl_pair.h /usr/include/c++/5/bits/move.h \
 /usr/include/c++/5/bits/concept_check.h /usr/include/c++/5/type_traits \
 /usr/include/c++/5/bits/stl_iterator_base_types.h \
 /usr/include/c++/5/bits/stl_iterator_base_funcs.h \
 /usr/include/c++/5/debug/debug.h /usr/include/c++/5/bits/stl_iterator.h \
 /usr/include/c++/5/bits/ptr_traits.h \
 /usr/include/c++/5/bits/predefined_ops.h \
 /usr/include/c++/5/bits/postypes.h /usr/include/c++/5/cwchar \
 /usr/include/wchar.h /usr/include/stdio.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdarg.h \
 /usr/include/x86_64-linux-gnu/bits/wchar.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h /usr/include/xlocale.h \
 /usr/include/c++/5/cstdint \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdint.h /usr/include/stdint.h \
 /usr/include/c++/5/bits/allocator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++allocator.h \
 /usr/include/c++/5/ext/new_allocator.h /usr/include/c++/5/new \
 /usr/include/c++/5/exception \
 /usr/include/c++/5/bits/atomic_lockfree_defines.h \
 /usr/include/c++/5/bits/exception_ptr.h \
 /usr/include/c++/5/bits/nested_exception.h \
 /usr/include/c++/5/bits/localefwd.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++locale.h \
 /usr/include/c++/5/clocale /usr/include/locale.h \
 /usr/include/x86_64-linux-gnu/bits/locale.h /usr/include/c++/5/iosfwd \
 /usr/include/c++/5/cctype /usr/include/ctype.h \
 /usr/include/x86_64-linux-gnu/bits/types.h \
 /usr/include/x86_64-linux-gnu/bits/typesizes.h /usr/include/endian.h \
 /usr/include/x86_64-linux-gnu/bits/endian.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap-16.h \
 /usr/include/c++/5/bits/ostream_insert.h \
 /usr/include/c++/5/bits/cxxabi_forced.h \
 /usr/include/c++/5/bits/stl_function.h \
 /usr/include/c++/5/backward/binders.h \
 /usr/include/c++/5/bits/range_access.h \
 /usr/include/c++/5/initializer_list \
 /usr/include/c++/5/bits/basic_string.h \
 /usr/include/c++/5/ext/atomicity.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr-default.h \
 /usr/include/pthread.h /usr/include/sched.h /usr/include/time.h \
 /usr/include/x86_64-linux-gnu/bits/sched.h \
 /usr/include/x86_64-linux-gnu/bits/time.h \
 /usr/include/x86_64-linux-gnu/bits/timex.h \
 /usr/include/x86_64-linux-gnu/bits/pthreadtypes.h \
 /usr/include/x86_64-linux-gnu/bits/setjmp.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h \
 /usr/include/c++/5/ext/alloc_traits.h \
 /usr/include/c++/5/bits/alloc_traits.h \
 /usr/include/c++/5/ext/string_conversions.h /usr/include/c++/5/cstdlib \
 /usr/include/stdlib.h /usr/include/x86_64-linux-gnu/bits/waitflags.h \
 /usr/include/x86_64-linux-gnu/bits/waitstatus.h \
 /usr/include/x86_64-linux-gnu/sys/types.h \
 /usr/include/x86_64-linux-gnu/sys/select.h \
 /usr/include/x86_64-linux-gnu/bits/select.h \
 /usr/include/x86_64-linux-gnu/bits/sigset.h \
 /usr/include/x86_64-linux-gnu/sys/sysmacros.h /usr/include/alloca.h \
 /usr/include/x86_64-linux-gnu/bits/stdlib-float.h \
 /usr/include/c++/5/cstdio /usr/include/libio.h /usr/include/_G_config.h \
 /usr/include/x86_64-linux-gnu/bits/stdio_lim.h \
 /usr/include/x86_64-linux-gnu/bits/sys_errlist.h \
 /usr/include/c++/5/cerrno /usr/include/errno.h \
 /usr/include/x86_64-linux-gnu/bits/errno.h /usr/include/linux/errno.h \
 /usr/include/x86_64-linux-gnu/asm/errno.h \
 /usr/include/asm-generic/errno.h /usr/include/asm-generic/errno-base.h \
 /usr/include/c++/5/bits/functional_hash.h \
 /usr/include/c++/5/bits/hash_bytes.h \
 /usr/include/c++/5/bits/basic_string.tcc sprites/sprite.h \
 sprites/../drawable.h  /usr/include/c++/5/iostream \
 /usr/include/c++/5/ostream /usr/include/c++/5/ios \
 /usr/include/c++/5/bits/ios_base.h \
 /usr/include/c++/5/bits/locale_classes.h \
 /usr/include/c++/5/bits/locale_classes.tcc \
 /usr/include/c++/5/system_error \
 /usr/include/x86_64-linux-gnu/c++/5/bits/error_constants.h \
 /usr/include/c++/5/stdexcept /usr/include/c++/5/streambuf \
 /usr/include/c++/5/bits/streambuf.tcc \
 /usr/include/c++/5/bits/basic_ios.h \
 /usr/include/c++/5/bits/locale_facets.h /usr/include/c++/5/cwctype \
 /usr/include/wctype.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_base.h \
 /usr/include/c++/5/bits/streambuf_iterator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_inline.h \
 /usr/include/c++/5/bits/locale_facets.tcc \
 /usr/include/c++/5/bits/basic_ios.tcc \
 /usr/include/c++/5/bits/ostream.tcc /usr/include/c++/5/istream \
 /usr/include/c++/5/bits/istream.tcc sprites/../math/vector2f.h \
 sprites/../graphics/components/image.h sprites/../data/gameData.h \
 /usr/include/c++/5/map /usr/include/c++/5/bits/stl_tree.h \
 /usr/include/c++/5/ext/aligned_buffer.h \
 /usr/include/c++/5/bits/stl_map.h /usr/include/c++/5/tuple \
 /usr/include/c++/5/utility /usr/include/c++/5/bits/stl_relops.h \
 /usr/include/c++/5/array /usr/include/c++/5/bits/uses_allocator.h \
 /usr/include/c++/5/bits/stl_multimap.h \
 sprites/../data/../math/vector2f.h sprites/../data/parseXML.h \
 /usr/include/c++/5/fstream /usr/include/c++/5/bits/codecvt.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/basic_file.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++io.h \
 /usr/include/c++/5/bits/fstream.tcc /usr/include/c++/5/deque \
 /usr/include/c++/5/bits/stl_construct.h \
 /usr/include/c++/5/bits/stl_uninitialized.h \
 /usr/include/c++/5/bits/stl_deque.h /usr/include/c++/5/bits/deque.tcc \
 /usr/include/expat.h /usr/include/expat_external.h \
 sprites/../data/gameData.h sprites/../math/vector2f.h
	$(CC) -c $< -o $@ $(CFLAGS)
$(OBJDIR)sprite.o: sprites/sprite.cpp /usr/include/stdc-predef.h \
 /usr/include/c++/5/cmath \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/os_defines.h \
 /usr/include/features.h /usr/include/x86_64-linux-gnu/sys/cdefs.h \
 /usr/include/x86_64-linux-gnu/bits/wordsize.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs-64.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/cpu_defines.h \
 /usr/include/c++/5/bits/cpp_type_traits.h \
 /usr/include/c++/5/ext/type_traits.h /usr/include/math.h \
 /usr/include/x86_64-linux-gnu/bits/math-vector.h \
 /usr/include/x86_64-linux-gnu/bits/libm-simd-decl-stubs.h \
 /usr/include/x86_64-linux-gnu/bits/huge_val.h \
 /usr/include/x86_64-linux-gnu/bits/huge_valf.h \
 /usr/include/x86_64-linux-gnu/bits/huge_vall.h \
 /usr/include/x86_64-linux-gnu/bits/inf.h \
 /usr/include/x86_64-linux-gnu/bits/nan.h \
 /usr/include/x86_64-linux-gnu/bits/mathdef.h \
 /usr/include/x86_64-linux-gnu/bits/mathcalls.h /usr/include/c++/5/random \
 /usr/include/c++/5/cstdlib /usr/include/stdlib.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h \
 /usr/include/x86_64-linux-gnu/bits/waitflags.h \
 /usr/include/x86_64-linux-gnu/bits/waitstatus.h /usr/include/endian.h \
 /usr/include/x86_64-linux-gnu/bits/endian.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap.h \
 /usr/include/x86_64-linux-gnu/bits/types.h \
 /usr/include/x86_64-linux-gnu/bits/typesizes.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap-16.h /usr/include/xlocale.h \
 /usr/include/x86_64-linux-gnu/sys/types.h /usr/include/time.h \
 /usr/include/x86_64-linux-gnu/sys/select.h \
 /usr/include/x86_64-linux-gnu/bits/select.h \
 /usr/include/x86_64-linux-gnu/bits/sigset.h \
 /usr/include/x86_64-linux-gnu/bits/time.h \
 /usr/include/x86_64-linux-gnu/sys/sysmacros.h \
 /usr/include/x86_64-linux-gnu/bits/pthreadtypes.h /usr/include/alloca.h \
 /usr/include/x86_64-linux-gnu/bits/stdlib-float.h \
 /usr/include/c++/5/string /usr/include/c++/5/bits/stringfwd.h \
 /usr/include/c++/5/bits/memoryfwd.h \
 /usr/include/c++/5/bits/char_traits.h \
 /usr/include/c++/5/bits/stl_algobase.h \
 /usr/include/c++/5/bits/functexcept.h \
 /usr/include/c++/5/bits/exception_defines.h \
 /usr/include/c++/5/ext/numeric_traits.h \
 /usr/include/c++/5/bits/stl_pair.h /usr/include/c++/5/bits/move.h \
 /usr/include/c++/5/bits/concept_check.h /usr/include/c++/5/type_traits \
 /usr/include/c++/5/bits/stl_iterator_base_types.h \
 /usr/include/c++/5/bits/stl_iterator_base_funcs.h \
 /usr/include/c++/5/debug/debug.h /usr/include/c++/5/bits/stl_iterator.h \
 /usr/include/c++/5/bits/ptr_traits.h \
 /usr/include/c++/5/bits/predefined_ops.h \
 /usr/include/c++/5/bits/postypes.h /usr/include/c++/5/cwchar \
 /usr/include/wchar.h /usr/include/stdio.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdarg.h \
 /usr/include/x86_64-linux-gnu/bits/wchar.h /usr/include/c++/5/cstdint \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdint.h /usr/include/stdint.h \
 /usr/include/c++/5/bits/allocator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++allocator.h \
 /usr/include/c++/5/ext/new_allocator.h /usr/include/c++/5/new \
 /usr/include/c++/5/exception \
 /usr/include/c++/5/bits/atomic_lockfree_defines.h \
 /usr/include/c++/5/bits/exception_ptr.h \
 /usr/include/c++/5/bits/nested_exception.h \
 /usr/include/c++/5/bits/localefwd.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++locale.h \
 /usr/include/c++/5/clocale /usr/include/locale.h \
 /usr/include/x86_64-linux-gnu/bits/locale.h /usr/include/c++/5/iosfwd \
 /usr/include/c++/5/cctype /usr/include/ctype.h \
 /usr/include/c++/5/bits/ostream_insert.h \
 /usr/include/c++/5/bits/cxxabi_forced.h \
 /usr/include/c++/5/bits/stl_function.h \
 /usr/include/c++/5/backward/binders.h \
 /usr/include/c++/5/bits/range_access.h \
 /usr/include/c++/5/initializer_list \
 /usr/include/c++/5/bits/basic_string.h \
 /usr/include/c++/5/ext/atomicity.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr-default.h \
 /usr/include/pthread.h /usr/include/sched.h \
 /usr/include/x86_64-linux-gnu/bits/sched.h \
 /usr/include/x86_64-linux-gnu/bits/timex.h \
 /usr/include/x86_64-linux-gnu/bits/setjmp.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h \
 /usr/include/c++/5/ext/alloc_traits.h \
 /usr/include/c++/5/bits/alloc_traits.h \
 /usr/include/c++/5/ext/string_conversions.h /usr/include/c++/5/cstdio \
 /usr/include/libio.h /usr/include/_G_config.h \
 /usr/include/x86_64-linux-gnu/bits/stdio_lim.h \
 /usr/include/x86_64-linux-gnu/bits/sys_errlist.h \
 /usr/include/c++/5/cerrno /usr/include/errno.h \
 /usr/include/x86_64-linux-gnu/bits/errno.h /usr/include/linux/errno.h \
 /usr/include/x86_64-linux-gnu/asm/errno.h \
 /usr/include/asm-generic/errno.h /usr/include/asm-generic/errno-base.h \
 /usr/include/c++/5/bits/functional_hash.h \
 /usr/include/c++/5/bits/hash_bytes.h \
 /usr/include/c++/5/bits/basic_string.tcc /usr/include/c++/5/limits \
 /usr/include/c++/5/bits/random.h /usr/include/c++/5/vector \
 /usr/include/c++/5/bits/stl_construct.h \
 /usr/include/c++/5/bits/stl_uninitialized.h \
 /usr/include/c++/5/bits/stl_vector.h \
 /usr/include/c++/5/bits/stl_bvector.h /usr/include/c++/5/bits/vector.tcc \
 /usr/include/c++/5/bits/uniform_int_dist.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/opt_random.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/x86intrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/ia32intrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/mmintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/xmmintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/mm_malloc.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/emmintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/pmmintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/tmmintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/ammintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/smmintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/popcntintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/wmmintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/immintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avxintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx2intrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512fintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512erintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512pfintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512cdintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512vlintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512bwintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512dqintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512vlbwintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512vldqintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512ifmaintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512ifmavlintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512vbmiintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/avx512vbmivlintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/shaintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/lzcntintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/bmiintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/bmi2intrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/fmaintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/f16cintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/rtmintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/xtestintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/mm3dnow.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/prfchwintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/fma4intrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/xopintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/lwpintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/tbmintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/rdseedintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/fxsrintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/xsaveintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/xsaveoptintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/adxintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/clwbintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/clflushoptintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/xsavesintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/xsavecintrin.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/mwaitxintrin.h \
 /usr/include/c++/5/bits/random.tcc /usr/include/c++/5/numeric \
 /usr/include/c++/5/bits/stl_numeric.h /usr/include/c++/5/functional \
 /usr/include/c++/5/typeinfo /usr/include/c++/5/tuple \
 /usr/include/c++/5/utility /usr/include/c++/5/bits/stl_relops.h \
 /usr/include/c++/5/array /usr/include/c++/5/stdexcept \
 /usr/include/c++/5/bits/uses_allocator.h sprites/sprite.h \
 sprites/../drawable.h  /usr/include/c++/5/iostream \
 /usr/include/c++/5/ostream /usr/include/c++/5/ios \
 /usr/include/c++/5/bits/ios_base.h \
 /usr/include/c++/5/bits/locale_classes.h \
 /usr/include/c++/5/bits/locale_classes.tcc \
 /usr/include/c++/5/system_error \
 /usr/include/x86_64-linux-gnu/c++/5/bits/error_constants.h \
 /usr/include/c++/5/streambuf /usr/include/c++/5/bits/streambuf.tcc \
 /usr/include/c++/5/bits/basic_ios.h \
 /usr/include/c++/5/bits/locale_facets.h /usr/include/c++/5/cwctype \
 /usr/include/wctype.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_base.h \
 /usr/include/c++/5/bits/streambuf_iterator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_inline.h \
 /usr/include/c++/5/bits/locale_facets.tcc \
 /usr/include/c++/5/bits/basic_ios.tcc \
 /usr/include/c++/5/bits/ostream.tcc /usr/include/c++/5/istream \
 /usr/include/c++/5/bits/istream.tcc sprites/../math/vector2f.h \
 sprites/../graphics/components/image.h sprites/../data/gameData.h \
 /usr/include/c++/5/map /usr/include/c++/5/bits/stl_tree.h \
 /usr/include/c++/5/ext/aligned_buffer.h \
 /usr/include/c++/5/bits/stl_map.h /usr/include/c++/5/bits/stl_multimap.h \
 sprites/../data/../math/vector2f.h sprites/../data/parseXML.h \
 /usr/include/c++/5/fstream /usr/include/c++/5/bits/codecvt.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/basic_file.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++io.h \
 /usr/include/c++/5/bits/fstream.tcc /usr/include/c++/5/deque \
 /usr/include/c++/5/bits/stl_deque.h /usr/include/c++/5/bits/deque.tcc \
 /usr/include/expat.h /usr/include/expat_external.h \
 sprites/../data/gameData.h sprites/../math/vector2f.h \
 sprites/../graphics/imageFactory.h \
 sprites/../graphics/components/image.h \
 sprites/../graphics/../data/gameData.h \
 sprites/../graphics/../sprites/spriteSheet.h  
	$(CC) -c $< -o $@ $(CFLAGS)
$(OBJDIR)spriteSheet.o: sprites/spriteSheet.cpp /usr/include/stdc-predef.h \
 sprites/spriteSheet.h   
	$(CC) -c $< -o $@ $(CFLAGS)
$(OBJDIR)walkingSprite.o: sprites/walkingSprite.cpp /usr/include/stdc-predef.h \
 sprites/walkingSprite.h sprites/sprite.h /usr/include/c++/5/string \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/os_defines.h \
 /usr/include/features.h /usr/include/x86_64-linux-gnu/sys/cdefs.h \
 /usr/include/x86_64-linux-gnu/bits/wordsize.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs-64.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/cpu_defines.h \
 /usr/include/c++/5/bits/stringfwd.h /usr/include/c++/5/bits/memoryfwd.h \
 /usr/include/c++/5/bits/char_traits.h \
 /usr/include/c++/5/bits/stl_algobase.h \
 /usr/include/c++/5/bits/functexcept.h \
 /usr/include/c++/5/bits/exception_defines.h \
 /usr/include/c++/5/bits/cpp_type_traits.h \
 /usr/include/c++/5/ext/type_traits.h \
 /usr/include/c++/5/ext/numeric_traits.h \
 /usr/include/c++/5/bits/stl_pair.h /usr/include/c++/5/bits/move.h \
 /usr/include/c++/5/bits/concept_check.h /usr/include/c++/5/type_traits \
 /usr/include/c++/5/bits/stl_iterator_base_types.h \
 /usr/include/c++/5/bits/stl_iterator_base_funcs.h \
 /usr/include/c++/5/debug/debug.h /usr/include/c++/5/bits/stl_iterator.h \
 /usr/include/c++/5/bits/ptr_traits.h \
 /usr/include/c++/5/bits/predefined_ops.h \
 /usr/include/c++/5/bits/postypes.h /usr/include/c++/5/cwchar \
 /usr/include/wchar.h /usr/include/stdio.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdarg.h \
 /usr/include/x86_64-linux-gnu/bits/wchar.h \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h /usr/include/xlocale.h \
 /usr/include/c++/5/cstdint \
 /usr/lib/gcc/x86_64-linux-gnu/5/include/stdint.h /usr/include/stdint.h \
 /usr/include/c++/5/bits/allocator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++allocator.h \
 /usr/include/c++/5/ext/new_allocator.h /usr/include/c++/5/new \
 /usr/include/c++/5/exception \
 /usr/include/c++/5/bits/atomic_lockfree_defines.h \
 /usr/include/c++/5/bits/exception_ptr.h \
 /usr/include/c++/5/bits/nested_exception.h \
 /usr/include/c++/5/bits/localefwd.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++locale.h \
 /usr/include/c++/5/clocale /usr/include/locale.h \
 /usr/include/x86_64-linux-gnu/bits/locale.h /usr/include/c++/5/iosfwd \
 /usr/include/c++/5/cctype /usr/include/ctype.h \
 /usr/include/x86_64-linux-gnu/bits/types.h \
 /usr/include/x86_64-linux-gnu/bits/typesizes.h /usr/include/endian.h \
 /usr/include/x86_64-linux-gnu/bits/endian.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap.h \
 /usr/include/x86_64-linux-gnu/bits/byteswap-16.h \
 /usr/include/c++/5/bits/ostream_insert.h \
 /usr/include/c++/5/bits/cxxabi_forced.h \
 /usr/include/c++/5/bits/stl_function.h \
 /usr/include/c++/5/backward/binders.h \
 /usr/include/c++/5/bits/range_access.h \
 /usr/include/c++/5/initializer_list \
 /usr/include/c++/5/bits/basic_string.h \
 /usr/include/c++/5/ext/atomicity.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/gthr-default.h \
 /usr/include/pthread.h /usr/include/sched.h /usr/include/time.h \
 /usr/include/x86_64-linux-gnu/bits/sched.h \
 /usr/include/x86_64-linux-gnu/bits/time.h \
 /usr/include/x86_64-linux-gnu/bits/timex.h \
 /usr/include/x86_64-linux-gnu/bits/pthreadtypes.h \
 /usr/include/x86_64-linux-gnu/bits/setjmp.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h \
 /usr/include/c++/5/ext/alloc_traits.h \
 /usr/include/c++/5/bits/alloc_traits.h \
 /usr/include/c++/5/ext/string_conversions.h /usr/include/c++/5/cstdlib \
 /usr/include/stdlib.h /usr/include/x86_64-linux-gnu/bits/waitflags.h \
 /usr/include/x86_64-linux-gnu/bits/waitstatus.h \
 /usr/include/x86_64-linux-gnu/sys/types.h \
 /usr/include/x86_64-linux-gnu/sys/select.h \
 /usr/include/x86_64-linux-gnu/bits/select.h \
 /usr/include/x86_64-linux-gnu/bits/sigset.h \
 /usr/include/x86_64-linux-gnu/sys/sysmacros.h /usr/include/alloca.h \
 /usr/include/x86_64-linux-gnu/bits/stdlib-float.h \
 /usr/include/c++/5/cstdio /usr/include/libio.h /usr/include/_G_config.h \
 /usr/include/x86_64-linux-gnu/bits/stdio_lim.h \
 /usr/include/x86_64-linux-gnu/bits/sys_errlist.h \
 /usr/include/c++/5/cerrno /usr/include/errno.h \
 /usr/include/x86_64-linux-gnu/bits/errno.h /usr/include/linux/errno.h \
 /usr/include/x86_64-linux-gnu/asm/errno.h \
 /usr/include/asm-generic/errno.h /usr/include/asm-generic/errno-base.h \
 /usr/include/c++/5/bits/functional_hash.h \
 /usr/include/c++/5/bits/hash_bytes.h \
 /usr/include/c++/5/bits/basic_string.tcc sprites/../drawable.h  \
 /usr/include/c++/5/iostream /usr/include/c++/5/ostream \
 /usr/include/c++/5/ios /usr/include/c++/5/bits/ios_base.h \
 /usr/include/c++/5/bits/locale_classes.h \
 /usr/include/c++/5/bits/locale_classes.tcc \
 /usr/include/c++/5/system_error \
 /usr/include/x86_64-linux-gnu/c++/5/bits/error_constants.h \
 /usr/include/c++/5/stdexcept /usr/include/c++/5/streambuf \
 /usr/include/c++/5/bits/streambuf.tcc \
 /usr/include/c++/5/bits/basic_ios.h \
 /usr/include/c++/5/bits/locale_facets.h /usr/include/c++/5/cwctype \
 /usr/include/wctype.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_base.h \
 /usr/include/c++/5/bits/streambuf_iterator.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/ctype_inline.h \
 /usr/include/c++/5/bits/locale_facets.tcc \
 /usr/include/c++/5/bits/basic_ios.tcc \
 /usr/include/c++/5/bits/ostream.tcc /usr/include/c++/5/istream \
 /usr/include/c++/5/bits/istream.tcc sprites/../math/vector2f.h \
 sprites/../graphics/components/image.h sprites/../data/gameData.h \
 /usr/include/c++/5/map /usr/include/c++/5/bits/stl_tree.h \
 /usr/include/c++/5/ext/aligned_buffer.h \
 /usr/include/c++/5/bits/stl_map.h /usr/include/c++/5/tuple \
 /usr/include/c++/5/utility /usr/include/c++/5/bits/stl_relops.h \
 /usr/include/c++/5/array /usr/include/c++/5/bits/uses_allocator.h \
 /usr/include/c++/5/bits/stl_multimap.h \
 sprites/../data/../math/vector2f.h sprites/../data/parseXML.h \
 /usr/include/c++/5/fstream /usr/include/c++/5/bits/codecvt.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/basic_file.h \
 /usr/include/x86_64-linux-gnu/c++/5/bits/c++io.h \
 /usr/include/c++/5/bits/fstream.tcc /usr/include/c++/5/deque \
 /usr/include/c++/5/bits/stl_construct.h \
 /usr/include/c++/5/bits/stl_uninitialized.h \
 /usr/include/c++/5/bits/stl_deque.h /usr/include/c++/5/bits/deque.tcc \
 /usr/include/expat.h /usr/include/expat_external.h \
 sprites/../data/gameData.h sprites/../math/vector2f.h
	$(CC) -c $< -o $@ $(CFLAGS)
clean:
	rm -f $(OBJDIR)/* $(EXEC)
