#include <utility>
#include <cmath>
#include <string>
#include "sprite.h"
#include "walkingSprite.h"
#include "enemy.h"
#include "../math/vector2f.h"
#include "bidirectionalMultisprite.h"
#include "explodingSprite.h"

Enemy::~Enemy() {
  subject.second.detach(subject.first);
}

Enemy::Enemy(const std::string& name, Observable<Vector2f *>& observable, unsigned nPos) :
  Sprite(name),
  BidirectionalMultiSprite(name),
  WalkingSprite(name),
  numPositions(nPos),
  walkingVelocity{ data.getFloat("sprites/" + name + "/velocity/x"), 0 },
  subject(
    observable.attach([this](Vector2f * positions) { onPlayersMove(positions); }),
    observable
  ),
  hurting(false),
  exploding(false),
  explosion(*this, getImage()) {}

Enemy::Enemy(const Enemy& enemy) :
  Sprite(enemy),
  BidirectionalMultiSprite(enemy),
  WalkingSprite(enemy),
  numPositions(enemy.numPositions),
  walkingVelocity(enemy.walkingVelocity),
  subject(
    enemy.subject.second.attach([this](Vector2f * positions) { onPlayersMove(positions); }),
    enemy.subject.second
  ),
  hurting(false),
  exploding(false),
  explosion(*this, getImage()) {}

void Enemy::onPlayersMove(Vector2f * positions) {
  if (hurting) return;
  std::pair<Vector2f&, unsigned> minDistance(positions[0], getPosition() >> positions[0]);
  for (unsigned i = 1; i < numPositions; ++i) {
    const float d = getPosition() >> positions[i];
    if (d < minDistance.second) minDistance = std::make_pair(positions[i], d);
  }
  if (minDistance.first[0] < getX()) setVelocityX(-walkingVelocity[0]);
  else if (minDistance.first[0] > getX()) setVelocityX(walkingVelocity[0]);
}

void Enemy::update(Uint32 ticks) {
  if (exploding) {
    explosion.update(ticks);
    return;
  }
  Sprite::update(ticks);
  WalkingSprite::update(ticks);
  if (getVelocityX() > 0) offset = hurting ? numberOfFrames / 2 : 0;
  else if (getVelocityX() < 0) offset = hurting ? 0 : numberOfFrames / 2;
  advanceFrame(ticks);
  if (hurting && getY() == worldHeight - getScaledHeight() - 26) hurting = false;
}
void Enemy::draw() {
  if (exploding) explosion.draw();
  else BidirectionalMultiSprite::draw();
}

void Enemy::hurt(const bool left) {
  hurting = true;
  setVelocity(Vector2f{ std::abs(getVelocityX()) * (left ? -1 : 1), -300.0 });
}

void Enemy::kill() {
  exploding = true;
  explosion = ExplodingSprite(*this, getImage());
  explosion.setPosition(getPosition());
}

bool Enemy::isDead() const {
  return exploding && explosion.chunkCount() == 0;
}
