#include "multisprite.h"
#include "../data/gameData.h"
#include "../graphics/imageFactory.h"

void MultiSprite::advanceFrame(Uint32 ticks) {
	timeSinceLastFrame += ticks;
	if (timeSinceLastFrame > frameInterval) {
    currentFrame = (currentFrame+1) % numberOfFrames;
		timeSinceLastFrame = 0;
	}
}

MultiSprite::MultiSprite(const std::string& name, unsigned rows) :
  Sprite(name),
  images(ImageFactory::getInstance().getImages("sprites/" + name, rows)),
  currentFrame(0),
  numberOfFrames(data.getInt("sprites/" + name + "/frames") * rows),
  frameInterval(data.getInt("sprites/" + name + "/frameInterval")),
  timeSinceLastFrame(0)
{ }

MultiSprite::MultiSprite(const MultiSprite& s) :
  Sprite(s),
  images(s.images),
  currentFrame(s.currentFrame),
  numberOfFrames(s.numberOfFrames),
  frameInterval(s.frameInterval),
  timeSinceLastFrame(s.timeSinceLastFrame)
  { }

MultiSprite& MultiSprite::operator=(const MultiSprite& s) {
  Sprite::operator=(s);
  images = s.images;
  currentFrame = s.currentFrame;
  numberOfFrames = s.numberOfFrames;
  frameInterval = s.frameInterval;
  timeSinceLastFrame = s.timeSinceLastFrame;
  return *this;
}

void MultiSprite::draw() {
  images[currentFrame]->draw(getX(), getY(), getScale());
}

void MultiSprite::update(Uint32 ticks) {
  advanceFrame(ticks);
}
