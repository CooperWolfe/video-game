#include <cmath>
#include <random>
#include <functional>
#include <cmath>
#include "sprite.h"
#include "../data/gameData.h"
#include "../graphics/imageFactory.h"
#include "../math/vector2f.h"

Vector2f Sprite::makeVelocity(int vx, int vy) const {
  float newvx = data.getRandFloat(vx-50,vx+50);
  float newvy = data.getRandFloat(vy-50,vy+50);
  newvx *= [](){ if(rand()%2) return -1; else return 1; }();
  newvy *= [](){ if(rand()%2) return -1; else return 1; }();

  return Vector2f(newvx, newvy);
}

Sprite::Sprite(const std::string& name) :
  Drawable(name, Vector2f(0.0, 0.0), Vector2f(0.0, 0.0)),
  image(ImageFactory::getInstance().getImage("sprites/" + name)),
  data(Gamedata::getInstance()),
  worldWidth(data.getInt("world/width")),
  worldHeight(data.getInt("world/height"))
{
  setScale(data.getFloat("sprites/" + name + "/scale"));
  setPosition(Vector2f{
    data.tryGetFloat("sprites/" + name + "/startLoc/x", data.getRandInRange(0, worldWidth)),
    data.tryGetFloat("sprites/" + name + "/startLoc/y", data.getRandInRange(0, worldHeight))
  });
  setVelocity(Vector2f{
    data.tryGetFloat("sprites/" + name + "/velocity/x", 0),
    data.tryGetFloat("sprites/" + name + "/velocity/y", 0)
  });
}

Sprite::Sprite(const std::string& name, const Vector2f& pos, const Vector2f& vel, Image*& im) :
  Drawable(name, pos, vel),
  image(im),
  data(Gamedata::getInstance()),
  worldWidth(data.getInt("world/width")),
  worldHeight(data.getInt("world/height"))
{
  setScale(data.getFloat("sprites/" + name + "/scale"));
}

Sprite::Sprite(const Sprite& s) :
  Drawable(s),
  image(s.image),
  data(Gamedata::getInstance()),
  worldWidth(data.getInt("world/width")),
  worldHeight(data.getInt("world/height"))
{ }

Sprite& Sprite::operator=(const Sprite& rhs) {
  Drawable::operator=(rhs);
  image = rhs.image;
  worldWidth = rhs.worldWidth;
  worldHeight = rhs.worldHeight;
  return *this;
}

inline namespace{
  constexpr float SCALE_EPSILON = 2e-7;
}

void Sprite::draw() {
  if (getScale() < SCALE_EPSILON) return;
  image->draw(getX(), getY(), getScale());
}

void Sprite::update(Uint32 ticks) {
  Vector2f incr = getVelocity() * static_cast<float>(ticks) * 0.001;
  setPosition(getPosition() + incr);

  if (getY() < 0) setVelocityY(std::abs(getVelocityY()));
  if (getY() > worldHeight-getScaledHeight()) setVelocityY(-std::abs(getVelocityY()));

  if (getX() < 0) setVelocityX(std::abs(getVelocityX()));
  if (getX() > worldWidth-getScaledWidth()) setVelocityX(-std::abs(getVelocityX()));
}
