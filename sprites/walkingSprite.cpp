#include "walkingSprite.h"

void WalkingSprite::left() {
  if (getX() > 0) setVelocityX(-walkingVelocity[0]);
}

void WalkingSprite::right() {
  if (getX() < worldWidth-getScaledWidth())
    setVelocityX(walkingVelocity[0]);
}

void WalkingSprite::jump() {
  if (getY() < worldHeight - getScaledHeight() - 26) return;
  setVelocityY(-500);
}

void WalkingSprite::update(Uint32 ticks) {
  const int floor = worldHeight - getScaledHeight() - 26;
  if (getY() == floor) return;
  if (getY() > floor) {
    setY(floor);
    return;
  }
  const float newVelocity = gravity * ticks * 0.01 + getVelocityY();
  setVelocityY(newVelocity);
}
