#ifndef EXPLODING_SPRITE_H
#define EXPLODING_SPRITE_H

#include <vector>
#include <list>
#include "chunk.h"
#include "sprite.h"
#include "../graphics/components/image.h"

class ExplodingSprite : public Sprite {
public:
  ExplodingSprite() = delete;
  ExplodingSprite(const ExplodingSprite&) = delete;
  ExplodingSprite(const Sprite& s, const Image * f);
  ExplodingSprite& operator=(const ExplodingSprite&);
  ~ExplodingSprite();

  virtual void draw() const;
  virtual void update(Uint32 ticks);

  unsigned int chunkCount() const { return chunks.size(); }
  unsigned int freeCount() const { return freeList.size(); }
private:
  std::list<Chunk *> chunks;
  std::list<Chunk *> freeList;
  const Image * frame;

  void makeChunks(unsigned int);
};

#endif // EXPLODING_SPRITE_H
