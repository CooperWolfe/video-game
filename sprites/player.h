#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include <list>
#include "walkingSprite.h"
#include "bidirectionalMultisprite.h"
#include "../math/vector2f.h"
#include "../managers/projectilePool.h"
#include "../managers/trapPool.h"

class Player : public BidirectionalMultiSprite, public WalkingSprite {
public:
  Player() = delete;
  ~Player() = default;
  Player(const std::string& name) :
    Sprite(name),
    BidirectionalMultiSprite(name),
    WalkingSprite(name),
    lives(data.getShort("players/lives")),
    recovery(300),
    projectilePool("bullet", data.getUnsigned("players/bullets")),
    trapPool("vortex", data.getUnsigned("players/portals")) {}
  Player(const Player& player) :
    Sprite(player),
    BidirectionalMultiSprite(player),
    WalkingSprite(player),
    lives(player.lives),
    recovery(300),
    projectilePool("bullet", data.getUnsigned("players/bullets")),
    trapPool("vortex", data.getUnsigned("players/portals")) {}

  virtual void draw();
  virtual void update(Uint32 ticks);

  void dropPortal();
  void removeTrap(MultiSprite * trap) { trapPool.free(trap); }
  void shoot();
  void hurt();
  std::list<ProjectileSprite *> getBullets() const {
    return projectilePool.getActive();
  }
  std::list<MultiSprite *> getTraps() const {
    return trapPool.getActive();
  }
  short getLives() const { return lives; }

protected:
  virtual void advanceFrame(Uint32 ticks);

private:
  short lives;
  unsigned recovery;
  ProjectilePool projectilePool;
  TrapPool trapPool;
};

#endif // PLAYER_H
