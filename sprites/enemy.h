#ifndef ENEMY_H
#define ENEMY_H

#include <functional>
#include <utility>
#include "bidirectionalMultisprite.h"
#include "../observable/observable.h"
#include "../math/vector2f.h"
#include "walkingSprite.h"
#include "explodingSprite.h"

class Enemy : public BidirectionalMultiSprite, public WalkingSprite {
public:
  Enemy() = delete;
  ~Enemy();
  Enemy(const std::string& name, Observable<Vector2f *>& observable, unsigned nPos);
  Enemy(const Enemy& enemy);

  void update(Uint32);
  void draw();

  void hurt(const bool left);
  void onPlayersMove(Vector2f * positions);
  void kill();
  bool isExploding() const { return exploding; }
  bool isDead() const;

private:
  unsigned numPositions;
  Vector2f walkingVelocity;
  const std::pair<const unsigned, Observable<Vector2f *>&> subject;
  bool hurting, exploding;
  ExplodingSprite explosion;
};

#endif // ENEMY_H
