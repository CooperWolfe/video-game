#include "sprite.h"
#include "bidirectionalMultisprite.h"
#include "../graphics/imageFactory.h"
#include <string>

BidirectionalMultiSprite::BidirectionalMultiSprite(const std::string& name) :
  Sprite(name),
  MultiSprite(name, 2),
  offset(0)
{
  const int midpoint = data.getInt("world/width") / 2;
  if (getX() > midpoint) offset = numberOfFrames / 2;
}

void BidirectionalMultiSprite::update(Uint32 ticks) {
  if (getVelocityX() > 0) offset = 0;
  else if (getVelocityX() < 0) offset = numberOfFrames / 2;
  advanceFrame(ticks);
}

void BidirectionalMultiSprite::advanceFrame(Uint32 ticks) {
  timeSinceLastFrame += ticks;
  if (timeSinceLastFrame > frameInterval) {
    currentFrame = (currentFrame + 1) % (numberOfFrames / 2) + offset;
    timeSinceLastFrame = 0;
  }
}

BidirectionalMultiSprite& BidirectionalMultiSprite::operator=(const BidirectionalMultiSprite& sprite) {
  MultiSprite::operator=(sprite);
  offset = sprite.offset;
  return *this;
}
