#ifndef MULTISPRITE__H
#define MULTISPRITE__H
#include <string>
#include <vector>
#include <cmath>
#include "sprite.h"

class MultiSprite : virtual public Sprite {
public:
  MultiSprite() = delete;
  MultiSprite(const std::string& name) : MultiSprite(name, 1) {}
  MultiSprite(const MultiSprite&);
  virtual ~MultiSprite() = default;

  virtual void draw();
  virtual void update(Uint32 ticks);

  virtual const Image* getImage() const {
    return images[currentFrame];
  }
  int getScaledWidth()  const {
    return getScale()*images[currentFrame]->getWidth();
  }
  int getScaledHeight()  const {
    return getScale()*images[currentFrame]->getHeight();
  }
  virtual const SDL_Surface* getSurface() const {
    return images[currentFrame]->getSurface();
  }

protected:
  std::vector<Image *> images;

  unsigned currentFrame;
  unsigned numberOfFrames;
  unsigned frameInterval;
  float timeSinceLastFrame;

  void advanceFrame(Uint32 ticks);
  MultiSprite& operator=(const MultiSprite&);
  MultiSprite(const std::string& name, unsigned rows);
};
#endif
