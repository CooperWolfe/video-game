#ifndef WALKING_SPRITE_H
#define WALKING_SPRITE_H
#include "sprite.h"
#include "../math/vector2f.h"

class WalkingSprite : virtual public Sprite {
public:
  WalkingSprite() = delete;
  WalkingSprite(const std::string& name) :
    Sprite(name),
    walkingVelocity{ data.getFloat("sprites/" + name + "/velocity/x"), 0 },
    gravity(data.getFloat("world/gravity")) {}
  WalkingSprite(const WalkingSprite& sprite) :
    Sprite(sprite),
    walkingVelocity(sprite.walkingVelocity),
    gravity(data.getFloat("world/gravity")) {}
  virtual ~WalkingSprite() = default;

  virtual void update(Uint32 ticks);

  void left();
  void right();
  void jump();

protected:
  WalkingSprite& operator=(const WalkingSprite&);
  Vector2f walkingVelocity;
  float gravity;
};

#endif // WALKING_SPRITE_H
