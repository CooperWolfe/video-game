#ifndef BIDIRECTIONALMULTISPRITE_H
#define BIDIRECTIONALMULTISPRITE_H
#include <string>
#include <vector>
#include "multisprite.h"

class BidirectionalMultiSprite : public MultiSprite {
public:
  BidirectionalMultiSprite() = delete;
  virtual ~BidirectionalMultiSprite() = default;
  BidirectionalMultiSprite(const std::string& name);
  BidirectionalMultiSprite(const BidirectionalMultiSprite& sprite) :
    Sprite(sprite),
    MultiSprite(sprite),
    offset(sprite.offset) {}

  virtual void draw() { MultiSprite::draw(); }
  virtual void update(Uint32 ticks);

  virtual const Image* getImage() const {
    return images[currentFrame];
  }
  virtual const SDL_Surface* getSurface() const {
    return images[currentFrame]->getSurface();
  }
  unsigned getOffset() const {
    return offset;
  }

protected:
  virtual void advanceFrame(Uint32 ticks);
  BidirectionalMultiSprite& operator=(const BidirectionalMultiSprite&);
  unsigned offset;
};
#endif // BIDIRECTIONALMULTISPRITE_H
