#ifndef SPRITE__H
#define SPRITE__H
#include <string>
#include "../drawable.h"
#include "../data/gameData.h"
#include "../math/vector2f.h"

class Sprite : public Drawable {
public:
  Sprite() = delete;
  Sprite(const std::string&);
  Sprite(const std::string&, const Vector2f&, const Vector2f&, Image*&);
  Sprite(const Sprite&);
  virtual ~Sprite() = default;
  Sprite& operator=(const Sprite&);

  virtual void draw();
  virtual void update(Uint32 ticks);

  virtual const Image* getImage() const { return image; }
  virtual const SDL_Surface* getSurface() const { return image->getSurface(); }
  virtual int getScaledWidth() const { return getScale()*image->getWidth(); }
  virtual int getScaledHeight() const { return getScale()*image->getHeight(); }

private:
  const Image* image;

protected:
  Gamedata& data;
  int worldWidth;
  int worldHeight;

  Vector2f makeVelocity(int, int) const;
};
#endif
