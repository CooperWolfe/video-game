#include <iostream>
#include <cmath>
#include "explodingSprite.h"
#include "sprite.h"
#include "../graphics/components/image.h"
#include "chunk.h"
#include "../math/vector2f.h"

ExplodingSprite::ExplodingSprite(const Sprite& s, const Image * f) :
  Sprite(s),
  chunks(),
  freeList(),
  frame(f) {
  makeChunks(Gamedata::getInstance().getInt("sprites/" + s.getName() + "/chunk/size"));
}

ExplodingSprite& ExplodingSprite::operator=(const ExplodingSprite& sprite) {
  Sprite::operator=(sprite);
  for (Chunk * c : chunks) delete c;
  for (Chunk * c : freeList) delete c;
  chunks.clear(); freeList.clear();
  frame = sprite.frame;
  makeChunks(Gamedata::getInstance().getInt("sprites/" + sprite.getName() + "/chunk/size"));
  return *this;
}

ExplodingSprite::~ExplodingSprite() {
  for (Chunk * c : chunks) delete c;
  for (Chunk * c : freeList) delete c;
}

void ExplodingSprite::draw() const {
  for (Chunk* chunk : chunks) chunk->draw();
}

void ExplodingSprite::update(Uint32 ticks) {
  auto ptr = chunks.begin();
  while (ptr != chunks.end()) {
    (*ptr)->update(ticks);
    if ((*ptr)->goneTooFar()) {
      freeList.push_back(*ptr);
      ptr = chunks.erase(ptr);
    }
    else ++ptr;
  }
}

void ExplodingSprite::makeChunks(unsigned int n) {
  // Break the SDL_Surface into n*n squares; where each square
  // has width and height of imageWidth/n and imageHeight/n
  // Note that "n" s/b a perfect square.
  int chunk_width = std::max(1u, frame->getWidth()/n);
  int chunk_height = std::max(1u, frame->getHeight()/n);
  int speedx = static_cast<int>(getVelocityX()); // Wanna test for zero...
  int speedy = static_cast<int>(getVelocityY()); // Make sure it's an int.
  if (speedx == 0) speedx = 1; // Make sure it's not 0
  if (speedy == 0) speedy = 1; // Make sure it's not 0

  int source_y = 0;
  while  (source_y+chunk_height < frame->getHeight()) {
    int source_x = 0;
    while (source_x+chunk_width < frame->getWidth()) {
      // Give each chunk it's own speed/direction:
      float sx = (rand() % speedx + 40) * (rand()%2?-1:1); // 'cause %0 is
      float sy = (rand() % speedy + 40) * (rand()%2?-1:1); // float except

      Image * image = frame->crop({ source_x, source_y, chunk_width, chunk_height });
      Chunk * chunk = new Chunk(
        Vector2f{ getX() + source_x, getY() + source_y },
        Vector2f{ sx, sy },
        getName(),
        image
      );
      chunk->setScale(getScale());
      chunks.push_back(chunk);
      source_x += chunk_width;
    }
    source_y += chunk_height;
  }
}
