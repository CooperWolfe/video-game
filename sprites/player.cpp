#include "walkingSprite.h"
#include "bidirectionalMultisprite.h"
#include "sprite.h"
#include "player.h"

void Player::update(Uint32 ticks) {
  Sprite::update(ticks);
  BidirectionalMultiSprite::update(ticks);
  WalkingSprite::update(ticks);
  setVelocityX(0);
  projectilePool.update(ticks);
  trapPool.update(ticks);
}
void Player::advanceFrame(Uint32 ticks) {
  if (!getVelocityX()) {
    currentFrame = getOffset();
    return;
  }

  timeSinceLastFrame += ticks;
  if (timeSinceLastFrame > frameInterval) {
    currentFrame = (currentFrame + 1) % (numberOfFrames / 2) + getOffset();
    timeSinceLastFrame = 0;
  }
}

void Player::draw() {
  projectilePool.draw();
  trapPool.draw();
  if (recovery) {
    --recovery;
    if (recovery % 8 < 4) return;
  }
  images[currentFrame]->draw(getX(), getY(), getScale());
}

void Player::hurt() {
  if (recovery != 0) return;
  recovery = 100;
  --lives;
}

void Player::dropPortal() {
  if (recovery != 0) return;
  if (getY() != worldHeight - getScaledHeight() - 26) return;
  trapPool.activate(Vector2f{
    getX(),
    getY() + getScaledHeight() - 16
  });
}

void Player::shoot() {
  if (recovery != 0) return;
  projectilePool.activate(Vector2f{
    getX() + (getOffset() ? 0 : getScaledWidth()),
    getY() + getScaledHeight() / 2
  }, getOffset());
}
