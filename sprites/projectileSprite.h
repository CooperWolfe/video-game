#ifndef PROJECTILE_SPRITE_H
#define PROJECTILE_SPRITE_H
#include <string>
#include "sprite.h"

class ProjectileSprite : virtual public Sprite {
public:
  ProjectileSprite() = delete;
  ProjectileSprite(const std::string& name) : Sprite(name), active(true) {}
  ~ProjectileSprite() = default;
  ProjectileSprite& operator=(const ProjectileSprite&);

  void update(Uint32 ticks);

  bool isActive() const { return active; }
  void toggleActive() { active = !active; }

private:
  bool active;
};

#endif // PROJECTILE_SPRITE_H
