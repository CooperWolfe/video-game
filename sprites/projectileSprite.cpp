#include "projectileSprite.h"
#include "sprite.h"

ProjectileSprite& ProjectileSprite::operator=(const ProjectileSprite& rhs) {
  Sprite::operator=(rhs);
  active = rhs.active;
  return *this;
}

void ProjectileSprite::update(Uint32 ticks) {
  Vector2f incr = getVelocity() * static_cast<float>(ticks) * 0.001;
  setPosition(getPosition() + incr);
  if (getX() < 0 || getX() > worldWidth-getScaledWidth()) active = false;
}
