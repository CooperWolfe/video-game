#!/bin/bash

IMPEXT=".cpp"
OBJEXT=".o"
CC="g++"
CFLAGS="`sdl2-config --cflags` -g -W -Wall -Werror -std=c++11 -Weffc++ -Wextra -pedantic -O0 -I `sdl2-config --prefix`/include/"
LDFLAGS="`sdl2-config --libs` -lm -lexpat -lSDL2_ttf -lSDL2_image"
EXEC="run"
# OBJDIR MUST BE FILLED NOT SET TO A DIRECTORY CONTAINING IMPORTANT FILES !!
# Read under target `clean` for reasoning
OBJDIR=".os/" # This cannot be empty or filled with important files!
# ^ Did you change this to somewhere important or leave it empty?
# If so, fix that!

IMPFILES=$(find * -name "*$IMPEXT")

echo "CC := "$CC
echo "CFLAGS := "$CFLAGS
echo "LDFLAGS := "$LDFLAGS
echo "EXEC := "$EXEC
echo ""
echo '# OBJDIR MUST BE SET AND SET NOT TO A DIRECTORY'
echo '# CONTAINING IMPORTANT FILES !! SEE TARGET `CLEAN` FOR REASONING'
echo "OBJDIR := "$OBJDIR
echo -e "OBJS := \\"
for file in $IMPFILES; do
	OBJECTFILE=${file%$IMPEXT}$OBJEXT
	echo -e "\t""\$(OBJDIR)"${OBJECTFILE##*/}" \\"
done
echo ""
echo "compile: \$(OBJS)"
echo -e "\t\$(CC) \$(OBJS) -o \$(EXEC) \$(LDFLAGS)"
echo ""
for file in $IMPFILES; do
	echo -n "\$(OBJDIR)"
	$CC -std=c++11 -M -MG $file | sed 's/SDL.h//g' | sed 's/SDL_ttf.h//g' | sed 's/SDL_surface.h//g' | sed 's/SDL_image.h//g'
	echo -e "\t\$(CC) -c \$< -o \$@ \$(CFLAGS)"
done
echo "clean:"
echo -e "\trm -f \$(OBJDIR)/* \$(EXEC)"
