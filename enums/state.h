#ifndef STATE_ENUM
#define STATE_ENUM

enum state {
  start,
  play,
  lose,
  win
};

#endif
