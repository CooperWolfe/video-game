#include <iostream>
#include "state.h"

std::ostream& operator<<(std::ostream& os, state s) {
  switch (s) {
    case start: os << "start"; break;
    case play:  os << "play";  break;
    case lose:  os << "lose";  break;
    case win:   os << "win";   break;
    default: os.setstate(std::ios_base::failbit);
  }
  return os;
}
